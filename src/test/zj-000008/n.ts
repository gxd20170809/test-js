import { ICommonParams, IHeaders } from '@dfeidao/nodejs/interfaces';
import { HTMLElement } from 'node-html-parser';
import s from './ns';

import init from '@dfeidao/nodejs/component';

import tpl from './tpl';

/// FEIDAO IMPACTIONS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)

/// FEIDAO IMPACTIONS END

export default function main(html: HTMLElement, url: string, msg: ICommonParams, headers: IHeaders, query: {}) {

	/// FEIDAO ACTIONS BEGIN
	/// 请不要修改下面的代码哟(๑•ω•๑)

	const actions = {};

	/// FEIDAO ACTIONS END


	return init('zj-000008', tpl, s, actions, html, url, msg, headers, query);
}

