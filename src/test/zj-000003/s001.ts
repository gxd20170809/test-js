import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';


interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	// page: {
	// 	pn: number;// 定义页面参数
	// 	feature_name: string;// 输入框的值
	// 	feature_name_2: string;
	// 	asc?: string[];
	// 	desc?: string[];
	// };
	filter: {
		pn: number;
		feature_name: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

interface Quhiddenfeatst {
	standard_no: string;
	count: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:f:\培训\项目\feidao-4.6\test-js\src\test\zj-000001\s001,action_id:' + action_id);
	// { productid：productid}
	const q = query(action_id, session_id, systemid);
	q.prepare('qu_hidden_feat_st', ['hidden_source_feature_no', 'hidden_source_feature_name', 'plan_cla_attr_name', 'plan_type_name', 'companyname', 'area_name', 'target', 'hidden_code', 'hidden_name', 'standard_no'], { productid, hidden_source_feature_name: { $like: '%' + message.filter.feature_name + '%' } }, 10, message.filter.pn, [], []);
	// [[{area_name:'省'}，{area_name:'市'}], [{area_name:'省'}，{area_name:'市'}]]
	const res = await q.exec<Quhiddenfeatst>();

	// [{standard_no:'1'}，{standard_no:'2'}]
	const data = res[0];
	const arr = [];
	for (let i = 0; i < data.length; i++) {
		// [{area_name:'省'}，{area_name:'市'}]
		// {area_name:'省'}
		const element = data[i];
		const v = element.standard_no;
		arr.push(v);
	}

	const q1 = complex_query(action_id, session_id, systemid);
	// [{standard_no:'1', count: 2},{standard_no:'2', count: 3}]
	const d = await q1.add_field('qu_check_result', 'standard_no', 'standard_no')
		.add_fun('qu_check_result', 'standard_no', 'count', 'count')
		.groupby('qu_check_result', 'standard_no')
		.where_in('qu_check_result', 'standard_no', arr)
		.exec<Quhiddenfeatst>();

	for (let i = 0; i < data.length; i++) {
		// [{area_name:'省'}，{area_name:'市'}]
		// {area_name:'省'}
		const element = data[i];
		const v = element.standard_no; // 3
		const ars = d.filter((value, index, array) => {
			const no = value.standard_no;
			return v === no;
		});
		if (ars.length > 0) {
			const cnv = ars[0];
			element.count = cnv.count;
		} else {
			element.count = 0;
		}
	}

	const { prepare: p, exec } = count(action_id, session_id, systemid);
	p('qu_hidden_feat_st', { productid, hidden_source_feature_name: { $like: '%' + message.filter.feature_name + '%' } });
	// prepare('qu_hidden_feat_st', { productid });
	// prepare('qu_hidden_feat_st', { productid });

	const [c] = await exec();

	log('Service end path:f:\培训\项目\feidao-4.6\test-js\src\test\zj-000001\s001,action_id:' + action_id);
	return {
		data: { data, c }

	};
}
