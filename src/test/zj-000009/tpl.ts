export default `<div class="bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12 display-n" style="z-index:120;">
	<div style="width:620px;padding-top:100px;padding-bottom:50px;margin:0 auto;">
		<div class="bg-f overflow-h text-l" style="border-radius:4px">
			<div class="font14 bg-theme color-f ht40 l-ht40" style="padding:0 10px;border-top-left-radius: 4px;border-top-right-radius: 4px">
				<i class="icon iconfont icon-cuo fr text-c cursor-p font12" title="close" data-feidao-actions="click:eu-001"></i>隐患源特性集
			</div>
			<div>
				<!-- 筛选条件 start -->
				<div class="ht50 l-ht50" style="padding-left:20px">
					<div class="display-ib" style="margin-right:20px;">
						<input type="text">
					</div>
					<div class="display-ib bg-theme text-c cursor-p ht30 l-ht30 radius-4" style="width:30px;margin-right:50px">
						<i class="iconfont icon-sousuo color-f" title="搜索"></i>
					</div>
					<input type="button" class="btn " style="width:50px;line-height:20px;margin-left: 290px" value="确定">
				</div>
				<!-- 筛选条件 end -->
				<div class="p20" style="min-height:200px">
					<!-- 表格 start -->
					<!--表头 start -->
					<div class="t_title b-box" style="width:570px;">
						<span class="display-ib p5 border-r-1 text-c b-box" style="width:190px;">隐患源特性代码
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:190px;">隐患源特性名称
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:190px;">公司
						</span>
					</div>
					<!--表头 end-->
					<!-- 表体 start -->
					<div class="t_body display-" style="width:571px;">
						<div class="tableRow b-box">
							<span class="display-ib p5 border-r-1 text-c b-box" style="width:190px;">Yl132123
							</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h" style="width:190px;" title="声音">声音
							</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:190px;" title="郑州科技游戏间公司">郑州科技游戏公司
							</span>
						</div>
					</div>
					<!-- 表体 end -->
					<!-- 表格 end -->
					<!--筛选结果为空的蒙版 start-->
					<div class="none display-">
						<i class="icon iconfont icon-wushuju display-ib text-c vertical-a-t color-9" style="font-size:100px;"></i>
						<div class="font22 color-9" style="height:50px;line-height:50px;">当前筛选条件下，没有匹配的数据</div>
					</div>
					<!--筛选结果为空的蒙版 end-->
					<!--分页 start-->
					<div class="page">
						分页控件
					</div>
					<!--分页 end-->
				</div>
			</div>
		</div>
	</div>
</div>`;
