import { parse } from 'node-html-parser';

const html = `
<zj-000001></zj-000001>
<zj-000002></zj-000002>
<div class="w100" style="margin-top: 60px;height: calc(100% - 60px);background: #EAEDF2;">
		<!-- 页面名称 start -->
	<div class="ht40 l-ht40 bg-f pos-f font14 color-3" style="left:200px;right:0;z-index: 4;padding-left:20px ">
		<i class="iconfont icon-weizhang"></i>
			<span>隐患源特性标准</span>
	</div>
	<!-- 页面名称 end -->
	<!-- 主内容 start -->
	<div style="padding-left:220px;padding-top: 60px;">
		<div class="bg-f">
			<zj-000003></zj-000003>
			<zj-000004></zj-000004>
			<zj-000005></zj-000005>
			<zj-000006></zj-000006>
		</div>
	</div>
</div>
<zj-000003></zj-000003>
<zj-000004></zj-000004>
<zj-000005></zj-000005>
<zj-000006></zj-000006>
<zj-000007></zj-000007>
<zj-000008></zj-000008>
<zj-000009></zj-000009>
<zj-000010></zj-000010>`;

export default parse(html);

