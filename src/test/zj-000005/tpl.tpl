<div class="display-n">
	<div class="ht40 b-box" style="padding-left:10px;border-bottom:1px solid #eaedf2;">
		<span class="font12 color-6 ht40 cursor-p" style="display:inline-flex;align-items:center;">
			<i class="iconfont icon-fanhui color-theme font22" style="margin-right:10px;"></i>
			返回
		</span>
		<span class="font12 color-6 ht40 cursor-p" style="display:inline-flex;align-items:center;margin-left:20px;">
			<i class="iconfont icon-keep color-theme font20" style="margin-right:10px;"></i>
			保存
		</span>
	</div>
	<div class="ht40" style="display:flex;align-items:center;">
		<span class="display-b bg-theme" style="width:3px;height:18px;"></span>
		<span class="font12 color-6" style="margin-left:10px;">特性标准</span>
	</div>
	<div style="margin-left:20px;">
		<div style="height:50px;display:flex;align-items: center;">
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
					隐患源编号：
				</span>
				<i class="iconfont icon-icon02 color-theme font20 vertical-a-m"></i>
				<span style="margin-left:5px;" class="font12 color-6 text-r b-box display-ib vertical-a-m">YL004568</span>
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r display-ib" style="width:100px;">
					隐患源名称：
				</span>
				<input type="text" style="width: 140px">
			</div>
		</div>
		<div style="height:50px;display:flex;align-items: center;">
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>
					特性代码：
				</span>
				<i class="iconfont icon-icon02 color-theme font20 vertical-a-m"></i>
				<span style="margin-left:5px;" class="font12 color-6 text-r b-box display-ib vertical-a-m">YL004568</span>
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r display-ib" style="width:100px;">
					特性名称：
				</span>
				<input type="text" style="width: 140px">
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r  display-ib" style="width:100px;">
					公司：
				</span>
				<input type="text" style="width: 140px">
			</div>
		</div>
		<div style="height:50px;display:flex;align-items: center;">
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					区域：
				</span>
				<select class="select" style="width:140px;">
					<option>请选择</option>
				</select>
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r display-ib" style="width:100px;">
					<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
					计划分类属性：
				</span>
				<select class="select" style="width:140px;">
					<option>巡查类</option>
				</select>
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r display-ib" style="width:100px;">
					<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
					计划类型：
				</span>
				<select class="select" style="width:140px;">
					<option>巡查类</option>
				</select>
			</div>

		</div>
		<div style="height:50px;display:flex;align-items: center;">
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>目标值：
				</span>
				<input type="text" style="width: 140px">
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>
					单位：
				</span>
				<input type="file">
			</div>
		</div>
		<div style="height:50px;display:flex;align-items: center;">
			<div style="width:30%" style="margin-right: 50px;">
				<span class="font12 color-6 text-r b-box display-ib " style="width:100px;">
					<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>规范上限：
				</span>
				<input type="text" style="width: 140px">
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					<span class="color-red b-box vertical-a-m  display-ib" style="margin-right:3px;">*</span>
					规范下限：
				</span>
				<input type="text" style="width: 140px">
			</div>
		</div>
		<div style="height:50px;display:flex;align-items: center;">
			<div style="width:30%">
				<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
					<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>上限：
				</span>
				<input type="text" style="width: 140px">
			</div>
			<div style="width:30%">
				<span class="font12 color-6 text-r display-ib" style="width:100px;">
					<span class="color-red b-box vertical-a-m " style="margin-right:3px;">*</span>下限：
				</span>
				<input type="text" style="width: 140px">
			</div>
		</div>
	</div>
	<div class="ht40" style="display:flex;align-items:center;">
		<span class="display-b bg-theme" style="width:3px;height:18px;"></span>
		<span class="font12 color-6" style="margin-left:10px;">特性关联结果</span>
		<i class="iconfont icon-icon02 color-theme font20 vertical-a-m" style="margin-left:20px;"></i><span class="color-6 font16"
		 style="margin-left:5px;">新增</span><i class="iconfont icon-jian color-theme font20 vertical-a-m" style="margin-left:20px;"></i><span
		 class="color-6 font16" style="margin-left:5px;">删除</span>
	</div>

	<div class="p20" style="width:70%">
		<div class="display-">
			<div class="t_title">
				<span class="display-ib p5 border-r-1 text-c b-box" style="width:5%;">
					<input type="checkbox" style="width:25px;">
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:20%;">检查结果编号
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:23%;">检查结果描述
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:10%;">是否异常
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:14%;">单位
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:14%;">检查上限
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:14%;">检查下限
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span>
			</div>
			<!--表头 end-->
			<!-- 表体 start -->
			<div class="t_body display-">
				<div class="tableRow">
					<span class="display-ib p5 border-r-1 text-c b-box" style="width:5%;">
						<input type="checkbox">
					</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h text-c color-theme" style="width:20%;"
					 title="">JC1155886
					</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h" style="width:23%;" title="">
						正常结果
					</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:10%;" title="">否
					</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h text-c" style="width:14%;" title="">分贝
					</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h text-c" style="width:14%;" title="">50
					</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h text-c" style="width:14%;" title="">40
					</span>
				</div>
			</div>
		</div>
	</div>
</div>