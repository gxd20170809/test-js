import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import operate from '@dfeidao/atom-nodejs/db/operate';
import { systemid, productid } from '../../atom/config';
interface Message {
	nos: string[];
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string[];
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders) {
	log('Service begin path:e:\王举\学习文件\飞道工作文件\项目\test-js\src\wangju\zj-000010\s001,action_id:' + action_id);

	const { del, exec } = operate(action_id, session_id, systemid);
	del('qu_hidden_feat_st', { productid, standard_no: { $in: message.nos } });
	del('qt_check_result', { productid, standard_no: { $in: message.nos } });
	const res = await exec();

	log('Service end path:e:\王举\学习文件\飞道工作文件\项目\test-js\src\wangju\zj-000010\s001,action_id:' + action_id);
	return res;
}
