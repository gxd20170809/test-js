import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
import get from '@dfeidao/atom-web/local/get';

export default async function a003(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo

	const info = get(fd, 'info')
	console.log(info);
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v = n.value;
	const res = await nodejs('wangju/zj-000009/s001', {
		hidden_source: info,
		hidden_source_feature_name: v,
		pn: 1
	});
	console.log(e);
	render(fd, res, p001, 'p001', 'inner');
}
