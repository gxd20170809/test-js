import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import query from '@dfeidao/atom-nodejs/db/query';
import count from '@dfeidao/atom-nodejs/db/count';
import { systemid, productid } from '../../atom/config';
interface Message {
	hidden_source: string,
	hidden_source_feature_name: string,
	pn: number
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string[];
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}
// 表名	hazard_source_feature				
// 标题	隐患源特性				
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// productid	产品ID	string	50	√	ⅹ
// hidden_source	隐患源编号	string	32	√	ⅹ
// companyid	公司编号	string	50	√	ⅹ
// companyname	公司名称	string	50	√	ⅹ
// factory_no	工厂编号	string	50	√	ⅹ
// factory_name	工厂名称	string	50	√	ⅹ
// hidden_source_feature_no	隐患源特性编号	string	50	√	ⅹ
// hidden_source_feature_name	隐患源特性名称	string	50	√	ⅹ
// hidden_source_feature_code	隐患源特性代码	string	50	√	ⅹ
// status_manage	状态管理	string	32	√	ⅹ
// control_data	控制数据	string	32	√	ⅹ


export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\王举\学习文件\飞道工作文件\飞道项目\test-js\src\wangju\zj-000009\s001,action_id:' + action_id);

	const { prepare, exec } = count(action_id, session_id, systemid);
	prepare('hazard_source_feature', { productid, hidden_source: message.hidden_source, hidden_source_feature_name: { $like: '%' + message.hidden_source_feature_name + '%' } });
	const [t] = await exec();

	const pp = query(action_id, session_id, systemid);
	pp.prepare('hazard_source_feature', ['hidden_source_feature_name', 'hidden_source_feature_code', 'companyid', 'companyname', 'hidden_source_feature_no', 'hidden_source'], { productid, hidden_source: message.hidden_source, hidden_source_feature_name: { $like: '%' + message.hidden_source_feature_name + '%' } }, 1, message.pn, [], []);

	const [data1] = await pp.exec();

	const d = data1.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	})

	log('Service end path:e:\王举\学习文件\飞道工作文件\飞道项目\test-js\src\wangju\zj-000009\s001,action_id:' + action_id);
	return {
		data: { data1: d, t }
	} as IWebResult;
}
