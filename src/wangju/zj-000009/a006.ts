import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
import page from "@dfeidao/fd-w000010"

export default async function a006(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {
	// todo
	const info = get(fd, 'info');
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v = n.value;
	const pn = a.page_no;
	const res = await nodejs('wangju/zj-000009/s001', {
		hidden_source: info.hidden_source,
		hidden_source_feature_name: v,
		pn
	});

	render(fd, res, p001, 'p001', 'inner');
	const page = fd.data.node.querySelector<page>('[data-id="page1"]');
	page.setAttribute('page-no', pn + '');

}
