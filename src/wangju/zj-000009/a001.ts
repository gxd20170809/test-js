import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string, info: { hidden_source: string }) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="add"]');
	n.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12');
	set(fd, 'flag', flag);
	set(fd, 'info', info);

	const res = await nodejs('wangju/zj-000009/s001', {
		hidden_source: info.hidden_source,
		hidden_source_feature_name: '',
		pn: 1
	});
	render(fd, res, p001, 'p001', 'inner');
}
