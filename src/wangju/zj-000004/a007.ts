import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import render from '@dfeidao/atom-web/render/render';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import p003 from './p003';
export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo
	const n = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const v = n.value;
	const res = await nodejs<{
		data1: {}[]
	}>('wangju/zj-000004/s002', {
		filter: {
			plan_cla_attr_no: v
		}
	});
	console.log(res);
	render(fd, res.data1, p003, 'p003', 'inner');
}
