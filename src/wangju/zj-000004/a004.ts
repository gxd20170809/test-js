import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';
import get from '@dfeidao/atom-web/local/get';
export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// 打开隐患源特性
	const info = get(fd, 'info')
	if (info) {
		fire('zj-000009', 'a001', 'add', info);
	} else {
		alert('请先选择隐患源');
	}

}
