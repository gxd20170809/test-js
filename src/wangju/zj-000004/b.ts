import init from '@dfeidao/web/component';

import s from './s';

/// FEIDAO IMPACTIONS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)
import a001 from './a001';
import a002 from './a002';
import a003 from './a003';
import a004 from './a004';
import a005 from './a005';
import a006 from './a006';
import a007 from './a007';
import a008 from './a008';
import a009 from './a009';
import a010 from './a010';
import a011 from './a011';
import a012 from './a012';
import a013 from './a013';
import a014 from './a014';
import a015 from './a015';

/// FEIDAO IMPACTIONS END

export default function main(url: string, query: {}) {
	/// FEIDAO ACTIONS BEGIN
	/// 请不要修改下面的代码哟(๑•ω•๑)
	const actions = { a001, a002, a003, a004, a005, a006, a007, a008, a009, a010, a011, a012, a013, a014, a015 };

	/// FEIDAO ACTIONS END
	return init('zj-000004', s, actions, url, query);
}
