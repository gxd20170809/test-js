import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import upload from '@dfeidao/atom-web/file/upload';
import { productid } from '../../atom/config';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import refresh from '@dfeidao/atom-web/url/refresh';

export default async function a014(fd: IFeidaoAiBrowserComponent) {
	// todo
	const info = get(fd, 'info')
	if (!info) {
		alert('请先选择隐患源');
		return;
	}
	const data = get(fd, 'feature_info') as { hidden_source_feature_name: string };
	console.log(data);
	if (!data) {
		alert('请先选择隐患源特性');
		return;
	}

	const area_name1 = fd.data.node.querySelector<HTMLSelectElement>('[data-id="area_name"]');
	const area_name = area_name1.value;
	if (area_name === '请选择') {
		alert("请选择区域");
		return;
	}
	const plan1 = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const plan = plan1.value;
	let plantext = '';
	if (plan === '请选择') {
		alert("请选择计划分类属性");
		return;
	} else {
		plantext = plan1.selectedOptions[0].text;
	}
	const plan_type1 = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan_type"]');
	const plan_type_no = plan_type1.value;
	let plan_type_name = '';
	if (plan_type_no === '请选择') {
		alert("请选择计划类型");
		return;
	} else {
		plan_type_name = plan_type1.selectedOptions[0].text;
	}
	const target1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="target"]');
	const target = target1.value;
	if (!target) {
		alert("请输入目标值");
		return;
	}
	const unit1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="unit"]');
	const res = await upload(productid, unit1);
	const unit = res.filename;
	if (!unit) {
		alert("请选择图片");
		return;
	}

	console.log("01400000000000000000", unit);
	const spec_floor1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_floor"]');
	const spec_floor = spec_floor1.value;
	if (!spec_floor) {
		alert("请输入规范下限");
		return;
	}
	const spec_ceil1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_ceil"]');
	const spec_ceil = spec_ceil1.value;
	if (!spec_ceil) {
		alert("请输入规范上限");
		return;
	}
	const upper_limit1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="upper_limit"]');
	const upper_limit = upper_limit1.value;
	if (!upper_limit) {
		alert("请输入上限");
		return;
	}
	const down_value1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="down_value"]');
	const down_value = down_value1.value;
	if (!down_value) {
		alert("请输入下限");
		return;
	}
	const d = get(fd, 'data');
	const obj = await nodejs<{ code: 0 | 1; result: string }>('wangju/zj-000004/s003', {
		filter: {
			plan_type_no: plan_type_no,
			plan_type_name: plan_type_name,
			plan_cla_attr_no: plan,
			plan_cla_attr_name: plantext,
			area_name: area_name,
			upper_limit: upper_limit,
			down_value: down_value,
			target: target,
			spec_ceil: spec_ceil,
			spec_floor: spec_floor,
			unit: unit

		},
		feature_info: data,
		data: info,
		result_info: d

	});
	console.log(obj);
	if (obj.code === 1) {
		alert('保存成功');
		refresh();
	}


}
