import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import operate from '@dfeidao/atom-nodejs/db/operate';
import { systemid, productid } from '../../atom/config';
import doc_code from '@dfeidao/atom-nodejs/msg/doc-code';
interface Message {
	filter: {
		plan_type_no: string,
		plan_type_name: string,
		plan_cla_attr_no: string,
		plan_cla_attr_name: string,
		area_name: string,
		upper_limit: string,
		down_value: string,
		target: string,
		spec_ceil: string,
		spec_floor: string,
		unit: string

	}
	feature_info: {
		companyid: string,
		companyname: string,
		hidden_source_feature_no: string,
		hidden_source_feature_name: string,
		hidden_source_feature_code: string,

	}
	data: {
		hidden_source: string,
		hidden_code: string,
		hidden_name: string,

	}
	result_info: Array<{ check_res_no: string; }>
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	// data: unknown;
	// cookie?: {
	// 	[name: string]: string;
	// } | null;
	// content_type?: string;
	// headers?: {
	// 	[key: string]: string[];
	// };
	// attachment?: string;
	// redirect?: string;
	// status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\王举\学习文件\飞道工作文件\飞道项目\test-js\src\wangju\zj-000004\s003,action_id:' + action_id);

	const { result: code } = await doc_code(action_id, session_id, systemid, 'BM0014321', '', 1);
	const { exec, insert } = operate(action_id, session_id, systemid);
	// 插入数据
	insert('qu_hidden_feat_st', {
		standard_no: code,
		companyid: message.feature_info.companyid,
		companyname: message.feature_info.companyname,
		area_name: message.filter.area_name,
		hidden_source: message.data.hidden_source,
		hidden_code: message.data.hidden_code,
		hidden_name: message.data.hidden_name,
		hidden_source_feature_no: message.feature_info.hidden_source_feature_no,
		hidden_source_feature_name: message.feature_info.hidden_source_feature_name,
		hidden_source_feature_code: message.feature_info.hidden_source_feature_code,
		upper_limit: message.filter.upper_limit,
		down_value: message.filter.down_value,
		target: message.filter.target,
		spec_ceil: message.filter.spec_ceil,
		spec_floor: message.filter.spec_floor,
		plan_type_no: message.filter.plan_type_no,
		plan_type_name: message.filter.plan_type_name,
		plan_cla_attr_no: message.filter.plan_cla_attr_no,
		plan_cla_attr_name: message.filter.plan_cla_attr_name,
		unit: message.filter.unit,
		productid
	});
	if (message.result_info && message.result_info.length > 0) {
		message.result_info.forEach((v) => {
			insert('qu_check_result', {
				standard_no: code,
				check_res_no: v.check_res_no,
				productid

			});
		})
	}

	const res = await exec();
	return res;


	log('Service end path:e:\王举\学习文件\飞道工作文件\飞道项目\test-js\src\wangju\zj-000004\s003,action_id:' + action_id);

}
