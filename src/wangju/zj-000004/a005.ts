import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';
import get from '@dfeidao/atom-web/local/get';

export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// 隐患源特性
	const data = get(fd, 'feature_info') as { hidden_source_feature_name: string };
	console.log(data);
	if (data) {
		fire('zj-000007', 'a001', data.hidden_source_feature_name, 'add');
	} else {
		alert('请先选择隐患源特性')
	}

}
