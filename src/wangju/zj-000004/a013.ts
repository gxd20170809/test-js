import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import p004 from './p004';
import render from '@dfeidao/atom-web/render/render';
import set from '@dfeidao/atom-web/local/set';
import a012 from './a012';

export default async function a013(fd: IFeidaoAiBrowserComponent) {
	// todo
	const chklistok = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]:checked');
	const list = Array.from(chklistok) as HTMLInputElement[];
	if (list.length === 0) {
		alert('请选择一条数据')
	} else {
		const nos = list.map((n) => {
			return n.getAttribute('data-no');
		})
		const dd = get(fd, 'data') as { check_res_no: string }[];
		const arr: Array<{ check_res_no: string }> = [];
		dd.forEach((v) => {
			const no = v.check_res_no;
			const s = nos.includes(no);
			if (!s) {
				arr.push(v);
			}
		});
		render(fd, arr, p004, 'p004', 'inner');
		set(fd, 'data1', arr);
		await a012(fd);
	}
}
