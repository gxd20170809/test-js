import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import render from '@dfeidao/atom-web/render/render';
import p004 from './p004';
import set from '@dfeidao/atom-web/local/set';
import get from '@dfeidao/atom-web/local/get';

export default async function a010(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string }>) {
	// 数据的渲染

	const dd = get(fd, 'data') as Array<{
		check_res_no: string, flag?: boolean
	}>;

	if (!dd) {
		render(fd, data, p004, 'p004', 'inner');
		set(fd, 'data', data);
	} else {
		data.forEach((v) => {
			const no = v.check_res_no;
			const res = dd.filter((ddd) => {
				return ddd.check_res_no === no;
			})
			if (res.length === 0) {
				dd.push(v);
			} else {
				for (let i = 0; i < dd.length; i++) {
					const v1 = dd[i];
					if (v1.check_res_no === no) {
						v1.flag = true;
					}
				}
			}
			render(fd, dd, p004, 'p004', 'inner');
			set(fd, 'data', dd);
		});
	}



}
