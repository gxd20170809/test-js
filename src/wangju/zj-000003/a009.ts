import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a009(fd: IFeidaoAiBrowserComponent, e: Event) {
	// 操作编辑
	const n = e.currentTarget as HTMLDivElement;
	const no = n.getAttribute('data-no');
	fd.data.node.style.display = 'none';
	fire('zj-000005', 'a001', no);

}
