import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
export default async function a001(fd: IFeidaoAiBrowserComponent) {
	// todo
	const page_no = fd.data.params['page_no'] as string || '1';
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v = n.value;
	const n1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v1 = n1.value;
	const res = await nodejs('wangju/zj-000003/s001', {
		filter: {
			hidden_name: v,
			hidden_source_feature_name: v1,
			page_no: Number(page_no),
		}
	});
	render(fd, res, p001, 'p001', 'inner');
}
