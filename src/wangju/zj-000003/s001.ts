import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import { systemid, productid } from '../../atom/config';
interface Message {
	filter: {
		hidden_name: string,
		hidden_source_feature_name: string,
		page_no: number,
		asc?: string[],
		desc?: string[],
	}
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	// data: unknown;
	// cookie?: {
	// 	[name: string]: string;
	// } | null;
	// content_type?: string;
	// headers?: {
	// 	[key: string]: string[];
	// };
	// attachment?: string;
	// redirect?: string;
	// status_code?: number;
}
interface Quhiddenfeatst {
	standard_no: string;
	count: number;
}
// 表名	qu_hidden_feat_st				
// 标题	定量隐患源特性标准				
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// standard_no	标准编号	string	50	√	ⅹ
// companyid	公司编号	string	50	√	ⅹ
// companyname	公司名称	string	50	√	ⅹ
// area_name	区域	string	100	√	ⅹ
// hidden_source	隐患源编号	string	32	√	ⅹ
// hidden_code	隐患源代码	string	32	√	ⅹ
// hidden_name	隐患源名称	string	32	√	ⅹ
// hidden_source_feature_no	隐患源特性编号	string	50	√	ⅹ
// hidden_source_feature_name	隐患源特性名称	string	50	√	ⅹ
// hidden_source_feature_code	隐患源特性代码	string	50	√	ⅹ
// upper_limit	上限	string	50	√	ⅹ
// down_value	下限	string	50	√	ⅹ
// target	目标值	string	50	√	ⅹ
// spec_ceil	规范上限	string	50	√	ⅹ
// spec_floor	规范下限	string	50	√	ⅹ
// plan_type_no	计划类型编号	string	50	√	ⅹ
// plan_type_name	计划类型名称	string	50	√	ⅹ
// plan_cla_attr_no	计划分类属性编号	string	50	√	ⅹ
// plan_cla_attr_name	计划分类属性名称	string	50	√	ⅹ
// unit	单位	string	50	√	ⅹ



export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\王举\学习文件\飞道工作文件\飞道项目\test-js\src\wangju\zj-000003\s001,action_id:' + action_id);
	const pp = query(action_id, session_id, systemid);
	pp.prepare('qu_hidden_feat_st', ['hidden_source_feature_code', 'hidden_source_feature_name', 'plan_cla_attr_name', 'plan_type_name', 'hidden_code', 'hidden_name', 'companyname', 'area_name', 'target', 'standard_no'],
		{ productid, hidden_source_feature_name: { $like: '%' + message.filter.hidden_source_feature_name + '%' }, hidden_name: { $like: '%' + message.filter.hidden_name + '%' } }, 5, message.filter.page_no, message.filter.asc || [], message.filter.desc || []);
	const dt = await pp.exec<Quhiddenfeatst>();
	const data = dt[0];
	const arr = [];
	for (let i = 0; i < data.length; i++) {
		const element = data[i];
		const v = element.standard_no;
		arr.push(v);
	}

	const { prepare: p, exec } = count(action_id, session_id, systemid);
	p('qu_hidden_feat_st', { productid, hidden_source_feature_name: { $like: '%' + message.filter.hidden_source_feature_name + '%' }, hidden_name: { $like: '%' + message.filter.hidden_name + '%' } });
	const [c] = await exec();

	const q = complex_query(action_id, session_id, systemid);
	const d = await q
		.add_field('qu_check_result', 'standard_no', 'standard_no')
		.add_fun('qu_check_result', 'standard_no', 'count', 'count')
		.groupby('qu_check_result', 'standard_no')
		.where_in('qu_check_result', 'standard_no', arr)
		.exec<Quhiddenfeatst>();
	for (let i = 0; i < data.length; i++) {
		const element = data[i];
		const v = element.standard_no;
		const ars = d.filter((value, index, array) => {
			const no = value.standard_no;
			return v === no;
		});
		if (ars.length > 0) {
			const cnv = ars[0];
			element.count = cnv.count;
		} else {
			element.count = 0;
		}

	}
	log('Service end path:e:\王举\学习文件\飞道工作文件\飞道项目\test-js\src\wangju\zj-000003\s003,action_id:' + action_id);
	return {
		data: { data, c }
	}

}