import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo
	const chk_list = fd.data.node.querySelectorAll('[data-id="chk_list"]:checked')

	if (chk_list.length === 0) {
		alert("请选择删除的用户");
	} else {
		const chklist = Array.from(chk_list) as HTMLInputElement[];
		const arr = chklist.map((n) => {
			return n.getAttribute('data-no')
		})
		fire('zj-000010', 'a001', arr);
	};
}
