import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';

import p001 from './p001';
export default async function a002(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo

	const nn = e.target as HTMLDivElement;
	const vv = nn.getAttribute('data-id');
	const ns = fd.data.node.querySelectorAll('[data-asc="desc"]');

	Array.from(ns).forEach((value: HTMLDivElement) => {
		value.setAttribute('class', 'iconfont cursor-p icon-shangxia color-3 font14 pos-a');
	})
	const flag = nn.getAttribute('data-flag');
	const asc: string[] = [];
	const desc: string[] = [];
	if (flag === "1") {
		//升序
		nn.setAttribute('data-flag', '0');
		nn.setAttribute('class', 'iconfont cursor-p icon-shang color-3 font14 pos-a');
		asc.push(vv)
	} else {
		//降序
		nn.setAttribute('data-flag', '1');
		nn.setAttribute('class', 'iconfont cursor-p icon-xia color-3 font14 pos-a');

	}
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const n1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v1 = n1.value;
	const res = await nodejs('wangju/zj-000003/s001', {
		filter: {
			hidden_name: v,
			hidden_source_feature_name: v1,
			page_no: 1,
			asc,
			desc
		}

	})
	render(fd, res, p001, 'p001', 'inner');

}
