import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a008(fd: IFeidaoAiBrowserComponent, e: Event) {
	//删除
	// currentTarget 找到绑定事件的节点 
	//Target      鼠标单击的节点
	console.log(e);
	const n = e.currentTarget as HTMLDivElement;
	const no = n.getAttribute('data-no');
	fire('zj-000010', 'a001', [no]);
}
