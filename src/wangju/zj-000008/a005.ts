import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import set_nodes_cls from '@dfeidao/atom-web/ui/set-nodes-cls';
import set from '@dfeidao/atom-web/local/set';

export default async function a005(fd: IFeidaoAiBrowserComponent, e: Event) {
	// e.target 获取当前节点
	// e.currentTarget 获取当前节点的父节点

	const d = e.currentTarget as HTMLDivElement;
	const nodes = fd.data.node.querySelectorAll('[data-name="hidden"]');
	const ns = Array.from(nodes) as HTMLDivElement[];
	set_nodes_cls(ns, "bg-select", false);
	set_node_cls(d, "bg-select", true);
	const v = d.getAttribute('data-v');
	const data = JSON.parse(v);
	set(fd, 'data', data);
}
