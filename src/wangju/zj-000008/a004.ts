import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
import page from "@dfeidao/fd-w000010"
export default async function a004(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {
	// todo
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const pn = a.page_no;
	const res = await nodejs<{ data1: Array<[]>; t: number }>('wangju/zj-000008/s001', {
		filter: {
			hidden_name: v,

		},
		pn,
	});
	render(fd, res, p001, 'p001', 'inner');
	const page = fd.data.node.querySelector<page>('[data-id="page"]');
	page.setAttribute('page-no', pn + '');
}
