<!--  详情“定量标准” start -->
<div class="display-n" data-id="edit">
	<div class="ht40 b-box" style="padding-left:10px;border-bottom:1px solid #eaedf2;">
		<span class="font12 color-6 ht40 cursor-p" data-feidao-actions='click:a002'
			style="display:inline-flex;align-items:center;">
			<i class="iconfont icon-fanhui color-theme font22" style="margin-right:10px;"></i>
			返回
		</span>
		<span class="font12 color-6 ht40 cursor-p" style="display:inline-flex;align-items:center;margin-left:20px;"
			data-feidao-actions='click:a014'>
			<i class="iconfont icon-keep color-theme font20" style="margin-right:10px;"></i>
			保存
		</span>
	</div>
	<div class="ht40" style="display:flex;align-items:center;">
		<span class="display-b bg-theme" style="width:3px;height:18px;"></span>
		<span class="font12 color-6" style="margin-left:10px;">特性标准</span>
	</div>
	<!-- //dot模板start -->
	<div style="margin-left:20px;" data-feidao-presentation='p001'>
	</div>
	<!-- //dot模板ent -->
	<div class="ht40" style="display:flex;align-items:center;">
		<span class="display-b bg-theme" style="width:3px;height:18px;"></span>
		<span class="font12 color-6" style="margin-left:10px;">特性关联结果</span>
		<i class="iconfont icon-icon02 color-theme font20 vertical-a-m" style="margin-left:20px;"
			data-feidao-actions='click:a008'></i><span class="color-6 font16" style="margin-left:5px;">新增</span><i
			class="iconfont icon-jian color-theme font20 vertical-a-m" style="margin-left:20px;"
			data-feidao-actions='click:a012'></i><span class="color-6 font16" style="margin-left:5px;">删除</span>
	</div>

	<div class="p20" style="width:70%">
		<div class="display-">
			<div class="t_title">
				<span class="display-ib p5 border-r-1 text-c b-box" style="width:5%;">
					<input type="checkbox" data-id="checkbox_all" data-feidao-actions='click:a010' style="width:25px;">
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:20%;">检查结果编号
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:23%;">检查结果描述
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:10%;">是否异常
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:14%;">单位
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:14%;">检查上限
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:14%;">检查下限
					<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
				</span>
			</div>
			<!--表头 end-->
			<!-- 表体 start -->
			<div class="t_body display-" data-feidao-presentation='p002'>

			</div>
		</div>
	</div>
</div>