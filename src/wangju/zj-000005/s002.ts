import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import operate from '@dfeidao/atom-nodejs/db/operate';
import query from '@dfeidao/atom-nodejs/db/query';
import { systemid, productid } from '../../atom/config';

interface Message {
	standard_no: string
	filter: {
		plan_type_no: string,
		plan_type_name: string,
		plan_cla_attr_no: string,
		plan_cla_attr_name: string,
		area_name: string,
		upper_limit: string,
		down_value: string,
		target: string,
		spec_ceil: string,
		spec_floor: string,
		unit: string

	}
	feature_info: {
		companyid: string,
		companyname: string,
		hidden_source_feature_no: string,
		hidden_source_feature_name: string,
		hidden_source_feature_code: string,

	}
	data: {
		hidden_source: string,
		hidden_code: string,
		hidden_name: string,

	}
	result_info: Array<{ check_res_no: string; }>
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

// interface IWebResult {
// 	data: unknown;
// 	cookie?: {
// 		[name: string]: string;
// 	} | null;
// 	content_type?: string;
// 	headers?: {
// 		[key: string]: string[];
// 	};
// 	attachment?: string;
// 	redirect?: string;
// 	status_code?: number;
// }

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders) {
	log('Service begin path:e:\王举\学习文件\飞道工作文件\项目\test-js\src\wangju\zj-000005\s002,action_id:' + action_id);


	const data = query(action_id, session_id, systemid);
	data.prepare('qu_check_result', ['_id'],
		{ productid, standard_no: message.standard_no }, 200, 1, [], []);
	const [d] = await data.exec<{ _id: string; }>();


	const { exec, update, insert, del } = operate(action_id, session_id, systemid);
	update('qu_hidden_feat_st', { productid, standard_no: message.standard_no }, {
		companyid: message.feature_info.companyid,
		companyname: message.feature_info.companyname,
		area_name: message.filter.area_name,
		hidden_source: message.data.hidden_source,
		hidden_code: message.data.hidden_code,
		hidden_name: message.data.hidden_name,
		hidden_source_feature_no: message.feature_info.hidden_source_feature_no,
		hidden_source_feature_name: message.feature_info.hidden_source_feature_name,
		hidden_source_feature_code: message.feature_info.hidden_source_feature_code,
		upper_limit: message.filter.upper_limit,
		down_value: message.filter.down_value,
		target: message.filter.target,
		spec_ceil: message.filter.spec_ceil,
		spec_floor: message.filter.spec_floor,
		plan_type_no: message.filter.plan_type_no,
		plan_type_name: message.filter.plan_type_name,
		plan_cla_attr_no: message.filter.plan_cla_attr_no,
		plan_cla_attr_name: message.filter.plan_cla_attr_name,
		unit: message.filter.unit,
		productid
	});
	d.forEach((e) => {
		del('qu_check_result', { _id: e._id })
	});
	if (message.result_info && message.result_info.length > 0) {
		message.result_info.forEach((v) => {
			insert('qu_check_result', { productid, standard_no: message.standard_no, check_res_no: v.check_res_no, });
		})
	}

	const res = await exec();
	log('Service end path:e:\王举\学习文件\飞道工作文件\项目\test-js\src\wangju\zj-000005\s002,action_id:' + action_id);
	return res
}
