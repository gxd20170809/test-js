import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a002(fd: IFeidaoAiBrowserComponent) {
	// 返回
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="edit"]');
	set_node_cls(n, 'display-n', true);
	fire('zj-000003', 'a006')
}
