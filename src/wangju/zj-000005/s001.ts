import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import query from '@dfeidao/atom-nodejs/db/query';
import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import { systemid, productid } from '../../atom/config';
interface Message {

	standard_no: string
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string[];
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}
// 表名	qu_check_result				
// 标题	定量检查结果				
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// standard_no	标准编号	string	50	√	ⅹ
// check_res_no	检查结果编号	string	50	√	ⅹ

// 表名	hidden_ck_res_set				
// 标题	隐患源检查结果选择集				
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// companyid	公司编号	string	50	√	ⅹ
// companyname	公司名称	string	50	√	ⅹ
// factory_no	工厂编号	string	50	√	ⅹ
// factory_name	工厂名称	string	50	√	ⅹ
// hidden_source_feature_no	隐患源特性编号	string	50	√	ⅹ
// hidden_source_feature_name	隐患源特性名称	string	50	√	ⅹ
// hidden_source_feature_code	隐患源特性代码	string	50	√	ⅹ
// is_abnormal	是否异常	int	16	√	ⅹ
// unusual_level	异常等级	string	32	√	ⅹ
// control_data	控制数据	string	32	√	ⅹ
// spec_ceil	范围上限	string	50	√	ⅹ
// spec_floor	范围下限	string	50	√	ⅹ
// unit	单位	string	50	√	ⅹ
// pre_head_no	预负责人编号	string	50	√	ⅹ
// pre_head_name	预负责人姓名	string	32	√	ⅹ
// instance_pic	实例图片	document	50	√	ⅹ
// sub_inf_content	报送信息内容	string	50	√	ⅹ
// check_res_no	检查结果编号	string	50	√	ⅹ
// check_res_des	检查结果描述	string	50	√	ⅹ



export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\王举\学习文件\飞道工作文件\项目\test-js\src\wangju\zj-000005\s001,action_id:' + action_id);

	const pp = query(action_id, session_id, systemid);
	pp.prepare('qu_hidden_feat_st', ['standard_no',
		'companyid',
		'companyname',
		'area_name',
		'hidden_source',
		'hidden_code',
		'hidden_name',
		'hidden_source_feature_no',
		'hidden_source_feature_name',
		'hidden_source_feature_code',
		'upper_limit',
		'down_value',
		'target',
		'spec_ceil',
		'spec_floor',
		'plan_type_no',
		'plan_type_name',
		'plan_cla_attr_no',
		'plan_cla_attr_name',
		'unit',
	],
		{ productid, standard_no: message.standard_no }, 1, 1, [], []);
	const [res] = await pp.exec();

	const q = complex_query(action_id, session_id, systemid);
	const d = await q
		.add_field('qu_check_result', 'check_res_no', 'check_res_no')
		.add_field('qu_check_result', 'standard_no', 'standard_no')
		.add_field('hidden_ck_res_set', 'is_abnormal', 'is_abnormal')
		.add_field('hidden_ck_res_set', 'check_res_des', 'check_res_des')
		.add_field('hidden_ck_res_set', 'unit', 'unit')
		.add_field('hidden_ck_res_set', 'spec_ceil', 'spec_ceil')
		.add_field('hidden_ck_res_set', 'spec_floor', 'spec_floor')
		.inner_join('qu_check_result', 'hidden_ck_res_set', ['check_res_no', 'check_res_no'])
		.where_eq('qu_check_result', 'standard_no', message.standard_no)
		.where_eq('qu_check_result', 'productid', productid)
		.exec();

	log('Service end path:e:\王举\学习文件\飞道工作文件\项目\test-js\src\wangju\zj-000005\s001,action_id:' + action_id);
	return {
		data: {
			res, d
		}
	} as IWebResult;
}
