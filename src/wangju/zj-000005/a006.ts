import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// 组件9传入隐患源编号
	const info = get(fd, 'info')
	const render_data = get(fd, 'render_data')
	if (info) {
		fire('zj-000009', 'a001', 'edit', info);
	} else {
		fire('zj-000009', 'a001', 'edit', render_data[0]);
	}
}
