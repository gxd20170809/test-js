import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a008(fd: IFeidaoAiBrowserComponent) {
	// 打开索性关联结果弹窗
	const data = get(fd, 'feature_info') as { hidden_source_feature_name: string };
	const render_data = get(fd, 'render_data')
	console.log(data);
	if (data) {
		fire('zj-000007', 'a001', data.hidden_source_feature_name, 'edit');
	} else {
		fire('zj-000007', 'a001', render_data[0].hidden_source_feature_name, 'edit');
	}
}
