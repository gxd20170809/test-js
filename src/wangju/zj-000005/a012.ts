import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import render from '@dfeidao/atom-web/render/render';
import set from '@dfeidao/atom-web/local/set';
import p002 from './p002';
import a010 from './a010';

export default async function a012(fd: IFeidaoAiBrowserComponent) {
	// todo
	const chklistok = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]:checked');
	const list = Array.from(chklistok) as HTMLInputElement[];
	if (list.length === 0) {
		alert('请选择一条数据')
	} else {
		const nos = list.map((n) => {
			return n.getAttribute('data-no');
		})
		const dd = get(fd, 'data') as { check_res_no: string }[];
		const arr: Array<{ check_res_no: string }> = [];
		dd.forEach((v) => {
			const no = v.check_res_no;
			const s = nos.includes(no);
			if (!s) {
				arr.push(v);
			}
		});
		render(fd, arr, p002, 'p002', 'inner');
		const chklistok = fd.data.node.querySelector<HTMLInputElement>('[data-id="checkbox_all"]');
		chklistok.checked = false;
		set(fd, 'data', arr);
		await a010(fd);
	}
}
