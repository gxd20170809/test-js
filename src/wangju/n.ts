import { ICommonParams, IHeaders } from '@dfeidao/nodejs/interfaces';
import html from './html';
import s from './ns';

import init from '@dfeidao/nodejs/page';

/// FEIDAO IMPCOMPONENTS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)
import c0 from './zj-000001/n';
import c1 from './zj-000002/n';
import c2 from './zj-000003/n';
import c3 from './zj-000004/n';
import c4 from './zj-000005/n';
import c5 from './zj-000006/n';
import c6 from './zj-000007/n';
import c7 from './zj-000008/n';
import c8 from './zj-000009/n';
import c9 from './zj-000010/n';

/// FEIDAO IMPCOMPONENTS END


/// FEIDAO IMPACTIONS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)
import na001 from './na001';

/// FEIDAO IMPACTIONS END

export default async function main(url: string, msg: ICommonParams, headers: IHeaders) {

	/// FEIDAO ACTIONS BEGIN
	/// 请不要修改下面的代码哟(๑•ω•๑)

	const actions = { na001 };

	/// FEIDAO ACTIONS END


	const res = await init(html, url, msg, headers, s, actions
		/// FEIDAO COMPONENTS BEGIN
		/// 请不要修改下面的代码哟(๑•ω•๑)
		, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9

		/// FEIDAO COMPONENTS END
	);

	const html_str = `<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="renderer" content="webkit">
		<title>wangju</title>
		<link inline rel="stylesheet" type="text/css" href="./css/iconfont.css">
		<link inline href="https://cdn.jsdelivr.net/npm/feidao-css/feidao.css" type="text/css" rel="stylesheet">
		<link inline rel="stylesheet" type="text/css" href="./css/daoke.css">
		<script src="https://cdn.jsdelivr.net/npm/@webcomponents/webcomponentsjs/custom-elements-es5-adapter.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/@webcomponents/webcomponentsjs/webcomponents-loader.js"></script>
		<script src="./dist/js/feidao.js"></script>
		<script type="text/javascript">
			window.addEventListener('WebComponentsReady', function () {
				var t = document.createElement('script');
				t.src = './dist/js/wangju.js';
				document.head.appendChild(t);
			});
		</script>
	</head>
	<body>
	${html.toString()}
	</body>
</html>
	`;
	const html_obj = { data: html_str };
	if (typeof res === 'object' && Object.keys(res).length > 0) {
		const result = Object.assign({}, html_obj, res);
		return result;
	}
	return html_obj;
}
