export default `<!-- 定量选择“检查结果”弹窗 start -->
<div data-id="add"
	class="bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12 display-n"
	style="z-index:120;">
	<div style="width:820px;padding-top:100px;padding-bottom:50px;margin:0 auto;">
		<div class="bg-f overflow-h text-l" style="border-radius:4px">
			<div class="font14 bg-theme color-f ht40 l-ht40"
				style="padding:0 10px;border-top-left-radius: 4px;border-top-right-radius: 4px">
				<i class="icon iconfont icon-cuo fr text-c cursor-p font12" title="close"
					data-feidao-actions="click:a002"></i>检查结果
			</div>
			<div>
				<div class="ht50 l-ht50" style="padding:30px 30px 0 20px;">
					<input type="button" class="btn fr " data-feidao-actions='click:a005'
						style="width:50px;line-height:20px;" value="确定">
				</div>
				<div class="p20" style="min-height:200px">
					<!-- 表格 start -->
					<!--表头 start -->
					<div class="t_title b-box" style="width:770px;">
						<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
							<input type="checkbox" style="width:25px;" data-feidao-actions="click:a003"
								data-id="checkbox_all">
						</span>
						<span class="display-ib p5 border-r-1 text-c b-box" style="width:150px;">检查结果编号</span><span
							class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:200px;">检查结果描述
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:80px;">是否异常
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:120px;">单位
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:120px;">检查上限
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:100px;">检查下限
						</span>
					</div>
					<!--表头 end-->
					<!-- 表体 start -->
					<div class="t_body display-" style="width:770px;" data-feidao-presentation='p001'>

					</div>
					<!-- 表体 end -->
					<!-- 表格 end -->
				</div>
			</div>
		</div>
	</div>
</div>`;
