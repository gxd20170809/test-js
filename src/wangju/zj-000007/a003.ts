import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a003(fd: IFeidaoAiBrowserComponent) {
	// 表头全选
	const chkAll = fd.data.node.querySelector<HTMLInputElement>('[data-id="checkbox_all"]');
	const chk = chkAll.checked;
	const chklist = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]');
	const lists = Array.from(chklist) as HTMLInputElement[];
	lists.forEach((value) => {
		value.checked = chk;
	})
	console.log(chk);
}
