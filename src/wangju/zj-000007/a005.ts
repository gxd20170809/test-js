import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from './a002';

export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// 回调
	const nodes = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]:checked');
	const lists = Array.from(nodes) as HTMLInputElement[];
	if (lists.length === 0) {
		alert('请选择一条数据');
	} else {
		const d = lists.map((n) => {
			const v = JSON.parse(n.getAttribute('data-v'))
			return v
		})

		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000004', 'a010', d);
			fire('zj-000004', 'a012');
		} else {
			//跳到编辑页面
			fire('zj-000005', 'a009', d);
			fire('zj-000004', 'a012');
		}
		await a002(fd);
	}


}
