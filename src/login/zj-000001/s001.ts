import log from '@dfeidao/atom-nodejs/logger/log';
import msg from "@dfeidao/atom-nodejs/msg";
import { IncomingHttpHeaders } from 'http';

interface Message {
	usercode: string;
	userpw: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:f:\培训\项目\feidao-4.6\demo\src\login\zj-000001\s001,action_id:' + action_id);
	const result = await msg.user_login(action_id, session_id, message.usercode, message.userpw);
	log('Service end path:login/zj-000001/s001,2,action_id:' + action_id);

	log('Service end path:f:\培训\项目\feidao-4.6\demo\src\login\zj-000001\s001,action_id:' + action_id);

	return {
		data: result,
		cookie: {
			sessionid: result.sessionID
		}
	};
}
