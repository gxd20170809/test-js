import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import p002 from './p002';

export default async function a009(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string }>) {
	// 索性关联列表打勾
	const dd = get(fd, 'data') as Array<{
		check_res_no: string, flag?: boolean
	}>;

	if (!dd) {
		render(fd, data, p002, 'p002', 'inner');
		set(fd, 'data', data);
	} else {
		data.forEach((v) => {
			const no = v.check_res_no;
			const res = dd.filter((ddd) => {
				return ddd.check_res_no === no;
			});
			if (res.length === 0) {
				dd.push(v);
			} else {
				for (let i = 0; i < dd.length; i++) {
					const v1 = dd[i];
					if (v1.check_res_no === no) {
						v1.flag = true;
					}
				}
			}
			render(fd, dd, p002, 'p002', 'inner');
			set(fd, 'data', dd);
		});
	}
}
