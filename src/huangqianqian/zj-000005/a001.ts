import get_files_url from '@dfeidao/atom-web/file/get-file-url';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import fire from '@dfeidao/atom-web/msg/fire';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import { productid } from '../../atom/config';
import p001 from './p001';
import p002 from './p002';

export default async function a001(fd: IFeidaoAiBrowserComponent, standard_no: string) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="edit"]');
	set_node_cls(n, 'display-n', false);
	console.log(standard_no);
	const res = await nodejs<{ res: Array<{ plan_cla_attr_no: string }>; d: Array<{}>; }>('huangqianqian/zj-000005/s001', {
		standard_no
	});


	if (res.res.length > 0) {
		const dd = await fire('zj-000004', 'a015');
		const data = res.res[0];
		const r = get_files_url(productid, 'unit', data);
		const plan_cla_attr_no = data.plan_cla_attr_no;
		let plan_type = [] as Array<{}>;
		if (plan_cla_attr_no) {
			plan_type = await nodejs<Array<{}>>('huangqianqianw/zj-000004/s002', {
				filter: {
					plan_cla_attr_no
				}
			});
		}
		const render_data = Object.assign({ plan_type }, r, dd);
		console.log("cheshisssss", [render_data]);
		render(fd, [render_data], p001, 'p001', 'inner');
		render(fd, res.d, p002, 'p002', 'inner');
		set(fd, 'render_data', [render_data]);
		set(fd, 'standard_no', standard_no);
		set(fd, 'res', res.res);
	}




}
