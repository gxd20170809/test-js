import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a005(fd: IFeidaoAiBrowserComponent, data: { hidden_source: string, hidden_name: string }) {
	// 隐患源弹窗回调值
	console.log("a005", data);
	const hidden_source = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source"]');
	hidden_source.innerHTML = data.hidden_source;
	const hidden_name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	hidden_name.value = data.hidden_name;
	set(fd, 'info', data);
}
