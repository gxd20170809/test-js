import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p003 from './p003';

export default async function a013(fd: IFeidaoAiBrowserComponent) {
	// 渲染计划类型下拉框
	const n = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const v = n.value;
	const res = await nodejs<{
		data1: Array<{}>
	}>('huangqianqian/zj-000004/s002', {
		filter: {
			plan_cla_attr_no: v
		}
	});
	console.log(res);
	render(fd, res.data1, p003, 'p003', 'inner');
}
