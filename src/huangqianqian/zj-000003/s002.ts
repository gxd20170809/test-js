import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';

interface Message {
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string[];
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:F:\test\test-js\src\huangqianqian\zj-000003\s002,action_id:' + action_id);

	log('Service end path:F:\test\test-js\src\huangqianqian\zj-000003\s002,action_id:' + action_id);
	return {} as IWebResult;
}
