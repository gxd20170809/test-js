import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo
	const chk_list = fd.data.node.querySelectorAll('[data_id="chk_list"]:checked');
	const node = Array.from(chk_list) as HTMLInputElement[];
	if (node.length === 0) {
		alert('请选择一行数据');

	} else {
		const nos = node.map((n) => {
			return n.getAttribute("data_no");
		});
		fire('zj-000010', 'a001', nos);
	}

}



