export default `{{~it.data:value:index}}
<div class="tableRow ">
	<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
		<input type="checkbox" data-id="chk_list" data-no="{{=value.standard_no}}" data-feidao-actions='click:a004'>
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h text-c color-theme"
		style="width:11%;" title="">{{=value.hidden_source_feature_code}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h" style="width:11%;"
		title="">{{=value.hidden_source_feature_name}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:10%;"
		title="">{{=value.hidden_code}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h" style="width:10%;"
		title="">{{=value.hidden_name}}
	</span><img src="http://a.hiphotos.baidu.com/image/pic/item/4610b912c8fcc3cea3aba0619c45d688d43f207c.jpg" alt=""
		style="width: 11.75%;height: 20px;">
	</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h" style="width:11%;"
		title="">{{=value.companyname}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:8%;"
		title="">{{=value.area_name}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:8%;"
		title="">{{=value.target}}</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
		style="width:8%;" title="">{{=value.count}}</span>
	<span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:8%;" title="">
		<i class="iconfont cursor-p icon-xiugai color-theme display-ib" style="width:30px;"></i>
		<i class="iconfont icon-shanchu1 cursor-p display-ib color-red" style="margin:0 10px;" title="删除"
			data-feidao-actions='click:a008' data-no="{{=value.standard_no}}"></i>
	</span>
</div>
{{~}}
{{?it.data.length===0}}
<!--筛选结果为空的蒙版 start-->
<div class="none display-">
	<i class="icon iconfont icon-wushuju display-ib text-c vertical-a-t color-9" style="font-size:100px;"></i>
	<div class="font22 color-9" style="height:50px;line-height:50px;">当前筛选条件下，没有匹配的数据</div>
</div>

<!--筛选结果为空的蒙版 end-->
{{??}}
<!--分页 start-->
<div class="page">
	<fd-w000009 key="page_no" size=5 total={{=it.c}} max-btn-num=5 show-first show-total show-last lang="zh_CN"
		show-goto>
	</fd-w000009>
</div>

{{?}}
<!--分页 end-->`;
