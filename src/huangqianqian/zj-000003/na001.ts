import { IFeidaoAiNodejsComponent } from '@dfeidao/atom-nodejs/interfaces';
import nodejs from '@dfeidao/atom-nodejs/msg/nodejs';
import render from '@dfeidao/atom-nodejs/render/render';
import p001 from './p001';
export default async function na001(fd: IFeidaoAiNodejsComponent) {
	// todo
	const page_no = fd.data.params['page-no'] as string || '1';
	const res = await nodejs(fd.data.actionid, fd.data.sessionid, 'huangqianqian/zj-000003/s001', {
		filter: {
			hidden_name: "",
			hidden_source_feature_name: "",
			page_no: Number(page_no)
		}
	});

	render(fd.data.node, res, p001, 'p001');
}
