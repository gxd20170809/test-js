import init from '@dfeidao/web/component';

import s from './s';

/// FEIDAO IMPACTIONS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)
import a001 from './a001';
import a002 from './a002';
import a003 from './a003';

/// FEIDAO IMPACTIONS END

export default function main(url: string, query: {}) {
	/// FEIDAO ACTIONS BEGIN
	/// 请不要修改下面的代码哟(๑•ω•๑)
	const actions = { a001, a002, a003 };

	/// FEIDAO ACTIONS END
	return init('zj-000010', s, actions, url, query);
}
