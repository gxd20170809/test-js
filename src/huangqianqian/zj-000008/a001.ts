import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="add"]');
	n.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12');
	set(fd, "flag", flag);
}
