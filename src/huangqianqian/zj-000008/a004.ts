import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import page from "@dfeidao/fd-w000010";
import p001 from './p001';
export default async function a004(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {
	// todo
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const pn = a.page_no;
	const res = await nodejs<{ data1: Array<[]>; t: number }>('huangqianqian/zj-000008/s001', {
		filter: {
			hidden_name: v

		},
		pn
	});
	render(fd, res, p001, 'p001', 'inner');
	const p = fd.data.node.querySelector<page>('[data-id="page"]');
	p.setAttribute('page-no', pn + '');
}
