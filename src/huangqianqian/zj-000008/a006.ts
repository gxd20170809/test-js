import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from './a002';
export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// todo
	console.log(fd);
	const data = get(fd, 'data');

	if (data) {
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000004', 'a008', data);
		}
		// else {

		// }

		await a002(fd);
	} else {
		alert('请选择一列数据');
	}
}
