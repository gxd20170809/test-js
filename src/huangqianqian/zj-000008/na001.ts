import { IFeidaoAiNodejsComponent } from '@dfeidao/atom-nodejs/interfaces';
import nodejs from '@dfeidao/atom-nodejs/msg/nodejs';
import render from '@dfeidao/atom-nodejs/render/render';
import p001 from './p001';

export default async function na001(fd: IFeidaoAiNodejsComponent) {
	// todo

	const res = await nodejs<{ data1: Array<[]>; t: number }>(fd.data.actionid, fd.data.sessionid, 'huangqianqian/zj-000008/s001', {
		filter: {
			hidden_name: ""
		},
		pn: 1
	});


	console.log(res);

	render(fd.data.node, res, p001, 'p001');
}
