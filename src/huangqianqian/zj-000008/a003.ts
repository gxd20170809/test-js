import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import page from "@dfeidao/fd-w000010";
import p001 from './p001';
// tslint:disable-next-line:class-name
interface total extends Event {
	page_no: number;
}

export default async function a003(fd: IFeidaoAiBrowserComponent, a: { page_no: number }, e: total) {
	// todo
	const page_no = fd.data.params['page-no'] as string || '1';
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const res = await nodejs('huangqianqian/zj-000008/s001', {
		filter: {
			hidden_name: v,
			page_no1: Number(page_no)
		}
	});
	console.log(v);
	console.log(res);
	render(fd, res, p001, 'p001', 'inner');

	const p = fd.data.node.querySelector('[data-id="paging"]');
	p.setAttribute('page-no', page_no + '');
}
