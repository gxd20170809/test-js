import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	filter: {
		hidden_name: string

	};
	pn: number;
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	// data: unknown;
	// cookie?: {
	// 	[name: string]: string;
	// } | null;
	// content_type?: string;
	// headers?: {
	// 	[key: string]: string[];
	// };
	// attachment?: string;
	// redirect?: string;
	// status_code?: number;
}

// 表名	hazard_source
// 标题	隐患源
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// productid	产品ID	string	50	√	ⅹ
// hidden_source	隐患源编号	string	32	√	ⅹ
// duty_department_no	责任部门编号	string	50	√	ⅹ
// duty_department	责任部门名称	string	50	√	ⅹ
// responsible	责任人名称	string	50	√	ⅹ
// responsible_task_no	任务责任人编号	string	50	√	ⅹ
// hidden_code	隐患源代码	string	32	√	ⅹ
// hidden_name	隐患源名称	string	32	√	ⅹ
// control_level	管控级别	string	32	√	ⅹ
// control_grade	管控层级	string	32	√	ⅹ
// assess_level	评价级别	string	32	√	ⅹ
// engineer_skill	工程技术	string	128	√	ⅹ
// manage_measure	管理措施	string	128	√	ⅹ
// train_education	培训教育	string	128	√	ⅹ
// emergency_measures	应急处置措施	string	128	√	ⅹ
// personal_protect	个体防护	string	128	√	ⅹ

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:F:\test\test-js\src\huangqianqian\zj-000008\s001,action_id:' + action_id);

	const { prepare, exec } = count(action_id, session_id, systemid);
	prepare('hazard_source', { productid, hidden_name: { $like: '%' + message.filter.hidden_name + '%' } });
	const [t] = await exec();

	const pp = query(action_id, session_id, systemid);
	pp.prepare('hazard_source', ['hidden_name', 'hidden_source', 'hidden_code', 'control_level', 'control_grade', 'assess_level'], { productid, hidden_name: { $like: '%' + message.filter.hidden_name + '%' } }, 2, message.pn, [], []);

	const [data1] = await pp.exec();

	const d = data1.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	});

	log('Service end path:F:\test\test-js\src\huangqianqian\zj-000008\s001,action_id:' + action_id);
	return { data1: d, t };
}
