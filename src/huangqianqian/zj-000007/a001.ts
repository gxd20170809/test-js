import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';

export default async function a001(fd: IFeidaoAiBrowserComponent, no: string, flag: string) {
	// todo

	const data = await nodejs<Array<{}>>('huangqianqian/zj-000007/s001', {
		no
	});
	console.log(data);
	render(fd, data, p001, 'p001', 'inner');
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="add"]');

	set(fd, 'flag', flag);
	n.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12');
}
