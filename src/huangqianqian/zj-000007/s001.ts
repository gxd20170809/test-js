import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';

import query from '@dfeidao/atom-nodejs/db/query';
import { productid } from '../../atom/config';
import { systemid } from '../../atom/config';

interface Message {

	no: string;
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

// interface IWebResult {
// 	data: unknown;
// 	cookie?: {
// 		[name: string]: string;
// 	} | null;
// 	content_type?: string;
// 	headers?: {
// 		[key: string]: string;
// 	};
// 	attachment?: string;
// 	redirect?: string;
// 	status_code?: number;
// }

// 表名	hidden_ck_res_set
// 标题	隐患源检查结果选择集
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// companyid	公司编号	string	50	√	ⅹ
// companyname	公司名称	string	50	√	ⅹ
// factory_no	工厂编号	string	50	√	ⅹ
// factory_name	工厂名称	string	50	√	ⅹ
// hidden_source_feature_no	隐患源特性编号	string	50	√	ⅹ
// hidden_source_feature_name	隐患源特性名称	string	50	√	ⅹ
// hidden_source_feature_code	隐患源特性代码	string	50	√	ⅹ
// is_abnormal	是否异常	int	16	√	ⅹ
// unusual_level	异常等级	string	32	√	ⅹ
// control_data	控制数据	string	32	√	ⅹ
// spec_ceil	范围上限	string	50	√	ⅹ
// spec_floor	范围下限	string	50	√	ⅹ
// unit	单位	string	50	√	ⅹ
// pre_head_no	预负责人编号	string	50	√	ⅹ
// pre_head_name	预负责人姓名	string	32	√	ⅹ
// instance_pic	实例图片	document	50	√	ⅹ
// sub_inf_content	报送信息内容	string	50	√	ⅹ
// check_res_no	检查结果编号	string	50	√	ⅹ
// check_res_des	检查结果描述	string	50	√	ⅹ

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders) {
	log('Service begin path:f:\test\test-js\src\huangqianqian\zj-000007\s001,action_id:' + action_id);

	const pp = query(action_id, session_id, systemid);
	pp.prepare('hidden_ck_res_set', ['check_res_no', 'check_res_des', 'unit', 'is_abnormal', 'spec_ceil', 'spec_floor', 'hidden_source_feature_no'], { productid, hidden_source_feature_no: message.no }, 200, 1, [], []);

	const [data] = await pp.exec();

	const res = data.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	});
	log('Service end path:f:\test\test-js\src\huangqianqian\zj-000007\s001,action_id:' + action_id);
	return {
		data: res
	};
}
