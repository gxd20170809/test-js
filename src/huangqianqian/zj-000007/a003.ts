import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a003(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 点击表头的框，实现全选和反选
	const chkall = fd.data.node.querySelector<HTMLInputElement>('[data-id="checkbox_all"]');
	const chk = chkall.checked;
	const chklist = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]');
	const lists = Array.from(chklist) as HTMLInputElement[];
	lists.forEach((value) => {
		value.checked = chk;
	});

	console.log(chk);
}
