import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from './a002';


export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	const list_chk = fd.data.node.querySelectorAll('[data-id="chk_list"]:checked');
	console.log(list_chk);
	const list = Array.from(list_chk) as HTMLInputElement[];
	if (list.length === 0) {
		alert('请选择至少一行数据');
	} else {
		const d = list.map((n) => {
			// const v = JSON.parse();
			return n.getAttribute('data-no');
		});
		const a = d as string[];
		console.log(a);
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000004', 'a010', d);
			fire('zj-000004', 'a012');
		} else {
			// 跳回编辑页面
		}
		await a002(fd);
	}




}
