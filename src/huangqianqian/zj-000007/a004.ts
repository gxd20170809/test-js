import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 判断全选和反全选
	const chklist = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]');

	const chklistok = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="chk_list"]:checked');
	const chkAll = fd.data.node.querySelector<HTMLInputElement>('[data-id="checkbox_all"]');

	if (Array.from(chklist).length === Array.from(chklistok).length) {
		chkAll.checked = true;
	} else {
		chkAll.checked = false;
	}
}
