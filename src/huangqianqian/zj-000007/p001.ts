export default `{{~it:value:index}}
<div class="tableRow b-box">

	<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
		<input type="checkbox" data-id="chk_list" data-no="{{=value.standard_no}}" data-feidao-actions='click:a004'
			data-v="{{=value.v}}">
	</span>
	<span class="display-ib p5 border-r-1 text-c b-box" style="width:150px;">
		{{=value.check_res_no}}
	</span>
	<span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h" style="width:200px;" title="
	{{=value.check_res_des}}">{{=value.check_res_des}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:80px;"
		title="{{?value.is_abnormal===1}}是{{??}}否{{?}}">{{?value.is_abnormal===1}}是{{??}}否{{?}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:120px;"
		title="{{=value.unit}}">{{=value.unit}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:120px;"
		title="{{=value.spec_ceil}}">{{=value.spec_ceil}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:100px;"
		title="{{=value.spec_floor}}">{{=value.spec_ceil}}
	</span>
</div>
{{~}}`;
