import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';


export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// 判断选择隐患源特性
	const data = get(fd, 'feature_info') as {
		hidden_source_feature_no: string
	};
	if (data) {
		fire('zj-000007', 'a001', data.hidden_source_feature_no, 'add');
	} else {
		alert('请先选择隐患源特性');
	}
	fire('zj-000007', 'a001');
}
