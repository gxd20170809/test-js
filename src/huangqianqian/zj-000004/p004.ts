export default `{{~ it:value:index}}
<div class="tableRow">
	<span class="display-ib p5 border-r-1 text-c b-box" style="width:5%;">
		<input type="checkbox" {{?value.flag===true}} checked="checked" {{?}}data-feidao-actions='click:a012'
			data-id="list" data-no="{{=value.check_res_no}}">
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h text-c color-theme"
		style="width:20%;" title="">{{=value.check_res_no}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h" style="width:23%;"
		title="{{=value.check_res_des}}">
		{{=value.check_res_des}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:10%;"
		title="{{?value.is_abnormal===1}}是{{??}}否{{?}}">{{?value.is_abnormal===1}}是{{??}}否{{?}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h text-c" style="width:14%;"
		title="{{=value.unit}}">{{=value.unit}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h text-c" style="width:14%;"
		title="{{=value.spec_ceil}}">{{=value.spec_ceil}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h text-c" style="width:14%;"
		title="{{=value.spec_ceil}}">{{=value.spec_ceil}}
	</span>
</div>
{{~}}`;
