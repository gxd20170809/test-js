import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
// tslint:disable-next-line:ordered-imports
import { productid, systemid } from '../../atom/config';
interface Message {
	no: string;
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	// data: unknown;
	// cookie?: {
	// 	[name: string]: string;
	// } | null;
	// content_type?: string;
	// headers?: {
	// 	[key: string]: string[];
	// };
	// attachment?: string;
	// redirect?: string;
	// status_code?: number;
}
// 表名	basic_data
// 标题	基础数据
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// productid	产品ID	string	50	√	ⅹ
// value_no	值编号	string	32	√	ⅹ
// value_title	值标题	string	32	√	ⅹ
// value_name	值名称	string	32	√	ⅹ

// 表名	property_optional_values
// 标题	属性可选值
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// productid	产品ID	string	50	√	ⅹ
// value_no	值编号	string	32	√	ⅹ
// optional_value	可选值	string	32	√	ⅹ
// priority	排序码	int	16	√	ⅹ
// value_description	值描述	string	128	√	ⅹ
// optional_value_no	可选值编号	string	32	√	ⅹ
// 表名	plan_classify
// 标题	计划分类
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// plan_cla_attr_no	计划分类属性编号	string	50	√	ⅹ
// plan_cla_attr_name	计划分类属性名称	string	50	√	ⅹ

// 表名	plan_type
// 标题	计划类型
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// plan_cla_attr_no	计划分类属性编号	string	50	√	ⅹ
// plan_type_no	计划类型编号	string	50	√	ⅹ
// plan_type_name	计划类型名称	string	50	√	ⅹ

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:F:\test\test-js\src\huangqianqian\zj-000004\s001,action_id:' + action_id);

	const pp = query(action_id, session_id, systemid);
	pp.prepare('plan_classify', ['plan_cla_attr_no', 'plan_cla_attr_name'],
		{ productid, systemid }, 200, 1, [], []);
	const [p] = await pp.exec();
	const dd = complex_query(action_id, session_id, systemid);
	const d = await dd
		.add_field('basic_data', 'value_no', 'value_no')
		.add_field('property_optional_values', 'optional_value', 'optional_value')
		.where_eq('basic_data', 'value_name', 'area_name')
		.inner_join('basic_data', 'property_optional_values', ['value_no', 'value_no'])
		.exec();



	log('Service end path:F:\test\test-js\src\huangqianqian\zj-000004\s001,action_id:' + action_id);
	return { d, p };
}
