import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
import p002 from './p002';
export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// todo
	const res = await nodejs<{ d: Array<{}>; p: Array<{}> }>('huangqianqian/zj-000004/s001', {});

	console.log(res);
	render(fd, res.d, p001, 'p001', 'after');
	render(fd, res.p, p002, 'p002', 'after');
}
