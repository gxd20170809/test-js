import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import a012 from './a012';
import p004 from './p004';

export default async function a013(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 删除，先判断是否选中节点，没有提示，在复选框添加一个属性(data-no)获取数据
	const list_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');
	const list = Array.from(list_chk) as HTMLInputElement[];
	if (list.length === 0) {
		alert('请选择至少一行数据');

	} else {
		const nos = list.map((n) => {
			return n.getAttribute('data-no');
		});
		//
		const d = get(fd, 'data') as Array<{ check_res_no: string; }>;
		const arr: Array<{ check_res_no: string; }> = [];
		// 循环
		d.forEach((v) => {
			const no = v.check_res_no;
			const s = nos.includes(no);
			if (!s) {
				arr.push(v);
			}
		});
		render(fd, d, p004, 'p004', 'inner');
		set(fd, 'data', d);
		await a012(fd);
	}
}
