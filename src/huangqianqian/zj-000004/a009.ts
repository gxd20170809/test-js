import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a009(fd: IFeidaoAiBrowserComponent, data: { hidden_source_feature_code: string, hidden_source_feature_name: string, companyname: string }) {
	// todo
	const hidden_source_feature_code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source_feature_code"]');
	hidden_source_feature_code.innerHTML = data.hidden_source_feature_code;
	const hidden_source_feature_name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	hidden_source_feature_name.value = data.hidden_source_feature_name;
	const companyname = fd.data.node.querySelector<HTMLInputElement>('[data-id="companyname"]');
	companyname.value = data.companyname;

	set(fd, 'feature_info', data);
}
