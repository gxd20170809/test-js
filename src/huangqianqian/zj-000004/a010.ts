import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import p004 from './p004';

export default async function a010(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string; }>) {
	const d = get(fd, 'data') as Array<{ check_res_no: string; flag?: boolean; }>;
	if (!d) {
		render(fd, data, p004, 'p004', 'inner');
		set(fd, 'data', data);

	} else {
		d.forEach(async (v) => {
			const no = v.check_res_no;
			// 重复
			const res = d.filter((dd) => {
				return dd.check_res_no === no;
			});
			if (res.length === 0) {
				d.push(v);
			} else {
				for (let i = 0; i < d.length; i++) {
					const v1 = d[i];
					if (v1.check_res_no === no) {
						v1.flag = true;
					}
				}
			}
			render(fd, d, p004, 'p004', 'inner');
			set(fd, 'data', d);


		});





	}
}
