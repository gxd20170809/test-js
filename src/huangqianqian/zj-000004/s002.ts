import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	filter: {
		plan_cla_attr_no: string;
	};
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	// data: unknown;
	// cookie?: {
	// 	[name: string]: string;
	// } | null;
	// content_type?: string;
	// headers?: {
	// 	[key: string]: string[];
	// };
	// attachment?: string;
	// redirect?: string;
	// status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:F:\test\test-js\src\huangqianqian\zj-000004\s002,action_id:' + action_id);

	const data = query(action_id, session_id, systemid);
	data.prepare('plan_type', ['plan_type_no', 'plan_type_name', 'plan_cla_attr_no'],
		{ productid, plan_cla_attr_no: message.filter.plan_cla_attr_no }, 200, 1, [], []);
	const [data1] = await data.exec();

	log('Service end path:F:\test\test-js\src\huangqianqian\zj-000004\s002,action_id:' + action_id);
	return { data1 };
}
