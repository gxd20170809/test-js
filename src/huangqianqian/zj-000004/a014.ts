import upload from '@dfeidao/atom-web/file/upload';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import { productid } from '../../atom/config';

export default async function a014(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 保存
	const info = get(fd, 'hidden_info');
	if (info) {
		alert('请先选择隐患源');
		return;
	}
	const data = get(fd, 'feature_info') as { hidden_source_feature_no: string };
	if (data) {
		alert('请先选择隐患源特性');
	}
	const area_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="area"]');
	const area = area_node.value;
	if (area === '请选择') {
		alert('请选择区域');
		return;
	}
	// 获取到了p002
	const plan_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const plan = plan_node.value;
	if (plan === '请选择') {
		alert('请选择计划分类属性');
		return;
	} else {
		// 获取下拉框的值（看p002）用option节点获取
		const plan_name = plan_node.selectedOptions[0].text;
	}

	const type_node = fd.data.node.querySelector<HTMLSelectElement>('[data - id="type"]');
	const type = type_node.value;
	if (type === '请选择') {
		alert('请选择计划分类属性');
		return;
	} else {
		// 获取下拉框的值（看p002）用option节点获取
		const type_name = type_node.selectedOptions[0].text;
	}
	const target = fd.data.node.querySelector<HTMLSelectElement>('[data - id="target"]').value;
	if (!target) {
		alert('请输入目标值');
		return;
	}
	const unit = fd.data.node.querySelector<HTMLSelectElement>('[data - id="unit"]').value;
	if (!unit) {
		alert('请输入单位');
		return;
	}
	const res = await upload(productid, fd.data.node.querySelector<HTMLInputElement>('[data - id="unit"]'));
	const unit_v = res.filename;
	// console.log('--------', res);

	const spec_ceil = fd.data.node.querySelector<HTMLSelectElement>('[data - id="spec_ceil"]').value;
	if (!spec_ceil) {
		alert('请输入范围上限');
		return;
	}
	const spec_floor = fd.data.node.querySelector<HTMLSelectElement>('[data - id="spec_floor"]').value;
	if (!spec_floor) {
		alert('请输入范围下限');
		return;
	}

	const upper_limit = fd.data.node.querySelector<HTMLSelectElement>('[data - id="upper_limit"]').value;
	if (!upper_limit) {
		alert('请输入上限');
		return;
	}
	const down_value = fd.data.node.querySelector<HTMLSelectElement>('[data - id="down_value"]').value;
	if (!down_value) {
		alert('请输入下限');
		return;
	}
	const d = get(fd, 'data');
	const r = await nodejs('huangqianqian/zj-000004/s003', {
		data: {
			plan_type_no: type,
			plan_type_name: type_name,
			plan_cla_attr_no: string,
			plan_cla_attr_name: string,
			area_name: string,
			upper_limit: string,
			down_value: string,
			target: string,
			spec_ceil: string,
			spec_floor: string,
			unit: string,
		}
	});

}
