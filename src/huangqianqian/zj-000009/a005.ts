import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from './a002';

export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	console.log(fd);
	const data = get(fd, 'data1');

	if (data) {
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000004', 'a009', data);
		}
		// else {

		// }

		await a002(fd);
	} else {
		alert('请选择一列数据');
	}
}
