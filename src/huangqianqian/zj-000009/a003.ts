import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';

export default async function a003(fd: IFeidaoAiBrowserComponent) {
	// todo

	const info = get(fd, 'info');
	console.log(info);
	const res = await nodejs('huangqianqian/zj-000009/s001', {
		hidden_source: info
	});

	render(fd, res, p001, 'p001', 'inner');
}
