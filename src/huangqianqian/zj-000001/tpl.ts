export default `<div class="header">
	<a href="" class="logo">智慧安全云平台</a>
	<div style="display:flex;align-items:center;margin-right:50px;">
		<!-- 消息 start -->
		<div class="pos-r bignews" style="margin-right:20px;">
			<i class="iconfont icon-xiaoxi color-f cursor-p" style="font-size:32px;"></i>
			<div class="news pos-a bg-f b-box"
				style="width:340px;top:53px;right:-80px;z-index: 10;border-radius:5px;box-shadow: 0px 5px 20px #ccc;">
				<div class="b-box" style="padding:20px;">
					<div class="b-box font14 color-3" style="height:30px;border-bottom: 3px solid #5575d7;width: 58px;">
						消息通知</div>
					<div>
						<!-- 有消息 start -->
						<div style="margin-top:10px;border-bottom: 1px solid #EAEDF2;">
							<div class="font14">
								<span class="overflow-h2 color-3" style="line-height:26px;">你好呀是是是的好地方和if好</span>
								<span class="color-9 l-ht40">2019-03-01 11:49</span>
							</div>
						</div>
						<a href="#" class="more fr font14 color-6" style="margin:20px 20px 20px 0;">查看更多</a>
						<!-- 有消息 end -->
						<!-- 无消息 start -->
						<div class="ht40 l-ht40 text-c font14 color-3">
							暂无消息
						</div>
						<!-- 无消息 end -->
					</div>
				</div>
			</div>
		</div>
		<!-- 消息 end -->
		<!-- 个人资料 start -->
		<div class="pos-r biginfo">
			<i class="iconfont icon-touxiang color-f cursor-p" style="font-size:40px;"></i>
			<div class="info pos-a bg-f b-box"
				style="width:340px;height:172px;top:53px;right:-20px;z-index: 10;border-radius:5px;box-shadow: 0px 5px 20px #ccc;">
				<div class="b-box" style="height:100px;padding:20px 0 20px 20px;border-bottom: 1px solid #EAEDF2;">
					<img src="../images/avtar.png" alt="" class="radius-circle" style="width:60px;height:60px;">
					<div class="display-ib vertical-a-t" style="margin-left:30px;">
						<div class="font14 color-3">周大发</div>
						<input type="button" value="我的个人信息" class="btn" style="width:110px;margin-top:10px;">
					</div>
				</div>
				<input type="button" value="退出" class="cbtn fr" style="width:50px;margin: 20px 20px 0 0;">
			</div>
		</div>
		<!-- 个人资料 end -->
	</div>
</div>`;
