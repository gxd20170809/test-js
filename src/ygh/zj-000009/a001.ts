import get_files_url from '@dfeidao/atom-web/file/get-file-url';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import fire from '@dfeidao/atom-web/msg/fire';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import { productid } from '../../atom/config';
import p001 from './p001';
import p002 from './p002';
// import set from '@dfeidao/atom-web/local/set';

export default async function a001(fd: IFeidaoAiBrowserComponent, nos: string) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="sty_up"]');
	// 将style的class删除
	n.removeAttribute('class');
	const data = await nodejs<{ res: Array<{ plan_cla_attr_no: string }>; res2: Array<{}> }>('ygh/zj-000009/s001', { std_no: nos });
	// console.log(data.res[0]);
	// console.log(data.res2);
	const arr = data.res[0];
	const u = get_files_url(productid, 'unit', arr);
	if (data.res.length > 0) {
		const ar = await fire('zj-000008', 'a018');
		const pl_no = data.res[0].plan_cla_attr_no;
		// let plan_no = [] as Array<{}>;
		let con = [] as Array<{}>;
		if (pl_no) {
			con = await nodejs('ygh/zj-000009/s003', { plan_cla_attr_no: pl_no });
		}
		const conn = Object.assign({ con }, u, ar);
		// const conn = Object.assign({ plan_type }, u, ar);
		console.log(conn);
		// console.log('000');
		render(fd, [conn], p001, 'p001', 'inner');
		// console.log(conn);
		render(fd, data.res2, p002, 'p002', 'inner');
		set(fd, 'info', data.res[0]);
		// console.log(data.res[0]);
		set(fd, 'data', data.res2);
		set(fd, 'no', nos);
		// set(fd, 'res', data);
	}

}
