// import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import quary from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;
	// plan_cla_attr_no: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000009\s002,action_id:' + action_id);

	const q = quary(action_id, session_id, systemid);
	q.prepare('plan_classify', ['plan_cla_attr_no', 'plan_cla_attr_name'], { productid }, 200, 1, [], []);
	const res = await q.exec();

	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000009\s002,action_id:' + action_id);
	return {
		data: { res }
	};
}
