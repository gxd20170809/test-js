import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import a009 from './a009';
import p002 from './p002';
export default async function a008(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string; flag?: boolean }>) {
	// todo

	const n = get(fd, 'data') as Array<{ check_res_no: string; flag?: boolean }>;
	if (!n) {
		//  undefined之前没有数据时，为undefined
		render(fd, data, p002, 'p002', 'inner');
		set(fd, 'data', data);
		await a009(fd);
	} else {
		data.forEach((v) => {
			const no = v.check_res_no;
			const res = n.filter((d) => {
				// 不存在一样的res的值为[]
				return d.check_res_no === no;
			});
			if (res.length === 0) {
				n.push(v);
			} else {
				for (let i = 0; i < n.length; i++) {
					const e = n[i];
					if (e.check_res_no === no) {
						e.flag = true;
					}
				}
			}
			// await a014(fd);

			// set(fd, 'arr', n);
			// render(fd, res, p004, 'p004', 'after');
		});
		// await a015(fd);
	}
	render(fd, n, p002, 'p002', 'inner');
	// console.log('122221111' + n);
	set(fd, 'data', n);
	await a009(fd);
	// render(fd, data, p004, 'p004', 'inner');
	// set(fd, 'data', data);
	/// await a014(fd);
	// F10逐行执行
	//  F8执行下一个断点
	// 鼠标放在代码上 代码执行结果
}
