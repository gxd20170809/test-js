// import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import operate from '@dfeidao/atom-nodejs/db/operate';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;
	//
	standard_no: string;
	mm: {
		plan_type_no: string;
		plan_type_name: string;
		plan_cla_attr_no: string;
		plan_cla_attr_name: string;
		area_name: string;
		upper_limit: string;
		down_value: string;
		target: string;
		spec_ceil: string;
		spec_floor: string;
		unit: string;
	};
	// 弹窗回填值
	fn_info: {
		companyid: string;
		companyname: string;
		hidden_source_feature_no: string;
		hidden_source_feature_name: string;
		hidden_source_feature_code: string;
	};
	hn_info: {
		hidden_code: string;
		hidden_source: string;
		hidden_name: string;
	};
	re_info: Array<{ check_res_no: string }>;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000009\s004,action_id:' + action_id);

	const q = query(action_id, session_id, systemid);
	q.prepare('qu_check_result', ['_id'], { productid, standard_no: message.standard_no }, 200, 1, [], []);
	const [data] = await q.exec<{ _id: string }>();

	const { exec, update, del, insert } = operate(action_id, session_id, systemid);
	update('qu_hidden_feat_st', { productid, standard_no: message.standard_no }, {
		// standard_no: res.result,
		companyid: message.fn_info.companyid,
		companyname: message.fn_info.companyname,
		area_name: message.mm.area_name,
		hidden_source: message.hn_info.hidden_source,
		hidden_code: message.hn_info.hidden_code,
		hidden_name: message.hn_info.hidden_name,
		hidden_source_feature_no: message.fn_info.hidden_source_feature_no,
		hidden_source_feature_name: message.fn_info.hidden_source_feature_name,
		hidden_source_feature_code: message.fn_info.hidden_source_feature_code,
		upper_limit: message.mm.upper_limit,
		down_value: message.mm.down_value,
		target: message.mm.target,
		spec_ceil: message.mm.spec_ceil,
		spec_floor: message.mm.spec_floor,
		plan_type_no: message.mm.plan_cla_attr_no,
		plan_type_name: message.mm.plan_type_name,
		plan_cla_attr_no: message.mm.plan_cla_attr_no,
		plan_cla_attr_name: message.mm.plan_cla_attr_name,
		unit: message.mm.unit,
		productid
	});

	data.forEach((v) => {
		del('qu_check_result', { _id: v._id });
	});
	if (message.re_info && message.re_info.length > 0) {
		message.re_info.forEach((v) => {
			insert('qu_check_result', {
				standard_no: message.standard_no,
				check_res_no: v.check_res_no,
				productid
			});
		});
	}

	const res = await exec();
	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000009\s004,action_id:' + action_id);
	return {
		data: res
	};
}
