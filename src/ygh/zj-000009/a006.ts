import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a006(fd: IFeidaoAiBrowserComponent, data: { hidden_source_feature_no: string, hidden_source_feature_name: string, companyname: string }) {
	// todo
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="feature_code1"]');
	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name1"]');
	const company = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_company1"]');
	// console.log(data.hidden_source_feature_no);
	// console.log(data.hidden_source_feature_name);
	// console.log(data.companyname);
	code.innerHTML = data.hidden_source_feature_no;// span设置值用innerHTML
	name.value = data.hidden_source_feature_name;
	company.value = data.companyname;
	set(fd, 'hidden_source_info', data);
}
