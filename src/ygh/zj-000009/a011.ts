import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import p002 from './p002';

export default async function a011(fd: IFeidaoAiBrowserComponent) {
	// todo
	const node_chk = fd.data.node.querySelectorAll('[data-id="ch_list1"]:checked');
	const list = Array.from(node_chk) as HTMLInputElement[];
	if (list.length === 0) {
		alert("请选择一条数据");
	} else {
		const l = list.map((v) => {
			return v.getAttribute('data-no');
		});
		console.log(l);
		const n = get(fd, 'data') as Array<{ check_res_no: string }>;
		const arr: Array<{ check_res_no: string }> = [];
		n.forEach((e) => {
			const no = e.check_res_no;
			// 确定数组是否包含特定元素,并根据需要返回 true 或 false。
			const s = l.includes(no);
			if (!s) {
				arr.push(e);
			}
		});
		render(fd, arr, p002, 'p002', 'inner');
		set(fd, 'data', arr);
	}
}
