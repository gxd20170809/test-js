import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;

	// 标准编号
	std_no: string;
}
interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000009\s001,action_id:' + action_id);

	const q = query(action_id, session_id, systemid);


	q.prepare('qu_hidden_feat_st', ['hidden_source', 'hidden_name', 'hidden_code', 'hidden_source_feature_no', 'hidden_source_feature_code', 'hidden_source_feature_name', 'companyid', 'companyname', 'area_name', 'unit', 'target', 'upper_limit', 'down_value', 'spec_ceil', 'spec_floor', 'standard_no', 'plan_type_no', 'plan_type_name', 'plan_cla_attr_no', 'plan_cla_attr_name'], { productid, standard_no: message.std_no }, 1, 1, [], []);

	const [res] = await q.exec();

	const c_q = complex_query(action_id, session_id, systemid);
	// 多表查询使用连接（内链接）将两张表连接在一起
	const res2 = await c_q.add_field('qu_check_result', 'standard_no', 'standard_no')
		.add_field('qu_check_result', 'check_res_no', 'check_res_no')
		.add_field('hidden_ck_res_set', 'check_res_des', 'check_res_des')
		.add_field('hidden_ck_res_set', 'is_abnormal', 'is_abnormal')
		.add_field('hidden_ck_res_set', 'spec_ceil', 'spec_ceil')
		.add_field('hidden_ck_res_set', 'spec_floor', 'spec_floor')
		.add_field('hidden_ck_res_set', 'unit', 'unit')
		.inner_join('qu_check_result', 'hidden_ck_res_set', ['check_res_no', 'check_res_no'])
		// 单表操作,第二个参数与相应条件的别名一致
		.where_eq('qu_check_result', 'standard_no', message.std_no)
		.where_eq('qu_check_result', 'productid', productid)
		.exec();
	// spec_ceil
	// spec_floor
	// unit

	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000009\s001,action_id:' + action_id);
	return {
		data: { res, res2 }
	};
}
// mm: {
// 	plan_type_no: string;
// 	plan_type_name: string;
// 	plan_cla_attr_no: string;
// 	plan_cla_attr_name: string;
// 	area_name: string;
// 	upper_limit: string;
// 	down_value: string;
// 	target: string;
// 	spec_ceil: string;
// 	spec_floor: string;
// 	unit: string;
// };
// // 弹窗回填值
// fn_info: {
// 	companyid: string;
// 	companyname: string;
// 	hidden_source_feature_no: string;
// 	hidden_source_feature_name: string;
// 	hidden_source_feature_code: string;
// };
// hn_info: {
// 	hidden_code: string;
// 	hidden_source: string;
// 	hidden_name: string;
// };
// re_info: Array<{ check_res_no: string }>;
