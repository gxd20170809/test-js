import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo
	const r_n = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_code1"]');
	// console.log(r_n.innerText);
	const data = get(fd, 'hidden_source_info') as { hidden_source_feature_no: string };
	// fire('zj-000004', '');
	if (r_n) {
		// fire('zj-000004', 'a001', data.hidden_source_feature_no, 'edit');
		fire('zj-000004', 'a001', r_n.innerText, 'edit');
	} else if (data) {
		fire('zj-000004', 'a001', data.hidden_source_feature_no, 'edit');
		// alert("请选择特性代码");
	} else {
		alert("请选择特性代码");
	}
}
