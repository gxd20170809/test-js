{{~it:value:index}}
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
			隐患源编号：
		</span>
		<i class="iconfont icon-icon02 color-theme font20 vertical-a-m" data-feidao-actions='click:a003'></i>
		<span style="margin-left:5px;" class="font12 color-6 text-r b-box display-ib vertical-a-m"
			data-id="hidden_source1">{{=value.hidden_source}}</span>
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			隐患源名称：
		</span>
		<input type="text" style="width: 140px" value="{{=value.hidden_name}}" data-id="hidden_name1">
	</div>
</div>
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>
			特性代码：
		</span>
		<i class="iconfont icon-icon02 color-theme font20 vertical-a-m" data-feidao-actions='click:a005'></i>
		<span style="margin-left:5px;" class="font12 color-6 text-r b-box display-ib vertical-a-m"
			data-id="feature_code1">{{=value.hidden_source_feature_no}}</span>
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			特性名称：
		</span>
		<input type="text" style="width: 140px" value="{{=value.hidden_source_feature_name}}" data-id="feature_name1">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r  display-ib" style="width:100px;">
			公司：
		</span>
		<input type="text" style="width: 140px" value="{{=value.companyname}}" data-id="feature_company1">
	</div>
</div>
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			区域：
		</span>
		<select class="select" style="width:140px;" data-id="area_name">
			<option>请选择</option>
			{{~value.res:v:i}}
			<option {{? value.area_name === v.optional_value}} selected {{?}} value="{{=v.optional_value}}">
				{{=v.optional_value}}</option>
			{{~}}
		</select>
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>计划分类属性：</span>
		<select class="select" style="width:140px;" data-id="plan_cla_attr_no" data-feidao-actions='change:a013'>
			<option>请选择</option>
			{{~value.res2[0]:v1:i1}}
			<option {{? value.plan_cla_attr_no === v1.plan_cla_attr_no}} selected {{?}}
				value="{{=v1.plan_cla_attr_no}}">
				{{=v1.plan_cla_attr_name}}</option>
			{{~}}
		</select>
	</div>
	<div style="width:30%" data-feidao-presentation='p003'>
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
			计划类型：
		</span>
		<select class="select" style="width:140px;" data-id="plan_typename">
			<option>请选择</option>
			{{~value.con.res[0]:v2:i2}}
			<option {{? value.plan_type_no === v2.plan_type_no}} selected {{?}} value="{{=v2.plan_type_no}}">
				{{=v2.plan_type_name}}</option>
			{{~}}
		</select>
	</div>

</div>
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>目标值：
		</span>
		<input type="text" style="width: 140px" value="{{=value.target}}" data-id="target">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>
			单位：
		</span>
		<input type="file" data-id="unit">
		<img style="width:50px;height:20px" src="{{=value.unit}}">
	</div>
</div>
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%" style="margin-right: 50px;">
		<span class="font12 color-6 text-r b-box display-ib " style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>规范上限：
		</span>
		<input type="text" style="width: 140px" data-id="spec_ceil" value="{{=value.spec_ceil}}">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m  display-ib" style="margin-right:3px;">*</span>
			规范下限：
		</span>
		<input type="text" style="width: 140px" data-id="spec_floor" value="{{=value.spec_floor}}">
	</div>
</div>
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>上限：
		</span>
		<input type="text" style="width: 140px" data-id="upper_limit" value="{{=value.upper_limit}}">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m " style="margin-right:3px;">*</span>下限：
		</span>
		<input type="text" style="width: 140px" data-id="down_value" value="{{=value.down_value}}">
	</div>
</div>
{{~}}