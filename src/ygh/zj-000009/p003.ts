export default `<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
	<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
	计划类型：
</span>
<select class="select" style="width:140px;" data-id="plan_typename">
	<option>请选择</option>
	{{~ it:value:index}}
	<option value="{{=value.plan_type_no}}">{{=value.plan_type_name}}</option>
	{{~}}
</select>`;
