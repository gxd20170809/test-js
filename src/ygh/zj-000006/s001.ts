// import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;
	page: {
		hidden_source: string;
		hidden_source_feature_name: string;
		pn: number;
	};
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000006\s001,action_id:' + action_id);

	const q = query(action_id, session_id, systemid);

	q.prepare('hazard_source_feature', ['hidden_source_feature_code', 'hidden_source_feature_name', 'companyname', 'companyid', 'hidden_source_feature_no', 'hidden_source'], { productid, hidden_source: message.page.hidden_source, hidden_source_feature_name: { $like: '%' + message.page.hidden_source_feature_name + '%' } }, 1, message.page.pn, [], []);

	const [res] = await q.exec();
	const d = res.map((value) => {
		// .{..value}是分别把value的值放到js对象里面 === {a:'a',b:'b'}
		// v是一个string v的值是的每一条表中的字段名及值
		return { ...value, v: JSON.stringify(value) };
	});
	const c = count(action_id, session_id, systemid);

	c.prepare('hazard_source_feature', { productid, hidden_source: message.page.hidden_source, hidden_source_feature_name: { $like: '%' + message.page.hidden_source_feature_name + '%' } });
	// c.prepare('hazard_source_feature', { productid });

	const [n] = await c.exec();

	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000006\s001,action_id:' + action_id);
	return {
		data: { res: d, n }
	};
}
