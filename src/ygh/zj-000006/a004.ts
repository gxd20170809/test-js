import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import set_nodes_cls from '@dfeidao/atom-web/ui/set-nodes-cls';

export default async function a004(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	const d = e.currentTarget as HTMLDivElement;
	// console.log(d);

	// d.setAttribute('style', 'red');
	// set_node_cls(d, 'bg-select', true);
	const n = fd.data.node.querySelectorAll('[data-name="hidden_source_name"]');
	const ns = Array.from(n) as HTMLDivElement[];
	set_nodes_cls(ns, 'bg-select', false);
	set_node_cls(d, 'bg-select', true);
	const v = d.getAttribute('data-value');
	const data = (JSON.parse(v));// 转化为JS对象｛id：'',name: ''｝
	// console.log(data);
	set(fd, 'data', data);
	// console.log("22222");
	// console.log(data);
}
