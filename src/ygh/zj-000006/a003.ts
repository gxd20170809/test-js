import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import pn_sty from '@dfeidao/fd-w000010';
import p001 from './p001';

export default async function a003(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {
	// todo
	const hidden_source_1 = get(fd, 'hidden_source');
	const pageno = a.page_no || 1;
	// console.log(a.page_no);
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v = n.value;
	const res = await nodejs('ygh/zj-000006/s001', {
		page: {
			hidden_source: hidden_source_1,
			hidden_source_feature_name: v,
			pn: Number(pageno) || 1
		}
	});
	render(fd, res, p001, 'p001', 'inner');
	const p = fd.data.node.querySelector<pn_sty>('[data-id="sty_3"]');
	p.setAttribute('page-no', pageno + '');
}
