import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';// local同意组建 global 页签
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
// import pn_sty from '@dfeidao/fd-w000010';
import p001 from './p001';
// import render from '@dfeidao/atom-nodejs/render/render';
// import p001 from './p001';

export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string, info: { hidden_source: string }) {
	// todo

	// const n1 = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	// const v = n1.value;
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="sty_2"]');
	n.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12');
	// const hidde = info.hidden_source;
	// console.log(hidde);
	// console.log(v);
	const res = await nodejs('ygh/zj-000006/s001', {
		page: {
			// 三个参数
			hidden_source: info.hidden_source,
			hidden_source_feature_name: '',
			pn: 1
		}
	}
	);
	// console.log(res);
	render(fd, res, p001, 'p001', 'inner');
	set(fd, 'flag', flag);
	set(fd, 'hidden_source', info.hidden_source);
}
