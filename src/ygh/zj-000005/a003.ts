import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import pn_sty from '@dfeidao/fd-w000010';
import p001 from './p001';

export default async function a003(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {
	// todo

	const pageno = a.page_no || 1;
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name"]');
	const v = n.value;
	const res = await nodejs('ygh/zj-000005/s001', {
		page: {
			pn: Number(pageno) || 1,// 定义页面参数
			feature_name: v// 输入框的值
		}
	});
	render(fd, res, p001, 'p001', 'inner');
	const p = fd.data.node.querySelector<pn_sty>('[data-id="sty_1"]');
	p.setAttribute('page-no', pageno + '');
}
