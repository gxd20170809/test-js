import { IFeidaoAiNodejsComponent } from '@dfeidao/atom-nodejs/interfaces';
import nodejs from '@dfeidao/atom-nodejs/msg/nodejs';
import render from '@dfeidao/atom-nodejs/render/render';
import p001 from './p001';

export default async function na001(fd: IFeidaoAiNodejsComponent) {
	// todo

	// const page_no = fd.data.params['page-no'] || '1';// 如果没有的话 则传给服务文件默认值1
	// const pn = page_no;
	const res = await nodejs(fd.data.actionid, fd.data.sessionid, 'ygh/zj-000005/s001', {
		page: {
			pn: 1,
			// pn: a.page_no,// 定义页面参数
			feature_name: ''// 输入框的值
		}
	});
	render(fd.data.node, res, p001, 'p001');
	// console.log(a);// 数据 总条数
}
