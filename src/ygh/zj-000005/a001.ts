import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="sty"]');
	// 将style的class删除
	// fd.data.node.style.display = '';
	// n.removeAttribute('class');
	n.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12');

	// const res = await nodejs('ygh/zj-000005/s001', {});
	// render(fd, res, p001, 'p001', 'inner');
	// console.log(res);
	set(fd, 'flag', flag);
}
