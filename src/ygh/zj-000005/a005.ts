import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a009 from '../zj-000008/a009';

export default async function a005(fd: IFeidaoAiBrowserComponent, flag: string) {
	// todo
	// global当前页签使用
	// local一个组件内使用，不同的相应文件之间传递参数
	// storage 缓存到浏览器 浏览器手动清除才会消失
	// console.log(fd);
	const data = get(fd, 'data');
	if (data) {
		const f = get(fd, 'flag') as string;
		if (f === 'add') {
			fire('zj-000008', 'a008', data);// data剩余参数
			// await a009(fd);
		} else {
			fire('zj-000009', 'a004', data);
		}
		// fire('zj-000008', 'a008', data);// data剩余参数
		await a009(fd);
	} else {
		alert("请选择一条数据");
	}
}
