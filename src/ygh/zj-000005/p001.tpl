{{~ it.res:value:index}}
<div class="t_body display-" style="width:571px;" data-feidao-presentation='p001'>
	<div class="tableRow b-box" data-feidao-actions='click:a004' data-name="hiddden" data-value='{{=value.v}}'>
		<span class="display-ib p5 border-r-1 text-c b-box" style="width:25%;">{{=value.hidden_name}}
		</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:25%;"
			title="">{{=value.control_grade}}
		</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:25%;"
			title="">{{=value.control_level}}
		</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:25%;"
			title="">{{=value.assess_level}}
		</span>
	</div>
</div>
{{~}}
{{? it.res.length === 0}}
<!--筛选结果为空的蒙版 start-->
<div class="none display-">
	<i class="icon iconfont icon-wushuju display-ib text-c vertical-a-t color-9" style="font-size:100px;"></i>
	<div class="font22 color-9" style="height:50px;line-height:50px;">当前筛选条件下，没有匹配的数据</div>
</div>
{{??}}
<!--分页 start-->
<div class="page">
	<!-- <fd-w000009 key="page-no" size=10 total={{=it.n}} max-btn-num=4 lang="zh_CN" show-first show-last show-goto
		show-total></fd-w000009> -->
	<fd-w000010 page-no="1" data-id="sty_1" data-feidao-actions="fdwe-change:a003" size=2 total={{=it.n}} max-btn-num=4
		show-first show-last show-goto show-total lang="zh_CN">
	</fd-w000010>
</div>
<!--分页 end-->
{{?}}
<!--筛选结果为空的蒙版 end-->