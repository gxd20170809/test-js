// import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	page: {
		pn: number;// 定义页面参数
		feature_name: string;// 输入框的值
		// pn?: number;// 定义页面参数
	};
	// pn: number;
	query: {};
	params: {};
	headers: {};
	captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};

	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000005\s001,action_id:' + action_id);
	const q = query(action_id, session_id, systemid);
	q.prepare('hazard_source', ['hidden_code', 'hidden_name', 'control_grade', 'control_level', 'assess_level', 'hidden_source'], { productid, hidden_name: { $like: '%' + message.page.feature_name + '%' } }, 2, message.page.pn, [], []);

	const [res] = await q.exec();
	// map是数组的方法 有返回值 返回值是括号里面的值
	const d = res.map((value) => {
		// .{..value}是分别把value的值放到js对象里面 === {a:'a',b:'b'}
		// v是一个string v的值是的每一条表中的字段名及值
		return { ...value, v: JSON.stringify(value) };
	});
	const c = count(action_id, session_id, systemid);

	c.prepare('hazard_source', { productid, hidden_name: { $like: '%' + message.page.feature_name + '%' } });

	const [n] = await c.exec();
	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000005\s001,action_id:' + action_id);
	return {
		data: { res: d, n }
	};
}
