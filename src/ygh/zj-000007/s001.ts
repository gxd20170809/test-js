import operate from '@dfeidao/atom-nodejs/db/operate';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;
	nos: string[];
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000007\s001,action_id:' + action_id);

	const { del, exec } = operate(action_id, session_id, systemid);
	//
	del('qu_hidden_feat_st', { standard_no: { $in: message.nos } });
	del('qu_check_result', { standard_no: { $in: message.nos } });

	const res = await exec();

	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000007\s001,action_id:' + action_id);
	return {
		data: res
	};
}
