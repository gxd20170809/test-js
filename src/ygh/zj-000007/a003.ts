import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import refresh from '@dfeidao/atom-web/url/refresh';

export default async function a003(fd: IFeidaoAiBrowserComponent) {
	// todo
	const std_no = get(fd, 'nos') as string[];
	// console.log(std_no);
	const res = await nodejs<{ code: 0 | 1, result: string }>('ygh/zj-000007/s001', { nos: std_no });
	if (res.code === 1) {
		alert('删除成功');
		refresh();
	} else {
		alert('删除失败');
	}
}
