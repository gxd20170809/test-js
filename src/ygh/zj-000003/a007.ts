import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo
	const node_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');
	const arr = Array.from(node_chk);
	if (arr.length === 0) {
		alert('请选择一条数据');
	} else {
		const nos = arr.map((v) => {
			return v.getAttribute("data-no");
		});
		fire('zj-000007', 'a001', nos);
	}
	// fire('zj-000007', 'a001', 'ch');
}
