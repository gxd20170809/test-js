import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a009(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	const n = e.currentTarget as HTMLDListElement;
	const no = n.getAttribute('data-no');
	fd.data.node.style.display = 'none';
	fire('zj-000009', 'a001', no);
}
