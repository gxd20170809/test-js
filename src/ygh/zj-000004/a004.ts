import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// todo
	const node = fd.data.node.querySelector<HTMLInputElement>('[data-id="ch_all"]');

	// 对比全选的与选中的长度（当前页面）是否一致，不一致取消全选按钮
	const node_all = fd.data.node.querySelectorAll('[data-id="ch_list"]');
	// console.log(node_all);
	// 已经选中的复选框
	const node_chk = fd.data.node.querySelectorAll('[data-id="ch_list"]:checked');
	if (Array.from(node_all).length === Array.from(node_chk).length) {
		node.checked = true;
		// const v = fd.data.node.querySelector('[data-v]');
		// console.log(v);
	} else {
		node.checked = false;
	}
	// const v = fd.data.node.querySelector('[data-v]');
	// console.log(v);
}
