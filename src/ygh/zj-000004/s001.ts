import query from "@dfeidao/atom-nodejs/db/query";
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	query: {};
	params: {};
	headers: {};
	captcha: string;
	page: {
		// check_res_no: string;
		hidden_source_feature_no: string;
	};
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000004\s001,action_id:' + action_id);

	const q = query(action_id, session_id, systemid);
	q.prepare('hidden_ck_res_set', ['check_res_no', 'check_res_des', 'is_abnormal', 'unit', 'spec_ceil', 'spec_floor', 'hidden_source_feature_no'], { productid, hidden_source_feature_no: message.page.hidden_source_feature_no }, 200, 1, [], []);
	const [res] = await q.exec();
	const d = res.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	});
	// check_res_no: message.page.check_res_no
	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000004\s001,action_id:' + action_id);
	return {
		data: d
	};
}
