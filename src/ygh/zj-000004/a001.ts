import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';

export default async function a001(fd: IFeidaoAiBrowserComponent, hidden_no: string, flag: string) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="sty_4"]');
	n.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12');

	console.log(hidden_no);
	console.log(flag);
	const res = await nodejs<Array<{}>>('ygh/zj-000004/s001', {
		page: {
			hidden_source_feature_no: hidden_no
		}
	});
	render(fd, res, p001, 'p001', 'inner');
	console.log(res);
	set(fd, 'flag', flag);
}
