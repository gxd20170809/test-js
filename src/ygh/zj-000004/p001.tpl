{{~ it:value:index}}
<div class="tableRow b-box">
	<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
		<input type="checkbox" data-id="ch_list" data-feidao-actions='click:a004' data-v='{{=value.v}}'>
	</span>
	<span class="display-ib p5 border-r-1 text-c b-box" style="width:150px;">{{=value.check_res_no}}
	</span>
	<span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:175px;">{{=value.check_res_des}}
	</span>
	<span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
		style="width:80px;">{{? value.is_abnormal === 1}}是{{??}}否{{?}}
	</span>
	<span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:120px;">{{=value.unit}}
	</span>
	<span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
		style="width:120px;">{{=value.spec_ceil}}
	</span>
	<span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
		style="width:100px;">{{=value.spec_floor}}
	</span>
</div>
{{~}}