import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	// const node = fd.data.node.querySelectorAll<HTMLInputElement>('[data-id="ch_list"]');
	// Array.from(node).forEach((n: HTMLInputElement) => {
	// 	if (n.checked === true) {
	// 		// console.log(n);
	// 		const v = n.getAttribute('data-v');
	// 		console.log(v);
	// 		// const id[] = v;
	// 	}
	// });
	// const flag = get(fd, 'flag') as string;
	const node_chk = fd.data.node.querySelectorAll('[data-id="ch_list"]:checked');
	const node = Array.from(node_chk) as HTMLInputElement[];
	if (node.length === 0) {
		alert("请选择");
	} else {
		const d = node.map((n) => {
			const v = JSON.parse(n.getAttribute('data-v'));
			return v;
		});
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000008', 'a013', d);
		} else {
			fire('zj-000009', 'a008', d);
		}
		// fire('zj-00008', 'a013', d);
	}
	const n1 = fd.data.node.querySelector<HTMLDivElement>('[data-id="sty_4"]');
	n1.setAttribute('class', 'bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12 display-n');
}
