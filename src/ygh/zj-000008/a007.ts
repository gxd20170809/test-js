import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p003 from './p003';

export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo

	const n = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan_cla_attr_no"]');
	const v = n.value;
	// console.log(v);
	const res = await nodejs<{ res: Array<{}> }>('ygh/zj-000008/s002', {
		plan_cla_attr_no: v
	});
	// console.log(v);
	// console.log(res.res);
	// const no = fd.data.node.querySelector('[data-id="dis"]');
	// no.setAttribute('class', 'display-n');
	// fd.data.node.style.display = 'none';
	render(fd, res.res[0], p003, 'p003', 'inner');

}
