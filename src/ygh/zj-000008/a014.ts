import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a014(fd: IFeidaoAiBrowserComponent) {
	// todo

	const node = fd.data.node.querySelector<HTMLInputElement>('[data-id="ch_all"]');
	// console.log(node);
	// 获取单选选中的输入框 querySelectorAll:NodeListOf<Element>，为数组，转化为数组Array.from
	const nodes = fd.data.node.querySelectorAll('[data-id="ch_list"]');
	// 循环执行，全选，使用.checked与全选的保持一致n.checked = node.checked;
	Array.from(nodes).forEach((n: HTMLInputElement) => {
		n.checked = node.checked;
	});
}
