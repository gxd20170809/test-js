import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';

export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// todo
	const data = get(fd, 'hidden_source_info') as { hidden_source_feature_no: string };
	// fire('zj-000004', '');
	if (data) {
		fire('zj-000004', 'a001', data.hidden_source_feature_no, 'add');
	} else {
		alert("请选择特性代码");
	}
}
