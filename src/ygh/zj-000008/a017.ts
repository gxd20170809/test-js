import upload from '@dfeidao/atom-web/file/upload';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import refresh from '@dfeidao/atom-web/url/refresh';
import { productid } from '../../atom/config';

export default async function a017(fd: IFeidaoAiBrowserComponent) {
	// todo

	// console.log('aaaaa' + data_t);
	// console.log(data_v);
	const va = get(fd, 'data');
	console.log(va);
	const info = get(fd, 'hidden_info') as { hidden_code: string, hidden_source: string, hidden_name: string };
	console.log(info.hidden_source);
	console.log(info.hidden_name);
	if (!info) {
		alert('请选择');
		return;
	}
	const data = get(fd, 'hidden_source_info') as { companyid: string, companyname: string, hidden_source_feature_no: string, hidden_source_feature_name: string, hidden_source_feature_code: string };
	// console.log(data.hidden_source_feature_no);
	// console.log(data.hidden_source_feature_name);
	// console.log(data.companyname);
	if (!data) {
		alert('请选择');
		return;
	}
	console.log(data);
	const a = fd.data.node.querySelector<HTMLSelectElement>('[data-id="area_name"]');
	if (a.value === '请选择') {
		alert('请选择区域');
		return;
	}
	console.log(a.value);
	const h = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan_cla_attr_no"]');
	let plan_name = '';
	if (h.value === '请选择计划分类属性') {
		alert('请选择');
		return;
	} else {
		plan_name = h.selectedOptions[0].text;
		console.log(h.value);
		console.log(plan_name);
	}
	const z = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan_typename"]');
	let type_name = '';
	if (z.value === '请选择') {
		alert('请选择');
		return;
	} else {
		type_name = z.selectedOptions[0].text;
		console.log(z.value);
		console.log(type_name);
	}
	const c = fd.data.node.querySelector<HTMLInputElement>('[data-id="target"]').value;
	if (!c) {
		alert('请选择目标值');
		return;
	}
	console.log(c);
	const p = fd.data.node.querySelector<HTMLInputElement>('[ data-id="unit"]').value;
	if (!p) {
		alert('请选择单位');
		return;
	}
	console.log(p);
	const d = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_ceil"]').value;
	if (!d) {
		alert('请选择目标值');
		return;
	}
	console.log(d);
	const e = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_floor"]').value;
	if (!e) {
		alert('请选择目标值');
		return;
	}
	console.log(d);
	const f = fd.data.node.querySelector<HTMLInputElement>('[data-id="upper_limit"]').value;
	if (!f) {
		alert('请选择目标值');
		return;
	}
	console.log(f);
	const g = fd.data.node.querySelector<HTMLInputElement>('[data-id="down_value"]').value;
	if (!g) {
		alert('请选择目标值');
		return;
	}
	// console.log(g);
	const fl = await upload(productid, fd.data.node.querySelector<HTMLInputElement>('[ data-id="unit"]'));
	// console.log(fl);
	// console.log(fl.filename);
	// filename: "85893232-1116-4a76-b775-40c1a6e8a3a2.jpeg"
	// 单位在数据库中保存的值是filename
	// const va = get(fd, 'data');
	// console.log('a017' + va);
	const aa = await nodejs<{ code: 0 | 1, mes: string }>('ygh/zj-000008/s003', {
		mm: {
			plan_type_no: z.value,
			plan_type_name: type_name,
			plan_cla_attr_no: h.value,
			plan_cla_attr_name: plan_name,
			area_name: a.value,
			upper_limit: f,
			down_value: g,
			target: c,
			spec_ceil: d,
			spec_floor: e,
			unit: fl.filename
		},
		fn_info: data,
		hn_info: info,
		re_info: va
	});
	// console.log(aa);
	if (aa.code === 1) {
		alert('保存成功');
		refresh();
	} else {
		alert('保存失败');
	}

}
