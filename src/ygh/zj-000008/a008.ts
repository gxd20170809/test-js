import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
// 新建形参来获取别的组件传递来的值 data:{}
export default async function a008(fd: IFeidaoAiBrowserComponent, data: { hidden_source: string, hidden_name: string }) {
	// todo
	// console.log("bbbbb");
	// console.log(data);
	// render(fd, res, p001, 'p001', 'inner');
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source"]');
	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	code.innerHTML = data.hidden_source;// span设置值用innerHTML
	name.value = data.hidden_name;
	set(fd, 'hidden_info', data);
	// console.log('12345' + data.hidden_name);
	// data-id="hidden_source"
}
