import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a016(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	console.log("aaaaa");
	const n = e.target as HTMLDivElement;// 使用DIV替换
	const v = n.getAttribute('data-id');
	const u = n.getAttribute('data-flag');

	// 排序 默认为1 点击一次变为0 降序
	// 找到所有对应标签名ALL 返回类型伪数组 转化为数组
	const ns = fd.data.node.querySelectorAll('[data-asc="desc"]');
	Array.from(ns).forEach((value: HTMLDivElement) => {
		value.setAttribute('class', 'iconfont cursor-p icon-shangxia color-3 font14 pos-a');
	});
	const asc: string[] = [];
	const desc: string[] = [];
	if (u === '1') {
		// 升
		n.setAttribute('data-flag', '0');
		n.setAttribute('class', 'iconfont cursor-p icon-shang color-3 font14 pos-a');
		asc.push(v);
	} else {
		// 降
		n.setAttribute('data-flag', '1');
		n.setAttribute('class', 'iconfont cursor-p icon-xia color-3 font14 pos-a');
		desc.push(v);
	}
}
