import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import a015 from './a015';
import p004 from './p004';
export default async function a013(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string; flag?: boolean }>) {
	// todo
	console.log(data);
	const n = get(fd, 'data') as Array<{ check_res_no: string; flag?: boolean }>;
	if (!n) {
		//  undefined之前没有数据时，为undefined
		render(fd, data, p004, 'p004', 'inner');
		set(fd, 'data', data);
		await a015(fd);
	} else {
		data.forEach((v) => {
			const no = v.check_res_no;
			const res = n.filter((d) => {
				// 不存在一样的res的值为[]
				return d.check_res_no === no;
			});
			if (res.length === 0) {
				n.push(v);
			} else {
				for (let i = 0; i < n.length; i++) {
					const e = n[i];
					if (e.check_res_no === no) {
						n[i].flag = true;
					}
				}
			}
			// await a014(fd);
			render(fd, n, p004, 'p004', 'inner');
			set(fd, 'data', data);
			// render(fd, res, p004, 'p004', 'after');
		});
		// await a015(fd);
	}
	await a015(fd);
	// render(fd, data, p004, 'p004', 'inner');
	// set(fd, 'data', data);
	/// await a014(fd);
	// F10逐行执行
	//  F8执行下一个断点
	// 鼠标放在代码上 代码执行结果
}
