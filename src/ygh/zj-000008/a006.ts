import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
import p002 from './p002';

export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// todo
	// const n = fd.data.node.querySelector<HTMLSelectElement>('[dta-id="plan_cla_attr_no"]');
	// const v = n.value;
	const res = await nodejs<{ res: Array<{}>, res2: Array<{}> }>('ygh/zj-000008/s001', {
	});
	// console.log(res.res);
	// console.log(res.res2);

	render(fd, res.res, p001, 'p001', 'after');
	render(fd, res.res2[0], p002, 'p002', 'after');
	return res;
}
