import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import quary from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	urls: {
		base: string;
		origin: string;
		url: string;
	};
	// plan: {
	// 	plan_cla_attr_no?: string;
	// };
	// plan_cla_attr_no?: string;
	query: {};
	params: {};
	headers: {};
	captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}
// interface PlanNo {
// 	plan_cla_attr_no?: string;
// }
export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\test-feidao\test-js\src\ygh\zj-000008\s001,action_id:' + action_id);
	// const plan_no = (message.plan_cla_attr_no);
	const quary_1 = complex_query(action_id, session_id, systemid);
	const res = await quary_1.add_field('basic_data', 'value_no', 'value_no')
		.add_field('property_optional_values', 'optional_value', 'optional_value')
		.where_eq('basic_data', 'value_name', 'area_name')
		.inner_join('basic_data', 'property_optional_values', ['value_no', 'value_no'])
		.exec();

	const quary_2 = quary(action_id, session_id, systemid);
	quary_2.prepare('plan_classify', ['plan_cla_attr_no', 'plan_cla_attr_name'], { productid }, 200, 1, [], []);
	const res2 = await quary_2.exec();

	// const res3 = quary_2.exec();

	// const quary_3 = quary(action_id, session_id, systemid);
	// quary_3.prepare('plan_type', ['plan_cla_attr_no', 'plan_type_no', 'plan_type_name'], { productid }, 200, 1, [], []);
	// const res3 = await quary_3.exec();

	log('Service end path:e:\test-feidao\test-js\src\ygh\zj-000008\s001,action_id:' + action_id);
	return {
		data: { res, res2 }
	};
	// console.log(res);
}
