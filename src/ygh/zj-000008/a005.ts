import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from '@dfeidao/atom-web/render/render';
import p004 from './p004';

export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	console.log("11111");
	// const n = get(fd, 'data');
	const node_chk = fd.data.node.querySelectorAll('[data-id="ch_list"]:checked');
	const list = Array.from(node_chk) as HTMLInputElement[];
	if (list.length === 0) {
		alert("请选择一条数据");
	} else {
		const l = list.map((v) => {
			return v.getAttribute('data-no');
		});
		console.log(l);
		const n = get(fd, 'data') as Array<{ check_res_no: string }>;
		const arr: Array<{ check_res_no: string }> = [];
		n.forEach((e) => {
			const no = e.check_res_no;
			const s = l.includes(no);
			if (!s) {
				arr.push(e);
			}
		});
		render(fd, arr, p004, 'p004', 'inner');
		set(fd, 'data', arr);
	}
}
