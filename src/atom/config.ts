export const spaceid = 'smart_security'; // 一个项目只有一个spaceid，且是唯一的
export const systemid = '7051'; // systemid，refsystemid 数据库编号，指向一个库；项目创建的时候已存在
export const refsystemid = ''; // 命名导入
export const productid = 'smart_security';

export const server = `http://127.0.0.1:8889`;
export const login = `${server}/custom-login`;

