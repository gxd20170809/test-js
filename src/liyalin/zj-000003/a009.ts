import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';
export default async function a009(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	// 进入修改页面不是弹窗 需要把原有界面隐藏掉  再添加新页面
	fd.data.node.style.display = 'none';
	// 已关闭原页面
	const n = e.currentTarget as HTMLDivElement;
	const no = n.getAttribute('data-no');
	// console.log('3 009');
	fire('zj-000005', 'a001', no);

}
