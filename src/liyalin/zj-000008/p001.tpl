{{~ it.data : value : index}}
<div class="t_body display-" style="width:571px;" data-feidao-presentation='p001'>
	<div class="tableRow b-box" data-feidao-actions="click:a004" data-name="hidden" data-v='{{=value.v}}'>
		<span class="display-ib p5 border-r-1 text-c b-box" style="width:25%;">{{=value.hidden_name}}
		</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:25%;"
			title="一层">{{=value.control_grade}}
		</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:25%;"
			title="一般">{{=value.control_level}}
		</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:25%;"
			title="一级">{{=value.assess_level}}
		</span>
	</div>
</div>
{{~}}
{{? it.data.length === 0}}
<div class="none display-">
	<i class="icon iconfont icon-wushuju display-ib text-c vertical-a-t color-9" style="font-size:100px;"></i>
	<div class="font22 color-9" style="height:50px;line-height:50px;">当前筛选条件下，没有匹配的数据</div>
</div>
{{??}}
<div class="page">
	   

	    <fd-w000010 data-feidao-actions="fdwe-change:a003" page-no="1" data-id="paging" size=2 total={{=it.n}}
		show-first show-last show-goto show-total lang="zh_CN" max-btn-num=4>
		    </fd-w000010>
</div>
{{?}}