import { IFeidaoAiNodejsComponent } from '@dfeidao/atom-nodejs/interfaces';
import nodejs from '@dfeidao/atom-nodejs/msg/nodejs';
import render from '@dfeidao/atom-nodejs/render/render';
import p001 from './p001';

export default async function na001(fd: IFeidaoAiNodejsComponent) {
	// todo
	// const page_no = fd.data.params['page-no'] as string || '1';
	// 调用服务端  只需要默认参数 用于初始化显示
	// 服务端与客户端服务文件的区别 就是 服务端需要的只是默认的参数 固定的值去调用服务
	// 而客户端需要动态的参数 或者说是url上的信息
	const res = await nodejs(fd.data.actionid, fd.data.sessionid, 'liyalin/zj-000008/s001', {
		page: {
			pn: 1,
			hidden_name: ''
		}
	});
	console.log(res);
	// console.log(page_no);
	// data-  开头的是自定义属性
	// 数据渲染
	render(fd.data.node, res, p001, 'p001');
}
