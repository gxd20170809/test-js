import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import Paging from "@dfeidao/fd-w000010";
import p001 from './p001';
export default async function a003(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {
	// todo
	// 初始化需要参数 不能是na001 在这里渲染
	// const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="main"]');
	// n.setAttribute('class', "bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12");
	const p = a.page_no;
	const p2 = Number(p) || 1;
	const hs = get(fd, 'info_hidden_source') as string;
	console.log(hs);
	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v = name.value;
	console.log(v);
	const res = await nodejs('liyalin/zj-000009/s001', {
		page: {
			pn: p2,
			hidden_source: hs,
			hidden_source_feature_name: v
		}
	});
	console.log(res);
	render(fd, res, p001, 'p001', 'inner');
	// 引入控件包 转换为控件类型
	const paging = fd.data.node.querySelector<Paging>('[data-id="paging"]');
	// setAttribute属性第二个参数为string型 需将数字转化为字符串
	paging.setAttribute('page-no', p2 + '');
}
