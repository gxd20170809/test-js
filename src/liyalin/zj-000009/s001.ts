import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	page: {
		pn: number;
		hidden_source: string;
		hidden_source_feature_name: string
	};
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}
export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000009\s001,action_id:' + action_id);
	const q = query(action_id, session_id, systemid);
	q.prepare('hazard_source_feature',
		['hidden_source_feature_code', 'hidden_source_feature_name', 'companyname', 'companyid', 'hidden_source_feature_no', 'hidden_source'],
		{ productid, hidden_source: message.page.hidden_source, hidden_source_feature_name: { $like: '%' + message.page.hidden_source_feature_name + '%' } }, 1, message.page.pn, [], []);
	const [data] = await q.exec();
	// map : 对数组的每个值 执行箭头函数 返回的值放入心的数组
	// value = {test:'1',a:'b'}
	// {...value} = {test:'1',a:'b'}
	const d = data.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	});


	const c = count(action_id, session_id, systemid);
	c.prepare('hazard_source_feature', { productid, hidden_source: message.page.hidden_source, hidden_source_feature_name: { $like: '%' + message.page.hidden_source_feature_name + '%' } });
	const n = await c.exec();
	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000009\s001,action_id:' + action_id);
	return {
		data: { data: d, n }
	};
}
