import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import set_node_cls from "@dfeidao/atom-web/ui/set-node-cls";
import set_nodes_cls from "@dfeidao/atom-web/ui/set-nodes-cls";
export default async function a004(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	console.log("组件8 a004  读取一行数据", e.currentTarget, e.target);
	const d = e.currentTarget as HTMLDivElement;

	const nodes = fd.data.node.querySelectorAll('[data-name="hidden"]');
	const ns = Array.from(nodes) as HTMLDivElement[];
	set_nodes_cls(ns, 'bg-select', false);
	set_node_cls(d, 'bg-select', true);

	// 获取data-v里面存的属性
	const v = d.getAttribute('data-v');
	// v里面存的是个字符串 解析它 变为JS对象
	const data = JSON.parse(v);

	set(fd, 'data009', data);
}
