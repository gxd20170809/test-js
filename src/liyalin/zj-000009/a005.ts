import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from '../zj-000009/a002';
export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	const data = get(fd, 'data009');
	// data如果在没点击一行的时候 会是 未定义
	if (data) {
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			// fire的剩余参数，就是要传过去的参数
			fire('zj-000004', 'a008', data);
		} else {
			// 修改组件
			fire('zj-000005', 'a007', data);
		}

		await a002(fd);
	} else {
		alert('请选择一条数据');
	}
}
