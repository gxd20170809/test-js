import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	filter: {
		standard_no: string;
	};
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000005\s001,action_id:' + action_id);

	const { prepare, exec } = query(action_id, session_id, systemid);

	prepare('qu_hidden_feat_st', ['hidden_source', 'hidden_name', 'hidden_code', 'hidden_source_feature_no', 'hidden_source_feature_code', 'hidden_source_feature_name', 'companyid', 'companyname', 'area_name', 'unit', 'target', 'upper_limit', 'down_value', 'spec_ceil', 'spec_floor', 'standard_no', 'plan_type_no', 'plan_type_name', 'plan_cla_attr_no', 'plan_cla_attr_name'], { productid, standard_no: message.filter.standard_no }, 1, 1, [], []);

	const [res1] = await exec();

	const query2 = complex_query(action_id, session_id, systemid);

	// 由于查询的是两张表 必须有一种连接方式
	// where_eq('qu_check_result', 'standard_no', message.filter.standard_no) 第二个参数和字段里起的别名保持一致

	const res2 = await query2.add_field('qu_check_result', 'standard_no', 'standard_no')
		.add_field('qu_check_result', 'check_res_no', 'check_res_no')
		.add_field('hidden_ck_res_set', 'check_res_des', 'check_res_des')
		.add_field('hidden_ck_res_set', 'is_abnormal', 'is_abnormal')
		.add_field('hidden_ck_res_set', 'spec_ceil', 'spec_ceil')
		.add_field('hidden_ck_res_set', 'spec_floor', 'spec_floor')
		.add_field('hidden_ck_res_set', 'unit', 'unit')
		.inner_join('qu_check_result', 'hidden_ck_res_set', ['check_res_no', 'check_res_no'])
		.where_eq('qu_check_result', 'standard_no', message.filter.standard_no)
		.where_eq('qu_check_result', 'productid', productid)
		.exec();


	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000005\s001,action_id:' + action_id);
	return {
		data: { res1, res2 }
	};
}
