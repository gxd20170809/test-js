import upload from '@dfeidao/atom-web/file/upload';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import { productid } from '../../atom/config';
export default async function a013(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 获取唯一标识 标准编号
	const no = get(fd, 'sno');
	const res1 = get(fd, 'render_data') as {
		hidden_code: string;
		hidden_name: string;
		hidden_source: string;
	};

	const res2 = get(fd, 'render_data') as {
		companyid: string;
		companyname: string;
		hidden_source_feature_no: string;
		hidden_source_feature_name: string;
		hidden_source_feature_code: string;
	};

	let info1 = get(fd, 'hidden_info');
	if (!info1) {
		info1 = res1;
	}

	let info2 = get(fd, 'hazard_source_feature');
	if (!info2) {
		info2 = res2;
	}


	const area_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="area"]');
	const area = area_node.value;
	if (area === '请选择') {
		alert('请选择区域');
		return;
	}
	const plan_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const plan = plan_node.value;
	let plan_name = '';
	if (plan === '请选择') {
		alert('请选择计划分类属性');
		return;
	} else {
		plan_name = plan_node.selectedOptions[0].text;
	}
	const type_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="type"]');
	const type = type_node.value;
	let type_name = '';
	if (type === '请选择') {
		alert('请选择计划类型');
		return;
	} else {
		type_name = type_node.selectedOptions[0].text;
	}

	const target = fd.data.node.querySelector<HTMLInputElement>('[data-id="target"]').value;
	if (!target) {
		alert('请输入目标值');
		return;
	}

	const unit = fd.data.node.querySelector<HTMLInputElement>('[data-id="unit"]').value;
	if (!unit) {
		alert('请选择图片');
		return;
	}
	// 对于图片 有对应的原子操作
	const res = await upload(productid, fd.data.node.querySelector<HTMLInputElement>('[data-id="unit"]'));
	const unit_v = res.filename;

	const spec_ceil = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_ceil"]').value;
	if (!spec_ceil) {
		alert('请输入规范上限');
		return;
	}

	const spec_floor = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_floor"]').value;
	if (!spec_floor) {
		alert('请输入规范下限');
		return;
	}
	const upper_limit = fd.data.node.querySelector<HTMLInputElement>('[data-id="upper_limit"]').value;
	if (!upper_limit) {
		alert('请输入上限');
		return;
	}
	const down_value = fd.data.node.querySelector<HTMLInputElement>('[data-id="down_value"]').value;
	if (!down_value) {
		alert('请输入下限');
		return;
	}

	const d = get(fd, 'data');

	const r = await nodejs<{ code: 0 | 1; result: string }>('liyalin/zj-000005/s002', {
		standard_no: no,
		data: {
			plan_type_no: type,
			plan_type_name: type_name,
			plan_cla_attr_no: plan,
			plan_cla_attr_name: plan_name,
			area_name: area,
			upper_limit,
			down_value,
			target,
			spec_ceil,
			spec_floor,
			unit: unit_v
		},
		feature_info: res2,
		hidden_info: res1,
		result_info: d
	});
	// console.log(r);
	if (r.code === 1) {
		alert('修改成功');
		// refresh();
	} else {
		alert('修改失败');
	}
}
