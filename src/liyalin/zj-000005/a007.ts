import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
export default async function a007(fd: IFeidaoAiBrowserComponent, data: { hidden_source_feature_code: string, hidden_source_feature_name: string, companyname: string }) {
	// todo
	// 接收第二个弹窗的回填值 将其回填在页面上
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source_feature_code"]');
	// span用innerHTML放入数据 input框用.value赋值就可以
	code.innerHTML = data.hidden_source_feature_code;
	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name2"]');
	name.value = data.hidden_source_feature_name;
	const cname = fd.data.node.querySelector<HTMLInputElement>('[data-id="companyname"]');
	cname.value = data.companyname;



	set(fd, 'hazard_source_feature', data);
}
