import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from "@dfeidao/atom-web/render/render";
import a011 from './a011';
import p002 from './p002';
export default async function a012(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 删除最下方表格数据的响应
	// 获取到打对勾的行数据
	const ns_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');
	const list = Array.from(ns_chk) as HTMLInputElement[];
	// console.log(list);
	if (list.length === 0) {
		alert('请选择至少一列数据');
	} else {
		//
		const nos = list.map((n) => {
			return n.getAttribute('data-no');
		});

		const d = get(fd, 'data') as Array<{ check_res_no: string }>;

		const arr: Array<{ check_res_no: string }> = [];

		d.forEach((v) => {
			const no = v.check_res_no;
			// nos 是个字符串数组 用includes方法
			const s = nos.includes(no);
			if (!s) {
				arr.push(v);
			}
		});

		render(fd, arr, p002, 'p002', 'inner');
		set(fd, 'data', arr);
		await a011(fd);
	}
}
