import operate from '@dfeidao/atom-nodejs/db/operate';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	standard_no: string;
	data: {
		plan_type_no: string;
		plan_type_name: string;
		plan_cla_attr_no: string;
		plan_cla_attr_name: string;
		area_name: string;
		upper_limit: string;
		down_value: string;
		target: string;
		spec_ceil: string;
		spec_floor: string;
		unit: string;
	};
	feature_info: {
		companyid: string;
		companyname: string;
		hidden_source_feature_no: string;
		hidden_source_feature_name: string;
		hidden_source_feature_code: string;
	};
	hidden_info: {
		hidden_code: string;
		hidden_name: string;
		hidden_source: string;
	};
	result_info: Array<{ check_res_no: string }>;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000005\s002,action_id:' + action_id);

	const { prepare, exec: e } = query(action_id, session_id, systemid);

	prepare('qu_check_result', ['_id'], { productid, standard_no: message.standard_no }, 200, 1, [], []);

	const [data] = await e<{ _id: string }>();

	const { exec, update, del, insert } = operate(action_id, session_id, systemid);

	update('qu_hidden_feat_st', { productid, standard_no: message.standard_no }, {
		// standard_no: res.result,
		companyid: message.feature_info.companyid,
		companyname: message.feature_info.companyname,
		area_name: message.data.area_name,
		hidden_source: message.hidden_info.hidden_source,
		hidden_code: message.hidden_info.hidden_code,
		hidden_name: message.hidden_info.hidden_name,
		hidden_source_feature_no: message.feature_info.hidden_source_feature_no,
		hidden_source_feature_name: message.feature_info.hidden_source_feature_name,
		hidden_source_feature_code: message.feature_info.hidden_source_feature_code,
		upper_limit: message.data.upper_limit,
		down_value: message.data.down_value,
		target: message.data.target,
		spec_ceil: message.data.spec_ceil,
		spec_floor: message.data.spec_floor,
		plan_type_no: message.data.plan_type_no,
		plan_type_name: message.data.plan_type_name,
		plan_cla_attr_no: message.data.plan_cla_attr_no,
		plan_cla_attr_name: message.data.plan_cla_attr_name,
		unit: message.data.unit,
		productid
	});

	// 循环删除 清空此id下的所有表格数据 先清空 再填入新的数据
	data.forEach((d) => {
		del('qu_check_result', { _id: d._id });
	});

	if (message.result_info && message.result_info.length > 0) {
		message.result_info.forEach((v) => {
			insert('qu_check_result', {
				standard_no: message.standard_no,
				check_res_no: v.check_res_no,
				productid
			});
		});
	}
	const r = await exec();

	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000005\s002,action_id:' + action_id);
	return {
		data: r
	};
}
