import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';
export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 第二个弹窗按钮的点击响应
	// const info = get(fd, 'hidden_info') as { hidden_source: string };
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source"]');
	// span用innerHTML放入数据 input框用.value赋值就可以
	const info = { hidden_source: "" };
	info.hidden_source = code.innerText;

	if (info) {
		fire('zj-000009', 'a001', 'edit', info);
	} else {
		alert('请先选择隐患源');
	}
}
