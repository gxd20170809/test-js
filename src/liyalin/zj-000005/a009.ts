import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from "@dfeidao/atom-web/render/render";
import a011 from './a011';
import p002 from './p002';
export default async function a009(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string, flag?: boolean }>) {
	// todo
	const d = get(fd, 'data') as Array<{ check_res_no: string, flag?: boolean }>;
	if (!d) {
		// 如果是第一次进行表格填充 d为unknown !d为真  直接渲染即可  无需与原有表格进行数据对比
		render(fd, data, p002, 'p002', 'inner');
		set(fd, 'data', data);
		// a012响应文件主要是用来控制表头上的全选按钮的显示  如果表体数据全部选中 就显示对号 否则不显示
		await a011(fd);
	} else {
		// d 里面有数据
		// 做数据对比 把新的数据放入data变量里
		// 对接受来的 较大的数组作为第一层循环
		data.forEach(async (v) => {
			// 获取到循环项的编号值
			const no = v.check_res_no;
			// 利用过滤函数（相当于二重循环 判断筛选）
			const res = d.filter((dd) => {
				// 获取到一组数据   得知新数组有哪些数据和旧数组相等
				return dd.check_res_no === no;
			});
			if (res.length === 0) {
				// 判断 如果新数组不包含旧数组的数据
				// 得知当前循环的这条数据是一条新数据 放入d d为要渲染的数据
				d.push(v);
			} else {
				// 否则 当前循环的这条数据是一条原有数据 需要在老数组里找到这条原有数据 将他的flag值设为true
				for (let i = 0; i < d.length; i++) {
					const t = d[i];
					if (t.check_res_no === no) {
						// 找到了这个原有的“重复数据” 将他的flag值设为true 渲染时会根据flag值判断是否显示选中
						d[i].flag = true;
					}
				}
			}
		});
		render(fd, d, p002, 'p002', 'inner');
		set(fd, 'data', d);
		await a011(fd);
	}
}
