import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
export default async function a004(fd: IFeidaoAiBrowserComponent, data: { hidden_source: string, hidden_name: string }) {
	// todo
	// 接收组件8的返回值 将返回值回填到页面上
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source"]');
	// span用innerHTML放入数据 input框用.value赋值就可以
	code.innerHTML = data.hidden_source;
	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	name.value = data.hidden_name;

	set(fd, 'hidden_info', data);
}
