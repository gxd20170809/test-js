import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
export default async function a008(fd: IFeidaoAiBrowserComponent) {
	// todo
	const data = get(fd, 'hidden_source_feature_no');
	console.log(data);

	if (data) {
		// 显示弹出框
		fire('zj-000007', 'a001', data, 'edit');
	} else {
		// 提示
		alert('请选择隐患源特性');
	}
}
