import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a011(fd: IFeidaoAiBrowserComponent) {
	// todo
	const ns = fd.data.node.querySelectorAll('[data-id="list"]');
	const all = fd.data.node.querySelector<HTMLInputElement>('[data-id="all"]');
	// 找到所有被选中的节点，如果和ns的数量不一样 就说明all的全选对号就不该显示了
	const ns_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');

	if (Array.from(ns).length === Array.from(ns_chk).length) {
		all.checked = true;
	} else {
		all.checked = false;
	}
}
