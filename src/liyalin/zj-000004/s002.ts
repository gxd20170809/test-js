import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	plan_cla_attr_no: string;
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000004\s002,action_id:' + action_id);
	console.log(message.plan_cla_attr_no);
	// 查询计划类型表
	const q = query(action_id, session_id, systemid);
	q.prepare('plan_type',
		['plan_type_no', 'plan_type_name'],
		{ productid, plan_cla_attr_no: message.plan_cla_attr_no }, 200, 1, [], []);

	const res = await q.exec();
	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000004\s002,action_id:' + action_id);
	return {
		data: { res }
	};
}
