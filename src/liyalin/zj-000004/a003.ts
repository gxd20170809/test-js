import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from "@dfeidao/atom-web/render/render";
import p001 from './p001';
import p002 from './p002';

export default async function a003(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 获取区域下拉框和计划分类属性的值
	const res = await nodejs<{ res: Array<{}>; res2: Array<{}> }>('liyalin/zj-000004/s001', {});
	console.log(res);
	// 渲染区域下拉框的值
	render(fd, res.res, p001, 'p001', 'after');
	// 渲染区域下拉框的值
	render(fd, res.res2[0], p002, 'p002', 'after');

	return res;

}
