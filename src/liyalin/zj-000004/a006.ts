import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
export default async function a006(fd: IFeidaoAiBrowserComponent, data: { hidden_source: string, hidden_name: string }) {
	// todo
	// b.ts丢失 从其他地方粘过来一个 把里面的相关参数改了
	// 回填值：“隐患源编号”、“隐患源代码”、“隐患源名称”
	// console.log(data);
	// console.log('回填值');
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source"]');
	// span用innerHTML放入数据 input框用.value赋值就可以
	code.innerHTML = data.hidden_source;
	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	name.value = data.hidden_name;

	set(fd, 'hidden_info', data);
}
