import upload from '@dfeidao/atom-web/file/upload';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
// import refresh from '@dfeidao/atom-web/url/refresh';
import { productid } from '../../atom/config';
export default async function a014(fd: IFeidaoAiBrowserComponent) {
	// todo
	const info = get(fd, 'hidden_info');
	if (!info) {
		alert('请先选择隐患源');
		// 加return阻止之后的额外代码运行
		return;
	}
	const data = get(fd, 'hazard_source_feature') as { hidden_source_feature_no: string };
	if (!data) {
		alert('请先选择隐患源');
		return;
	}
	const area_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="area"]');
	const area = area_node.value;
	if (area === '请选择') {
		alert('请选择区域');
		return;
	}
	const plan_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const plan = plan_node.value;
	let plan_name = '';
	if (plan === '请选择') {
		alert('请选择计划分类属性');
		return;
	} else {
		plan_name = plan_node.selectedOptions[0].text;
	}
	const type_node = fd.data.node.querySelector<HTMLSelectElement>('[data-id="type"]');
	const type = type_node.value;
	let type_name = '';
	if (type === '请选择') {
		alert('请选择计划类型');
		return;
	} else {
		type_name = type_node.selectedOptions[0].text;
	}

	const target = fd.data.node.querySelector<HTMLInputElement>('[data-id="target"]').value;
	if (!target) {
		alert('请输入目标值');
		return;
	}

	const unit = fd.data.node.querySelector<HTMLInputElement>('[data-id="unit"]').value;
	if (!unit) {
		alert('请选择图片');
		return;
	}
	// 对于图片 有对应的原子操作
	const res = await upload(productid, fd.data.node.querySelector<HTMLInputElement>('[data-id="unit"]'));
	const unit_v = res.filename;

	const spec_ceil = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_ceil"]').value;
	if (!spec_ceil) {
		alert('请输入规范上限');
		return;
	}

	const spec_floor = fd.data.node.querySelector<HTMLInputElement>('[data-id="spec_floor"]').value;
	if (!spec_floor) {
		alert('请输入规范下限');
		return;
	}
	const upper_limit = fd.data.node.querySelector<HTMLInputElement>('[data-id="upper_limit"]').value;
	if (!upper_limit) {
		alert('请输入上限');
		return;
	}
	const down_value = fd.data.node.querySelector<HTMLInputElement>('[data-id="down_value"]').value;
	if (!down_value) {
		alert('请输入下限');
		return;
	}

	const d = get(fd, 'data');

	const r = await nodejs<{ code: 0 | 1; result: string }>('liyalin/zj-000004/s003', {
		data: {
			plan_type_no: type,
			plan_type_name: type_name,
			plan_cla_attr_no: plan,
			plan_cla_attr_name: plan_name,
			area_name: area,
			upper_limit,
			down_value,
			target,
			spec_ceil,
			spec_floor,
			unit: unit_v
		},
		feature_info: data,
		hidden_info: info,
		result_info: d
	});
	// console.log(r);
	if (r.code === 1) {
		alert('保存成功');
		// refresh();
	} else {
		alert('保存失败');
	}
}
