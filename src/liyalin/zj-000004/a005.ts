import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from "@dfeidao/atom-web/render/render";
import p003 from './p003';
export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	// console.log("调用成功");
	const n = fd.data.node.querySelector<HTMLOptionElement>('[data-id="plan"]');
	const v = n.value;
	console.log(v);
	const res = await nodejs<{ res: Array<{}> }>('liyalin/zj-000004/s002', {
		plan_cla_attr_no: v
	});
	console.log(res);
	// 渲染区域下拉框的值
	render(fd, res.res[0], p003, 'p003', 'inner');
}
