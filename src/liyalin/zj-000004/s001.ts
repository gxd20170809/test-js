import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';


interface Message {
	cookie: {
		uk: string;
		[key: string]: string
	};
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000004\s001,action_id:' + action_id);

	// 获取区域下拉框的值
	const q1 = complex_query(action_id, session_id, systemid);
	const res = await q1.add_field('basic_data', 'value_no', 'value_no')
		.add_field('property_optional_values', 'optional_value', 'option_value')
		.where_eq('basic_data', 'value_name', 'area_name')
		.inner_join('basic_data', 'property_optional_values', ['value_no', 'value_no'])
		.exec();
	// 获取 计划分类属性 下拉框的值
	const q2 = query(action_id, session_id, systemid);
	q2.prepare('plan_classify', ['plan_cla_attr_no', 'plan_cla_attr_name'], { productid }, 200, 1, [], []);

	const res2 = await q2.exec();
	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000004\s001,action_id:' + action_id);
	return {
		data: { res, res2 }
	};
}
