import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
interface Message {
	no: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000007\s001,action_id:' + action_id);

	const q = query(action_id, session_id, systemid);
	q.prepare('hidden_ck_res_set',
		['check_res_no', 'check_res_des', 'unit', 'is_abnormal', 'spec_ceil', 'spec_floor'],
		{ productid, hidden_source_feature_no: message.no }, 200, 1, [], []);
	const [data] = await q.exec();
	// map : 对数组的每个值 执行箭头函数 返回的值放入心的数组
	// value = {test:'1',a:'b'}
	// {...value} = {test:'1',a:'b'}
	const res = data.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	});

	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000007\s001,action_id:' + action_id);
	return {
		data: res
	};
}
