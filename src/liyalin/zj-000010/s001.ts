import operate from '@dfeidao/atom-nodejs/db/operate';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { systemid } from '../../atom/config';
interface Message {
	nos: string[];
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000010\s001,action_id:' + action_id);
	const { exec, del } = operate(action_id, session_id, systemid);

	del('qu_hidden_feat_st', { standard_no: { $in: message.nos } });

	del('qu_check_result', { standard_no: { $in: message.nos } });

	const r = await exec();

	log('Service end path:c:\Users\李亚霖\Desktop\test-js\src\liyalin\zj-000010\s001,action_id:' + action_id);
	return {
		data: r
	};
}
