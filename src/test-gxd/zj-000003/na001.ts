import { IFeidaoAiNodejsComponent } from '@dfeidao/atom-nodejs/interfaces';
import nodejs from '@dfeidao/atom-nodejs/msg/nodejs';
import render from '@dfeidao/atom-nodejs/render/render';
import p001 from "./p001";

export default async function na001(fd: IFeidaoAiNodejsComponent) {
	// todo
	console.log(fd);
	const page_no = fd.data.params['page-no'] as string || '1';
	const res = await nodejs(fd.data.actionid, fd.data.sessionid, 'test-gxd/zj-000003/s001', {
		filter: {
			pn: Number(page_no),
			feature_name: '',
			hidden_name: ''
		}
	});
	console.log(res);
	render(fd.data.node, res, p001, 'p001');
}
