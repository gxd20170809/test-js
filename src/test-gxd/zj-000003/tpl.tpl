<div class="display-" style="padding:0 50px 20px 20px;">
	<div class="b-box" style="height:70px;display:flex;align-items:center;justify-content:space-between;margin-bottom:20px;padding-left:20px;">
		<div style="display:flex;align-items:center;">
			<span class="font12 color-3">特性名称：</span>
			<input type="text" data-id="feature_name">
			<span class="font12 color-3" style="margin-left:20px;">隐患源名称：</span><input type="text" data-id="hidden_name">
			<div class="ht30 l-ht30 bg-theme text-c cursor-p" style="width:30px;border-radius: 5px;margin-left:20px;" data-feidao-actions='click:a001'>
				<i class="iconfont icon-sousuo color-f font16"></i>
			</div>
		</div>
		<div>
			<input type="button" class="btn" style="width:80px;margin-left:20px;" value="新增" data-feidao-actions='click:a005'>
			<input type="button" class="btn" style="width:80px;margin-left:20px;" value="删除">
		</div>
	</div>
	<!--页签1定量标准 start-->
	<!-- 表格 start -->
	<!--表头 start -->
	<div class="display-">
		<div class="t_title">
			<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
				<input type="checkbox" style="width:25px;" data-feidao-actions='click:a003' data-id="all">
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:11%;">特性代码
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;" data-feidao-actions='click:a002' data-id="hidden_source_feature_code" data-flag="1" data-asc="desc"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:11%;">特性名称
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;" data-feidao-actions='click:a002' data-id="hidden_source_feature_name" data-flag="1" data-asc="desc"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:10%;">隐患源代码
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:10%;">隐患源名称
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:12%;">图片
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:11%;">公司
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:8%;">区域
				<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"></i>
			</span><span class="display-ib p5 border-r-1 text-c b-box" style="width:8%;">目标值
			</span><span class="display-ib p5 border-r-1 text-c b-box" style="width:8%;">检查结果个数
			</span><span class="display-ib p5 border-r-1 text-c b-box" style="width:8%;">操作
			</span>
		</div>
		<!--表头 end-->
		<!-- 表体 start -->
		<div class="t_body display-" data-feidao-presentation='p001'>

		</div>
	</div>
	<!-- 表体 end -->
	<!-- 表格 end -->
	<!--页签1定量标准 end-->

	
	
</div>