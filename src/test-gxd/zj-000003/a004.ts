import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// todo
	const ns = fd.data.node.querySelectorAll('[data-id="list"]');

	const ns_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');

	const all = fd.data.node.querySelector<HTMLInputElement>('[data-id="all"]');

	if (Array.from(ns).length === Array.from(ns_chk).length) {
		all.checked = true;
	} else {
		all.checked = false;
	}
}
