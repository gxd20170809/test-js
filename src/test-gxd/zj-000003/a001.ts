import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from "@dfeidao/atom-web/render/render";
import p001 from './p001';

export default async function a001(fd: IFeidaoAiBrowserComponent) {
	// todo
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name"]');
	const v = n.value;
	const n2 = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v2 = n2.value;
	const res = await nodejs('test-gxd/zj-000003/s001', {
		filter: {
			pn: 1,
			feature_name: v,
			hidden_name: v2
		}
	});
	render(fd, res, p001, 'p001', 'inner');
}
