import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';

export default async function a002(fd: IFeidaoAiBrowserComponent, e: Event) {
	const n = e.target as HTMLDivElement;
	const v = n.getAttribute('data-id');

	const ns = fd.data.node.querySelectorAll('[data-asc="desc"]');
	Array.from(ns).forEach((value: HTMLDivElement) => {
		value.setAttribute('class', 'iconfont cursor-p icon-shangxia color-3 font14 pos-a');
	});

	const flag = n.getAttribute('data-flag');
	const asc: string[] = [];
	const desc: string[] = [];
	if (flag === '1') {
		// 升序
		n.setAttribute('data-flag', '0');
		n.setAttribute('class', 'iconfont cursor-p icon-shang color-3 font14 pos-a');
		asc.push(v);
	} else {
		// 降序
		n.setAttribute('data-flag', '1');
		n.setAttribute('class', 'iconfont cursor-p icon-xia color-3 font14 pos-a');
		desc.push(v);
	}
	const n3 = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name"]');
	const v3 = n3.value;
	const n2 = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v2 = n2.value;
	const res = await nodejs('test-gxd/zj-000003/s001', {
		filter: {
			pn: 1,
			feature_name: v3,
			hidden_name: v2,
			asc,
			desc
		}
	});
	render(fd, res, p001, 'p001', 'inner');
}
