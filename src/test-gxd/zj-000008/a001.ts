import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';

export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="hidden"]');
	set_node_cls(n, 'display-n', false);

	set(fd, 'flag', flag);
}
