import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';

export default async function a003(fd: IFeidaoAiBrowserComponent) {
	// todo
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const res = await nodejs<{ res: Array<{}>; c: number; }>('test-gxd/zj-000008', {
		filter: {
			hidden_name: v
		},
		page_no: 1
	});
	render(fd, res, p001, 'p001', 'inner');
}
