import count from "@dfeidao/atom-nodejs/db/count";
import query from "@dfeidao/atom-nodejs/db/query";
import log from '@dfeidao/atom-nodejs/logger/log';
import { productid, systemid } from '../../atom/config';
interface Message {
	filter: {
		hidden_name: string
	};
	page_no: number;
}

// 表名	hazard_source
// 标题	隐患源
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// productid	产品ID	string	50	√	ⅹ
// hidden_source	隐患源编号	string	32	√	ⅹ
// duty_department_no	责任部门编号	string	50	√	ⅹ
// duty_department	责任部门名称	string	50	√	ⅹ
// responsible	责任人名称	string	50	√	ⅹ
// responsible_task_no	任务责任人编号	string	50	√	ⅹ
// hidden_code	隐患源代码	string	32	√	ⅹ
// hidden_name	隐患源名称	string	32	√	ⅹ
// control_level	管控级别	string	32	√	ⅹ
// control_grade	管控层级	string	32	√	ⅹ
// assess_level	评价级别	string	32	√	ⅹ
// engineer_skill	工程技术	string	128	√	ⅹ
// manage_measure	管理措施	string	128	√	ⅹ
// train_education	培训教育	string	128	√	ⅹ
// emergency_measures	应急处置措施	string	128	√	ⅹ
// personal_protect	个体防护	string	128	√	ⅹ


export default async function atom(msg: Message, action_id: string, session_id: string) {
	log('Service begin');
	const { prepare, exec } = query(action_id, session_id, systemid);
	prepare('hazard_source', ['hidden_code', 'hidden_name', 'control_level', 'control_grade', 'assess_level', 'hidden_source'], { productid, hidden_name: { $like: '%' + msg.filter.hidden_name + '%' } }, 2, msg.page_no, [], []);
	const [res] = await exec();

	const d = res.map((value) => {
		// value = {test:'test1',a:'b'}
		// {test:'test1',a:'b'} === {...value}
		return { ...value, v: JSON.stringify(value) };
	});


	// todo
	const { prepare: p, exec: e } = count(action_id, session_id, systemid);
	p('hazard_source', { productid, hidden_name: { $like: '%' + msg.filter.hidden_name + '%' } });
	const [c] = await e();
	log('Service end');
	return {
		data: { res: d, c }
	};
}
