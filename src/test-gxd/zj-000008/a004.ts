import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import Paging from "@dfeidao/fd-w000010";
import p001 from './p001';

export default async function a004(fd: IFeidaoAiBrowserComponent, e: { page_no: number; }) {
	console.log(e);
	// todo
	const page_no = e.page_no;
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const res = await nodejs<{ res: Array<{}>; c: number; }>('test-gxd/zj-000008/s001', {
		filter: {
			hidden_name: v
		},
		page_no
	});
	render(fd, res, p001, 'p001', 'inner');

	const p = fd.data.node.querySelector<Paging>('[data-id="paging"]');
	p.setAttribute('page-no', page_no + '');
}
