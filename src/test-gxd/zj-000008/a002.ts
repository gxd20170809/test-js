import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';

export default async function a002(fd: IFeidaoAiBrowserComponent) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="hidden"]');
	set_node_cls(n, 'display-n', true);
}
