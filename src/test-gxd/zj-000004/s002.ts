import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	plan_cla_attr_no: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:f:\培训\项目\feidao-4.6\test-js\src\test-gxd\zj-000004\s002,action_id:' + action_id);
	const { prepare, exec } = query(action_id, session_id, systemid);
	prepare('plan_type', ['plan_type_no', 'plan_type_name'], { productid, plan_cla_attr_no: message.plan_cla_attr_no }, 200, 1, [], []);
	const [res] = await exec();
	log('Service end path:f:\培训\项目\feidao-4.6\test-js\src\test-gxd\zj-000004\s002,action_id:' + action_id);
	return {
		data: res
	};
}
