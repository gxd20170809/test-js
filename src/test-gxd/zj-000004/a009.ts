import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a009(fd: IFeidaoAiBrowserComponent, data: { companyname: string; hidden_source_feature_name: string; hidden_source_feature_code: string; }) {
	// todo
	const c = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_code"]');
	c.innerHTML = data.hidden_source_feature_code;
	const f = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name"]');
	const companyname = fd.data.node.querySelector<HTMLInputElement>('[data-id="companyname"]');
	f.value = data.hidden_source_feature_name;
	companyname.value = data.companyname;
	set(fd, 'feature_info', data);
}
