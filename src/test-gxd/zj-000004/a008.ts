import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';

export default async function a008(fd: IFeidaoAiBrowserComponent, data: { hidden_name: string; hidden_source: string; }) {
	// todo
	console.log(data, 'a008---------');
	const code = fd.data.node.querySelector<HTMLSpanElement>('[data-id="hidden_source"]');
	code.innerHTML = data.hidden_source;

	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	name.value = data.hidden_name;
	set(fd, 'hidden_info', data);
}
