import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p003 from './p003';
export default async function a007(fd: IFeidaoAiBrowserComponent) {
	// todo
	const n = fd.data.node.querySelector<HTMLSelectElement>('[data-id="plan"]');
	const v = n.value;
	const res = await nodejs<Array<{}>>('test-gxd/zj-000004/s002', {
		plan_cla_attr_no: v
	});
	render(fd, res, p003, 'p003', 'inner');
}
