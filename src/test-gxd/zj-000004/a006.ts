import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
import p002 from './p002';

export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// todo
	const res = await nodejs<{ res: Array<{}>; data: Array<{}>; }>('test-gxd/zj-000004/s001', {});
	console.log(res);

	// 区域数据
	render(fd, res.res, p001, 'p001', 'after');
	// 计划分类属性
	render(fd, res.data, p002, 'p002', 'after');
}
