import init from '@dfeidao/web/component';

import s from './s';

/// FEIDAO IMPACTIONS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)
import a001 from './a001';
import a002 from './a002';

/// FEIDAO IMPACTIONS END

export default function main(url: string, query: {}) {
	/// FEIDAO ACTIONS BEGIN
	/// 请不要修改下面的代码哟(๑•ω•๑)
	const actions = { a001, a002 };

	/// FEIDAO ACTIONS END
	return init('zj-000007', s, actions, url, query);
}
