export default `<div class="bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12 display-n" style="z-index:120;">
	<div style="width:340px;padding-top:100px;padding-bottom:50px;margin:0 auto;">
		<div class="bg-f overflow-h text-l" style="border-radius:4px">
			<div class="font14 bg-theme color-f ht40 l-ht40" style="padding:0 10px;border-top-left-radius: 4px;border-top-right-radius: 4px">
				<i class="icon iconfont icon-cuo fr text-c cursor-p font12" title="close" data-feidao-actions="click:eu-001"></i>提示
			</div>
			<div class="p20">
				<div class="ht40 l-ht40 text-c color-6">
					<span>是否删除？</span>
				</div>
				<div class="text-c" style="margin-top: 20px;">
					<input type="button" value="确认" class="btn" style="width: 50px;line-height: 20px" />
					<input type="button" value="否" class="cbtn" style="width: 50px;margin-left: 60px;" />
				</div>
			</div>
		</div>
	</div>
</div>`;
