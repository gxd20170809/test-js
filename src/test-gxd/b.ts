import init from '@dfeidao/web/page';

import s from './s';

/// FEIDAO IMPCOMPONENTS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)
import c0 from './zj-000001/b';
import c1 from './zj-000002/b';
import c2 from './zj-000003/b';
import c3 from './zj-000004/b';
import c4 from './zj-000005/b';
import c5 from './zj-000006/b';
import c6 from './zj-000007/b';
import c7 from './zj-000008/b';
import c8 from './zj-000009/b';
import c9 from './zj-000010/b';

/// FEIDAO IMPCOMPONENTS END


/// FEIDAO IMPACTIONS BEGIN
/// 请不要修改下面的代码哟(๑•ω•๑)

/// FEIDAO IMPACTIONS END

import '@dfeidao/fd-w000009';
import '@dfeidao/fd-w000010';

(() => {
	/// FEIDAO ACTIONS BEGIN
	/// 请不要修改下面的代码哟(๑•ω•๑)

	const actions = {};

	/// FEIDAO ACTIONS END

	init(s, actions
		/// FEIDAO COMPONENTS BEGIN
		/// 请不要修改下面的代码哟(๑•ω•๑)
		, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9

		/// FEIDAO COMPONENTS END
	);
})();

