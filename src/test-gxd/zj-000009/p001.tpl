{{~ it.res:value:index}}
<div class="tableRow b-box" data-feidao-actions="click:a005" data-name="hidden" data-v='{{=value.v}}'>
	<span class="display-ib p5 border-r-1 text-c b-box" style="width:190px;">{{=value.hidden_source_feature_code}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h" style="width:190px;" title="{{=value.hidden_source_feature_name}}">{{=value.hidden_source_feature_name}}
	</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:190px;" title="{{=value.companyname}}">{{=value.companyname}}
	</span>
</div>
{{~}}
{{? it.res.length === 0}}
<!--筛选结果为空的蒙版 start-->
<div class="none display-">
	<i class="icon iconfont icon-wushuju display-ib text-c vertical-a-t color-9" style="font-size:100px;"></i>
	<div class="font22 color-9" style="height:50px;line-height:50px;">当前筛选条件下，没有匹配的数据</div>
</div>
<!--筛选结果为空的蒙版 end-->
{{??}}
<!--分页 start-->
<div class="page">
	<fd-w000010 data-feidao-actions="fdwe-change:a003" page-no="1" size=2 total={{=it.c}} max-btn-num=4 show-first show-last show-goto show-total lang="zh_CN">
</fd-w000010>
</div>
<!--分页 end-->
{{?}}