export default `<div class="bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12 display-n" style="z-index:120;" data-id="hidden-res">
	<div style="width:620px;padding-top:100px;padding-bottom:50px;margin:0 auto;">
		<div class="bg-f overflow-h text-l" style="border-radius:4px">
			<div class="font14 bg-theme color-f ht40 l-ht40" style="padding:0 10px;border-top-left-radius: 4px;border-top-right-radius: 4px">
				<i class="icon iconfont icon-cuo fr text-c cursor-p font12" title="close" data-feidao-actions="click:a002"></i>隐患源特性集
			</div>
			<div>
				<!-- 筛选条件 start -->
				<div class="ht50 l-ht50" style="padding-left:20px">
					<div class="display-ib" style="margin-right:20px;">
						<input type="text" data-id="feature_name">
					</div>
					<div class="display-ib bg-theme text-c cursor-p ht30 l-ht30 radius-4" style="width:30px;margin-right:50px" data-feidao-actions="click:a004">
						<i class="iconfont icon-sousuo color-f" title="搜索"></i>
					</div>
					<input type="button" class="btn " style="width:50px;line-height:20px;margin-left: 290px" value="确定" data-feidao-actions="click:a006">
				</div>
				<!-- 筛选条件 end -->
				<div class="p20" style="min-height:200px">
					<!-- 表格 start -->
					<!--表头 start -->
					<div class="t_title b-box" style="width:570px;">
						<span class="display-ib p5 border-r-1 text-c b-box" style="width:190px;">隐患源特性代码
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:190px;">隐患源特性名称
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:190px;">公司
						</span>
					</div>
					<!--表头 end-->
					<!-- 表体 start -->
					<div class="t_body display-" style="width:571px;" data-feidao-presentation='p001'>
						
					</div>
					<!-- 表体 end -->
					<!-- 表格 end -->
					
				</div>
			</div>
		</div>
	</div>
</div>`;
