import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from './a002';

export default async function a006(fd: IFeidaoAiBrowserComponent) {
	// todo
	const data = get(fd, 'data-s');
	if (!data) {
		alert('请选择一行数据');
	} else {
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000004', 'a009', data);
		}
		await a002(fd);
	}
}
