import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import set_nodes_cls from '@dfeidao/atom-web/ui/set-nodes-cls';

export default async function a005(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	const ns = fd.data.node.querySelectorAll('[data-name="hidden"]');
	const arr = Array.from(ns) as HTMLElement[];
	set_nodes_cls(arr, 'bg-select', false);

	const n = e.currentTarget as HTMLDivElement;
	set_node_cls(n, 'bg-select', true);

	const d = JSON.parse(n.getAttribute('data-v'));
	console.log(d);
	set(fd, 'data-s', d);
}
