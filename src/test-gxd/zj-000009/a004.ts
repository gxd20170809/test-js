import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';

export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// todo
	const data = get(fd, 'data') as { hidden_source: string };
	// todo
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name"]');
	const v = n.value;
	const res = await nodejs<{ res: Array<{}>; c: number; }>('test-gxd/zj-000009/s001', {
		filter: {
			hidden_source: data.hidden_source,
			hidden_source_feature_name: v
		},
		page_no: 1
	});
	render(fd, res, p001, 'p001', 'inner');
}
