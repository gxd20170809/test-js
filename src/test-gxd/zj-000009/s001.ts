import count from "@dfeidao/atom-nodejs/db/count";
import query from "@dfeidao/atom-nodejs/db/query";
import log from '@dfeidao/atom-nodejs/logger/log';
import { productid, systemid } from '../../atom/config';

interface Message {
	filter: {
		hidden_source: string;
		hidden_source_feature_name: string;
	};
	page_no: number;
}

// 表名	hazard_source_feature
// 标题	隐患源特性
// 字段名称	字段标题	字段类型	字段长度	是否为空	是否主键
// _id	id	string	50	ⅹ	√
// productid	产品ID	string	50	√	ⅹ
// hidden_source	隐患源编号	string	32	√	ⅹ
// companyid	公司编号	string	50	√	ⅹ
// companyname	公司名称	string	50	√	ⅹ
// factory_no	工厂编号	string	50	√	ⅹ
// factory_name	工厂名称	string	50	√	ⅹ
// hidden_source_feature_no	隐患源特性编号	string	50	√	ⅹ
// hidden_source_feature_name	隐患源特性名称	string	50	√	ⅹ
// hidden_source_feature_code	隐患源特性代码	string	50	√	ⅹ
// status_manage	状态管理	string	32	√	ⅹ
// control_data	控制数据	string	32	√	ⅹ


export default async function atom(msg: Message, action_id: string, session_id: string) {
	log('Service begin');
	// todo
	const { prepare, exec } = query(action_id, session_id, systemid);
	prepare('hazard_source_feature', ['companyname', 'companyid', 'hidden_source_feature_name', 'hidden_source_feature_code', 'hidden_source_feature_no'], {
		productid, hidden_source: msg.filter.hidden_source, hidden_source_feature_name: {
			$like: '%' + msg.filter.hidden_source_feature_name
				+ '%'
		}
	}, 2, msg.page_no, [], []);
	const [res] = await exec();

	const data = res.map((v) => {
		return { ...v, v: JSON.stringify(v) };
	});

	const { prepare: p, exec: e } = count(action_id, session_id, systemid);
	p('hazard_source_feature', {
		productid, hidden_source: msg.filter.hidden_source, hidden_source_feature_name: {
			$like: '%' + msg.filter.hidden_source_feature_name
				+ '%'
		}
	});

	const [c] = await e();

	log('Service end');
	return { data: { res: data, c } };
}
