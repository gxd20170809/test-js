import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import Paging from "@dfeidao/fd-w000010";
import p001 from './p001';

export default async function a003(fd: IFeidaoAiBrowserComponent, e: { page_no: number; }) {
	// todo
	// todo
	const data = get(fd, 'data') as { hidden_source: string };
	// todo
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="feature_name"]');
	const v = n.value;
	const res = await nodejs<{ res: Array<{}>; c: number; }>('test-gxd/zj-000009/s001', {
		filter: {
			hidden_source: data.hidden_source,
			hidden_source_feature_name: v
		},
		page_no: e.page_no
	});
	render(fd, res, p001, 'p001', 'inner');
	const p = fd.data.node.querySelector<Paging>('[data-id="paging"]');
	p.setAttribute('page-no', e.page_no + '');
}
