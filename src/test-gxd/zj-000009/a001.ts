import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import p001 from './p001';

export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string, info: { hidden_source: string; }) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="hidden-res"]');
	// { res: data, c }
	const rs = await nodejs<{ res: Array<{}>; c: number; }>('test-gxd/zj-000009/s001', {
		filter: {
			hidden_source: info.hidden_source,
			hidden_source_feature_name: ''
		},
		page_no: 1
	});
	render(fd, rs, p001, 'p001', 'inner');

	set_node_cls(n, 'display-n', false);

	set(fd, 'flag', flag);
	set(fd, 'data', info);
}
