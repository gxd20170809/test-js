export default `<div style="width:80%;margin:0 auto;">
	<div style="margin:20px 0;">
		<div class="display-ib vertical-a-t" style="width:45%;">
			<div class="text-l ht40 l-ht40">
				<span>模板：</span>
			</div>
			<div>
				<textarea data-feidao-id="template" cols="30" rows="10" style="width:100%;height:300px;resize: none"></textarea>
			</div>
		</div>
		<div class="display-ib vertical-a-t" style="width:45%;">
			<div class="text-l ht40 l-ht40">
				<span>数据：</span>
			</div>
			<div>
				<textarea data-feidao-id="data" cols="30" rows="10" style="width:100%;height:300px;resize: none"></textarea>
			</div>
		</div>
	</div>
	<div>
		<div>
			<span>结果：</span>
			<input type="button" class="btn" value="运行" style="width:100px;height:40px;" data-feidao-actions='click:a001'>
		</div>
		<div>
			<textarea data-feidao-id="result" cols="30" rows="10" style="width:90%;height:500px;resize: none"></textarea>
		</div>
	</div>
</div>`;
