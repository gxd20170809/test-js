import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';

interface Message {
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\Js\TypeScript\test-js\src\dotjs\zj-000001\s001,action_id:' + action_id);

	log('Service end path:e:\Js\TypeScript\test-js\src\dotjs\zj-000001\s001,action_id:' + action_id);
	return {
		data: 'feidao'
	};
}
