import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import error from "@dfeidao/atom-web/msgbox/error";
import render from "@dfeidao/atom-web/render/_data-render";
export default async function a001(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 运行
	const tpl = fd.data.node.querySelector<HTMLTextAreaElement>('[data-feidao-id="template"]').value;
	const data = fd.data.node.querySelector<HTMLTextAreaElement>('[data-feidao-id="data"]').value;
	if (!tpl.trim()) {
		await error('请输入模板');
		return;
	}
	if (!data.trim()) {
		await error('请输入数据');
		return;
	}
	try {
		const d = JSON.parse(data);
		const res = render(d, tpl);
		fd.data.node.querySelector<HTMLTextAreaElement>('[data-feidao-id="result"]').value = res;

	} catch (error) {
		fd.data.node.querySelector<HTMLTextAreaElement>('[data-feidao-id="result"]').value = error.message;
	}

}
