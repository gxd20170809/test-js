import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
export default async function a009(fd: IFeidaoAiBrowserComponent) {
	// todo
	const data = get(fd, 'hazard_source_feature') as { hidden_source_feature_no: string };
	// console.log(data.hidden_source_feature_no);
	if (data) {
		// 显示弹出框
		fire('zj-000007', 'a001', data.hidden_source_feature_no, 'add');
	} else {
		// 提示
		alert('请选择隐患源特性');
	}
}
