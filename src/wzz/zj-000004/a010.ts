import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import set from '@dfeidao/atom-web/local/set';
import render from "@dfeidao/atom-web/render/render";
import a012 from './a012';
import p004 from './p004';
export default async function a010(fd: IFeidaoAiBrowserComponent, data: Array<{ check_res_no: string, flag?: boolean }>) {
	// todo
	const d = get(fd, 'data') as Array<{ check_res_no: string, flag?: boolean }>;
	if (!d) {
		render(fd, data, p004, 'p004', 'inner');
		set(fd, 'data', data);
		await a012(fd);
	} else {
		// 做数据对比 把新的数据放入data变量里
		data.forEach(async (v) => {
			const no = v.check_res_no;
			const res = d.filter((dd) => {
				return dd.check_res_no === no;
			});
			if (res.length === 0) {
				// 新数据 放入d
				d.push(v);
			} else {
				for (let i = 0; i < d.length; i++) {
					const vl = d[i];
					if (vl.check_res_no === no) {
						vl.flag = true;
					}
				}
			}
			render(fd, d, p004, 'p004', 'inner');
			set(fd, 'data', d);
			await a012(fd);

		});
	}



}
