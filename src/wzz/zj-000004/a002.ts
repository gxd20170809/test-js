import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';
export default async function a002(fd: IFeidaoAiBrowserComponent) {
	// todo
	// 先把当前zj-000004的组件隐藏
	// 不能用 fd.data.node.style.display = 'none';这句话 否则第二次点击新增就什么都不显示了
	// 需使用和a001一样的设置属性的方法才行
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="add"]');
	n.setAttribute('class', "display-n");
	// 当前组件只能操纵组件内的东西 如果想操纵其他组件 应利用组件之间发送消息的原子操作 fdf快捷即可导入包
	// 上述组件间通信只在浏览器端有效
	// 调用事件 组件3的a006被组件4的a002调用
	fire('zj-000003', 'a006');
}
