import operate from "@dfeidao/atom-nodejs/db/operate";
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { systemid } from 'src/atom/config';

interface Message {
	nos: string[];
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\feidao\test-js\src\wzz\zj-000010\s001,action_id:' + action_id);

	const { del, exec } = operate(action_id, session_id, systemid);
	// 删除两张表中被选中的数据
	del('qu_jidden_feat_st', { standard_no: { $in: message.nos } });
	del('qu_check_result', { standard_no: { $in: message.nos } });

	const r = await exec();

	log('Service end path:e:\feidao\test-js\src\wzz\zj-000010\s001,action_id:' + action_id);
	return {
		data: r
	};
}
