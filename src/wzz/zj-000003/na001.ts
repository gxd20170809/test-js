import { IFeidaoAiNodejsComponent } from '@dfeidao/atom-nodejs/interfaces';
import nodejs from '@dfeidao/atom-nodejs/msg/nodejs';
import render from '@dfeidao/atom-nodejs/render/render';
import p001 from "./p001";
export default async function na001(fd: IFeidaoAiNodejsComponent) {
	// todo
	const page_no = fd.data.params['page-no'] as string || '1';
	const res = await nodejs(fd.data.actionid, fd.data.sessionid, 'wzz/zj-000003/s001', {
		page: {
			pn: Number(page_no),
			feature_name: '',
			hidden_name: ''
		}
	});
	console.log(res);
	console.log(fd);


	// console.log(page_no);
	// data-  开头的是自定义属性
	// 数据渲染 这里的渲染和客户端的渲染不同 一是包名不同 二是所需参数也不同
	render(fd.data.node, res, p001, 'p001');
}
