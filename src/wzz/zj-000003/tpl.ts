export default `<!-- 内容 start -->
<div class="w100" style="margin-top: 60px;height: calc(100% - 60px);background: #EAEDF2;">
	<!-- 页面名称 start -->
	<div class="ht40 l-ht40 bg-f pos-f font14 color-3" style="left:200px;right:0;z-index: 4;padding-left:20px ">
		<i class="iconfont icon-weizhang"></i>
		<span>隐患源特性标准</span>
	</div>
	<!-- 页面名称 end -->
	<!-- 主内容 start -->
	<div style="padding-left:220px;padding-top: 60px;">
		<div class="bg-f">
			<!--  start -->
			<div class="display-" style="padding:0 50px 20px 20px;">
				<div class="b-box"
					style="height:70px;display:flex;align-items:center;justify-content:space-between;margin-bottom:20px;padding-left:20px;">
					<div style="display:flex;align-items:center;">
						<span class="font12 color-3">特性名称：</span>
						<input type="text" data-id="feature_name">
						<span class="font12 color-3" style="margin-left:20px;">隐患源名称：</span><input type="text"
							data-id="hidden_name">
						<div class="ht30 l-ht30 bg-theme text-c cursor-p"
							style="width:30px;border-radius: 5px;margin-left:20px;" data-feidao-actions='click:a001'>
							<!--多个为data-feidao-actions='click:a001,mouseover:a002'-->
							<i class="iconfont icon-sousuo color-f font16"></i>
						</div>
					</div>
					<div>
						<input type="button" class="btn" style="width:80px;margin-left:20px;" value="新增"
							data-feidao-actions='click:a005'>
						<input type="button" class="btn" style="width:80px;margin-left:20px;" value="删除"
							data-feidao-actions='click:a007'>
					</div>
				</div>
				<!--页签1定量标准 start-->
				<!-- 表格 start -->
				<!--表头 start -->
				<div class="display-" data-feidao-presentation=''>
					<div class="t_title">
						<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
							<input type="checkbox" style="width:25px;" data-feidao-actions='click:a003' data-id="all">
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:11%;">特性代码
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"
								data-feidao-actions='click:a002' data-id="hidden_source_feature_code" data-flag="1"
								data-asc="desc"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:11%;">特性名称
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"
								data-feidao-actions='click:a002' data-id="hidden_source_feature_name" data-id="1"
								data-asc="desc"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:10%;">隐患源代码
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"
								data-feidao-actions='click:a002' data-id="hidden_code" data-flag="1"
								data-asc="desc"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:10%;">隐患源名称
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"
								data-feidao-actions='click:a002' data-id="hidden_name" data-flag="1"
								data-asc="desc"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:12%;">图片
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a"
								style="right: 5px;top:5px;"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:11%;">公司
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"
								data-feidao-actions='click:a002' data-id="companyname" data-flag="1"
								data-asc="desc"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box pos-r" style="width:8%;">区域
							<i class="iconfont cursor-p icon-shangxia color-3 font14 pos-a" style="right: 5px;top:5px;"
								data-feidao-actions='click:a002' data-id="area_name" data-flag="1" data-asc="desc"></i>
						</span><span class="display-ib p5 border-r-1 text-c b-box" style="width:8%;">目标值
						</span><span class="display-ib p5 border-r-1 text-c b-box" style="width:8%;">检查结果个数
						</span><span class="display-ib p5 border-r-1 text-c b-box" style="width:8%;">操作
						</span>
					</div>
					<!--表头 end-->
					<!-- 表体 start -->
					<div class="t_body display-" data-feidao-presentation='p001'>
						<div class="tableRow">
							<span class="display-ib p5 border-r-1 text-c b-box" style="width:3%;">
								<input type="checkbox">
							</span><span
								class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h text-c color-theme"
								style="width:11%;" title="">YL004568
							</span><span class="display-ib p5 border-r-1 vertical-a-t text-l b-box overflow-h"
								style="width:11%;" title="">
								声音
							</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
								style="width:10%;" title="">YH0054682
							</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h"
								style="width:10%;" title="">变压器
							</span><img
								src="http://a.hiphotos.baidu.com/image/pic/item/4610b912c8fcc3cea3aba0619c45d688d43f207c.jpg"
								alt="" style="width: 11.75%;height: 20px;">
							</span><span class="display-ib p5 border-r-1 vertical-a-t b-box overflow-h"
								style="width:11%;" title="">郑州前程科技有限公司
							</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
								style="width:8%;" title="">A31
							</span><span class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h"
								style="width:8%;" title="">30</span><span
								class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:8%;"
								title="">3</span><span
								class="display-ib p5 border-r-1 vertical-a-t text-c b-box overflow-h" style="width:8%;"
								title="">
								<i class="iconfont cursor-p icon-xiugai color-theme display-ib"
									style="width:30px;"></i><i
									class="iconfont icon-shanchu1 cursor-p display-ib color-red" style="margin:0 10px;"
									title="删除"></i>
							</span>
						</div>
					</div>
				</div>
				<!-- 表体 end -->
				<!-- 表格 end -->
				<!--页签1定量标准 end-->


				<!--筛选结果为空的蒙版 end-->
				<!--分页 start-->

				<!--分页 end-->
			</div>
			<!--  end -->
			<!--  新增“定量标准” start -->

			<!--  新增“定量标准” end -->

			<!--  详情“定量标准” start -->

			<!--  详情“定量标准” end -->
			<!--  修改“定量标准” start -->

			<!--  修改“定量标准” end -->

		</div>
	</div>
	<!-- 主内容 end -->
</div>
<!-- 内容 end -->`;
