import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import fire from '@dfeidao/atom-web/msg/fire';
export default async function a007(fd: IFeidaoAiBrowserComponent, e: Event) {
	// todo
	const ns_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');
	const nodes = Array.from(ns_chk);
	if (nodes.length === 0) {
		alert('请至少选中一列数据');
	} else {
		const nos = nodes.map((v) => {
			return v.getAttribute("data-no");
		});
		fire('zj-000010', 'a001', nos);

	}
}
