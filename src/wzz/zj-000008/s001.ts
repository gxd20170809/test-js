import count from '@dfeidao/atom-nodejs/db/count';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';
// 定义接受的参数结构  用于接收页面方式送来的参数  以便于查询指定数据
interface Message {
	page: {
		pn: number;
		hidden_name: string;
	};
	// cookie: {
	// 	uk: string;
	// 	[key: string]: string
	// };
	// urls: {
	// 	base: string;
	// 	origin: string;
	// 	url: string;
	// };
	// query: {};
	// params: {};
	// headers: {};
	// captcha: string;
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\feidao\test-js\src\wzz\zj-000008\s001,action_id:' + action_id);
	// , hidden_source_feature_name: { $like: '%' + message.page.feature_name }
	const q = query(action_id, session_id, systemid);
	q.prepare('hazard_source',
		['hidden_source', 'hidden_code', 'hidden_name', 'control_grade', 'control_level', 'assess_level'],
		{ productid, hidden_name: { $like: '%' + message.page.hidden_name + '%' } }, 2, message.page.pn, [], []);
	const [data] = await q.exec();
	// map : 对数组的每个值 执行箭头函数 返回的值放入心的数组
	// value = {test:'1',a:'b'}
	// {...value} = {test:'1',a:'b'}
	const d = data.map((value) => {
		return { ...value, v: JSON.stringify(value) };
	});


	const c = count(action_id, session_id, systemid);
	c.prepare('hazard_source', { productid, hidden_name: { $like: '%' + message.page.hidden_name + '%' } });
	const n = await c.exec();

	log('Service end path:c:\e:\feidao\test-js\src\wzz\zj-000008\s001,action_id:' + action_id);
	return {
		data: { data: d, n }
	};
}
