import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from '../zj-000008/a002';
export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	// global 存在当前页签上的 没有办法共享到其他页签 里面的数据只在当前页签有效 浏览器或页签关闭会清除掉
	// local 局部变量  只能是 同一个组件 不同的相应文件之间进行传递参数 在一个组件内使用
	// storage 缓存操作 管理到浏览器的local Storage里面  放在本地缓存 只有手动清除才能消失
	// 这里是a004和a005之间使用值 local更合适
	// console.log(fd);
	const data = get(fd, 'data');
	// data如果在没点击一行的时候 会是 未定义
	if (data) {
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			// fire的剩余参数，就是要传过去的参数
			fire('zj-000004', 'a006', data);
		} else {
			// 修改组件
		}

		await a002(fd);
	} else {
		alert('请选择一条数据');
	}
}
