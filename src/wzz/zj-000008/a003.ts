import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import Paging from "@dfeidao/fd-w000010";
import p001 from './p001';

export default async function a003(fd: IFeidaoAiBrowserComponent, a: { page_no: number }) {

	console.log(a);
	const p = a.page_no;
	const n = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_name"]');
	const v = n.value;
	const p2 = Number(p) || 1;
	const res = await nodejs('wzz/zj-000008/s001', {
		page: {
			pn: p2,
			hidden_name: v
		}
	});
	console.log(res);
	console.log("组件8 a003");
	render(fd, res, p001, 'p001', 'inner');
	// 引入控件包 转换为控件类型
	const paging = fd.data.node.querySelector<Paging>('[data-id="paging"]');
	// setAttribute属性第二个参数为string型 需将数字转化为字符串
	paging.setAttribute('page-no', p2 + '');
}
