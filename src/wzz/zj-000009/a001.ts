import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
export default async function a001(fd: IFeidaoAiBrowserComponent, flag: string, info: { hidden_source: string }) {
	// todo
	// 初始化需要参数 不能是na001 在这里渲染
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="main"]');
	n.setAttribute('class', "bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12");

	set(fd, 'flag', flag);
	set(fd, 'info_hidden_source', info.hidden_source);

	const name = fd.data.node.querySelector<HTMLInputElement>('[data-id="hidden_source_feature_name"]');
	const v = name.value;
	console.log(v);
	const res = await nodejs('wzz/zj-000009/s001', {
		page: {
			pn: 1,
			hidden_source: info.hidden_source,
			hidden_source_feature_name: ''
		}
	});
	console.log(res);
	render(fd, res, p001, 'p001', 'inner');
}
