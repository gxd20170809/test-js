export default `{{~it:value:index}}
<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
			隐患源编号：
		</span>
		<i class="iconfont icon-icon02 color-theme font20 vertical-a-m" data-feidao-actions='click:a003'></i>
		<span style="margin-left:5px;" class="font12 color-6 text-r b-box display-ib vertical-a-m"
			data-id="hidden_source">{{=value.hidden_source}}</span>
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			隐患源名称：
		</span>
		<input type="text" style="width: 140px" data-id="hidden_source_feature_name" value="{{=value.hidden_name}}">
	</div>
</div>

<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>
			特性代码：
		</span>
		<i class="iconfont icon-icon02 color-theme font20 vertical-a-m"></i>
		<span style="margin-left:5px;" class="font12 color-6 text-r b-box display-ib vertical-a-m">
			{{=value.hidden_source_feature_no}}</span>
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			特性名称：
		</span>
		<input type="text" style="width: 140px" value="{{=value.hidden_source_feature_name}}">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r  display-ib" style="width:100px;">
			公司：
		</span>
		<input type="text" style="width: 140px" value="{{=value.companyname}}">
	</div>
</div>

<div style="height:50px;display:flex;align-items: center;">

	<!--区域-->
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			区域：
		</span>
		<select class="select" style="width:140px;">
			<option>请选择</option>
			{{~value.res:v:i}}
			<option {{? value.area_name===v.optional_value}} selected{{?}} value="{{=v.optional_value}}">
				{{=v.optional_value}}
			</option>
			{{~}}
		</select>
	</div>

	<!--计划分类属性-->
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
			计划分类属性：
		</span>
		<select class="select" style="width:140px;">
			<option>请选择</option>
			{{~ivalue.res2[0]:v1:i1}}
			<option {{? value.area_name===v.optional_value}} selected{{?}} value="{{=v1.plan_cla_attr_name}}">
				{{=v1.plan_cla_attr_name}}
			</option>
			{{~}}
		</select>
	</div>

	<!--计划类型-->
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			<span class="color-red vertical-a-m b-box" style="margin-right:3px;">*</span>
			计划类型：
		</span>
		<select class="select" style="width:140px;">
			<option>请选择</option>
			{{~value.plan_type[0]:v2:i2}}
			<option {{? value.plan_type_no===v2.plan_type_no}} selected{{?}} value="{{=v2.plan_type_no}}">
				{{=v2.plan_type_name}}
			</option>
			{{~}}
		</select>
	</div>

</div>

<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>目标值：
		</span>
		<input type="text" style="width: 140px" value="{{=value.target}}">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>
			单位：
		</span>
		<input type="file" value="{{=value.unit}}">
		<img style="width: 20px;height: 20px;" src="{{=value.unit}}">
		<!--图片路径-->
	</div>
</div>

<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%" style="margin-right: 50px;">
		<span class="font12 color-6 text-r b-box display-ib " style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>规范上限：
		</span>
		<input type="text" style="width: 140px" value="{{=value.spec-ceil}}">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m  display-ib" style="margin-right:3px;">*</span>
			规范下限：
		</span>
		<input type="text" style="width: 140px" value="{{=value.spec-floor}}">
	</div>
</div>

<div style="height:50px;display:flex;align-items: center;">
	<div style="width:30%">
		<span class="font12 color-6 text-r b-box display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m" style="margin-right:3px;">*</span>上限：
		</span>
		<input type="text" style="width: 140px" value="{{=value.upper_limit}}">
	</div>
	<div style="width:30%">
		<span class="font12 color-6 text-r display-ib" style="width:100px;">
			<span class="color-red b-box vertical-a-m " style="margin-right:3px;">*</span>下限：
		</span>
		<input type="text" style="width: 140px" value="{{=value.down_value}}">
	</div>
</div>
{{~}}`;
