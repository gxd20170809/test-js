import complex_query from '@dfeidao/atom-nodejs/db/complex-query';
import query from '@dfeidao/atom-nodejs/db/query';
import log from '@dfeidao/atom-nodejs/logger/log';
import { IncomingHttpHeaders } from 'http';
import { productid, systemid } from '../../atom/config';

interface Message {
	filter: {
		standard_no: string;
	};
}

interface IWebResult {
	data: unknown;
	cookie?: {
		[name: string]: string;
	} | null;
	content_type?: string;
	headers?: {
		[key: string]: string;
	};
	attachment?: string;
	redirect?: string;
	status_code?: number;
}

export default async function atom(message: Message, action_id: string, session_id: string, headers: IncomingHttpHeaders): Promise<IWebResult> {
	log('Service begin path:e:\feidao\test-js\src\wzz\zj-000005\s001,action_id:' + action_id);

	const { prepare, exec } = query(action_id, session_id, systemid);
	prepare('qu-hidden_feat_st', ['standard_no', 'companyid', 'companyname', 'area_name', 'hidden_source', 'hidden_code', 'hidden_name', 'hidden_source_no', 'hidden_source_name', 'hidden_source_code', 'upper_limit', 'down_value', 'target', 'spec-ceil', "spec-floor", "plan_type_no", "plan_type_name", "plan_cla_attr_no", "plan_cla_attr_name", 'unit'], { productid, standard_no: message.filter.standard_no }, 1, 1, [], []);
	const [res1] = await exec();


	const q = complex_query(action_id, session_id, systemid);
	const res2 = await q.add_field('qu_check_result', 'standard_no', 'standard_no')
		.add_field('qu_check_result', 'check_res_no', 'check_res_no')
		.add_field('hidden_ck_res_set', 'check_res_des', 'check_res_des')
		.add_field('hidden_ck_res_set', 'is_abnormal', 'is_abnormal')
		.add_field('hidden_ck_res_set', 'spec_ceil', 'spec_ceil')
		.add_field('hidden_ck_res_set', 'spec_floor', 'spec_floor')
		.add_field('hidden_ck_res_set', 'unit', 'unit')
		.inner_join('qu_check_result', 'hidden_ck_res_set', ['check_res_no', 'check_res_no'])
		.where_eq('qu_check_result', 'standard_no', message.filter.standard_no)
		.where_eq('qu_check_result', 'productid', 'productid')
		.exec();


	log('Service end path:e:\feidao\test-js\src\wzz\zj-000005\s001,action_id:' + action_id);
	return {
		data: { res1, res2 }
	};
}
