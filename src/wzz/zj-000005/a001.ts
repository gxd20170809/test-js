import get_file_url from '@dfeidao/atom-web/file/get-file-url';
import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import fire from '@dfeidao/atom-web/msg/fire';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
// import set_node_cls from '@dfeidao/atom-web/ui/set-node-cls';
import { productid } from '../../atom/config';
import p001 from './p001';
import p002 from './p002';


export default async function a001(fd: IFeidaoAiBrowserComponent, no: number) {
	// todo
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="show"]');
	n.removeAttribute('class');

	const res = await nodejs<{ res1: Array<{ plan_cla_attr_no: string }>; res2: Array<{}>; }>('wzz/zj-000005/s001', {
		filter: {
			standard_no: no
		}
	});
	if (res.res1.length > 0) {
		// 获取到的前两个下拉框的值
		const d = await fire('zj-000004', 'a003');
		// 获取到的全页值
		const data = res.res1[0];

		const plan_cla_attr_no = data.plan_cla_attr_no;
		// 在一行里找到这个字段 并对这个值进行修改（修改后的值在返回结果里）
		const r = get_file_url(productid, 'unit', data);

		let plan_type = [] as Array<{}>;
		if (plan_cla_attr_no) {
			const r2 = await nodejs<{ res: Array<{}> }>('wzz/zj-000004/s002', {
				plan_cla_attr_no
			});
			plan_type = r2.res;
		}
		const render_data = Object.assign({ plan_type }, r, d);
		console.log(render_data);

		render(fd, [render_data], p001, 'p001', "inner");
		render(fd, res.res2, p002, 'p002', "inner");


		set(fd, 'no', no);
		set(fd, 'res', res);
	}

}
