import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import get from '@dfeidao/atom-web/local/get';
import fire from '@dfeidao/atom-web/msg/fire';
import a002 from './a002';
export default async function a005(fd: IFeidaoAiBrowserComponent) {
	// todo
	const ns_chk = fd.data.node.querySelectorAll('[data-id="list"]:checked');
	const list = Array.from(ns_chk) as HTMLInputElement[];
	if (list.length === 0) {
		alert('请选择至少一列数据');
	} else {
		const d = list.map((n) => {
			const v = JSON.parse(n.getAttribute('data-v'));
			return v;
		});
		console.log(d);
		const flag = get(fd, 'flag') as string;
		if (flag === 'add') {
			fire('zj-000004', 'a010', d);
		} else {

		}
		await a002(fd);

	}

}
