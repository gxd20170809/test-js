import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';
import set from '@dfeidao/atom-web/local/set';
import nodejs from '@dfeidao/atom-web/msg/nodejs';
import render from '@dfeidao/atom-web/render/render';
import p001 from './p001';
export default async function a001(fd: IFeidaoAiBrowserComponent, no: string, flag: string) {
	// todo
	console.log('组件7 a001');
	console.log(no);
	const n = fd.data.node.querySelector<HTMLDivElement>('[data-id="main"]');
	n.setAttribute('class', "bg-black-rgba6 pos-f pos-t0 pos-r0 pos-b0 pos-l0 overflow-a text-c ht100 w100 font12");

	set(fd, 'flag', flag);
	set(fd, 'no', no);

	const res = await nodejs<Array<{}>>('wzz/zj-000007/s001', {
		no
	});
	console.log(res);
	render(fd, res, p001, 'p001', 'inner');

}
