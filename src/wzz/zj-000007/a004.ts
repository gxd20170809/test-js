import { IFeidaoAiBrowserComponent } from '@dfeidao/atom-web/interfaces';

export default async function a004(fd: IFeidaoAiBrowserComponent) {
	// todo
	const ns = fd.data.node.querySelectorAll('[data-id="list"]');
	const all = fd.data.node.querySelector<HTMLInputElement>('[data-id="all"]');
	Array.from(ns).forEach((n: HTMLInputElement) => {
		n.checked = all.checked;
	});
}
