const gulp = require('gulp');
const shell = require('gulp-shell');
const colors = require('colors');
const spaceid = require('./feidao.json').spaceid;

gulp.task('test:build-html', (cb) => {
	const through = require('through2');
	const rename = require('gulp-rename');
	const path = require('path');
	const minifyHtml = require("gulp-minify-html");
	const glob = require('glob');
	const packages = [{
		name: 'daoke',
		location: '../dist/'
	}, {
		name: 'js-cookie',
		location: 'js-cookie/src/',
		main: 'js.cookie'
	}, {
		name: '@dfeidao/core',
		location: '@dfeidao/core/umd'
	}, {
		name: '@dfeidao/send-msg',
		location: '@dfeidao/send-msg/umd'
	}, {
		name: '@dfeidao/web',
		location: '@dfeidao/web/umd'
	}, {
		name: '@dfeidao/nodejs',
		location: '@dfeidao/nodejs/umd'
	}, {
		name: '@dfeidao/atom-web',
		location: '@dfeidao/atom-web/umd/'
	}, {
		name: '@dfeidao/atom-nodejs',
		location: '@dfeidao/atom-nodejs/umd/'
	}, {
		name: '@dfeidao/atom-nodejs/msg',
		location: '@dfeidao/atom-nodejs/umd/msg-b'
	}, {
		name: '@dfeidao/atom-nodejs/authorization',
		location: '@dfeidao/atom-nodejs/umd/authorization-b'
	}, {
		name: '@dfeidao/animation',
		location: '@dfeidao/animation/umd',
		main: 'index'
	}, {
		name: 'node-html-parser',
		location: 'node-html-parser/dist/umd',
		main: 'index'
	}, {
		name: 'mqtt',
		location: './mqtt/dist',
		main: 'mqtt'
	}, {
		name: 'dot',
		location: 'dot',
		main: 'doT'
	}, {
		name: 'nools-ts',
		location: 'nools-ts/umd'
	}, {
		name: 'lodash-ts',
		location: 'lodash-ts/umd'
	}, {
		name: 'he',
		location: './he',
		main: 'he.js'
	}, {
		name: 'mathjs',
		location: 'mathjs/dist/',
		main: 'math.min.js'
	}];
	const keys = new Set();
	packages.forEach((p) => {
		keys.add(p.name);
	});
	glob('./node_modules/**/amd.json', (err, files) => {
		files.forEach((file) => {
			const pkgs = require(file);
			pkgs.forEach((p) => {
				const name = p.name;
				if (!!name && keys.has(name) === false) {
					keys.add(name);
					packages.push(p);
				}
			});
		});
		const head = Buffer.from(`<!DOCTYPE html>
				<html>

				<head>
					<meta charset="UTF-8">
					<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
					<meta name="apple-mobile-web-app-capable" content="yes" />
					<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
					<meta name="renderer" content="webkit">
					<link rel="stylesheet" type="text/css" href="../css/iconfont.css">
					<link href="https://cdn.jsdelivr.net/npm/feidao-css/feidao.css" type="text/css" rel="stylesheet">
					<link rel="stylesheet" type="text/css" href="../css/daoke.css">
					<script src="https://cdn.jsdelivr.net/npm/katex"></script>
					<script src="../../node_modules/@dojo/loader/loader.js"></script>
					<script type="text/javascript">
(function(){
	require.config({
		baseUrl: '../../node_modules/',
		packages: ${JSON.stringify(packages, null, '\t')}
	});
})();
					</script>
				</head>
				<body>`, 'utf8');
		gulp.src('./src/**/html.ts')
			.pipe(rename((file) => {
				const page_no = file.dirname;
				file.basename = page_no;
				file.extname = '.html';
				file.dirname = '../pages';
			}))
			.pipe(through.obj((chunk, enc, callback) => {
				const page_no = path.basename(chunk.path, '.html');
				const tail = Buffer.from(`
				<script type="text/javascript">
					function uuid(){
						return Math.random().toString().substr(2);
					}
					function get_sessionid(){
						const cookie = document.cookie;
						let sessionid = '';
						if (cookie) {
							if (cookie.indexOf('sessionid') > -1) {
								const coks = cookie.split(';');
								const sess = coks.filter((item) => {
									return item.indexOf('sessionid') > -1;
								});
								if (sess.length > 0) {
									const obj = {};
									sess.forEach((se) => {
										const info = se.trim().split('=');
										obj[info[0]] = info[1];
									});
									sessionid = obj.sessionid;
								}
							}
						}
						return sessionid;
					}
					(function(){
						window.host = \`http://\${window.location.hostname}:8889\`;
						window.file_host = 'http://192.168.40.196';
						window.mqtt_uri = 'mqtt://192.168.40.205:9001';
						require(['daoke/${page_no}/n'],function(main){
							var url = window.location.href;
							main.default(url, {cookie:{}}, {
								spaceid: '${spaceid}',
								sessionid: get_sessionid() || uuid(),
								actionid: uuid()
							}).then(function(res){
								// 去掉n返回页面的头部内容，只留body内容
								const data = res.data;
								const content = data.match(/<body[\\s|\\S]+>([\\s|\\S]+)<\\/body>/);
								document.body.innerHTML = content[0];
								require(['daoke/${page_no}/b']);
							});
						});
					})();
				</script>
				</body></html>`, 'utf8');
				// const body = chunk.contents.toString('utf8');
				chunk.contents = Buffer.concat([head, /*Buffer.from(body.substring(body.indexOf('`') + 1, body.lastIndexOf('`'))), */tail]);
				// this.push(chunk);
				// callback();
				callback(null, chunk);
			}))	// through.obj
			.pipe(minifyHtml()) //压缩
			.pipe(gulp.dest('./dist/pages/'))
			.on('end', cb);
	});	// glob
});

gulp.task('test:browser-sync', () => {
	const browserSync = require('browser-sync').create();
	browserSync.init({
		files: ['./dist/'],
		server: {
			baseDir: './dist/pages',
			directory: true
		},
		online: true,
		host: '127.0.0.1',
		open: 'external',
		serveStatic: ['./'],
		port: 8000
	});
});

gulp.task('test:watch-tpl', () => {
	const fs = require('fs');
	return gulp.watch('src/**/*.tpl')
		.on('change', (file) => {
			console.debug('bbbbblalala', file);
			const tpl = file.replace('.tpl', '.ts');
			lazy(file, tpl, 0);
		})
		.on('add', (file) => {
			console.debug('lalala', file);
			const tpl = file.replace('.tpl', '.ts');
			const content = fs.readFileSync(file, 'utf8');
			fs.writeFileSync(tpl, `export default \`${content}\`;\n`);
		});
});

function lazy(file, tpl, time) {
	const fs = require('fs');
	const content = fs.readFileSync(file, 'utf8');
	if (time > 50) {
		return;
	}
	if (!content) {
		setTimeout(() => {
			lazy(file, tpl, time + 1);
		}, 50);
	} else {
		fs.writeFileSync(tpl, `export default \`${content}\`;\n`);
	}
}

gulp.task('test:ts', shell.task(`npm run test:ts`));

gulp.task('test:server', shell.task(`feidao-server`));

gulp.task('build:tpl', () => {
	const through = require('through2');
	const rename = require('gulp-rename');
	return gulp.src('src/**/*.tpl')
		.pipe(rename((file) => {
			const page_no = 'tpl';
			file.extname = '.ts';
			file.basename = page_no;
		}))
		.pipe(through.obj((chunk, enc, callback) => {
			const content = chunk.contents.toString('utf8');
			chunk.contents = Buffer.from(`export default \`${content}\`;\n`);
			callback(null, chunk);
		}))
		.pipe(gulp.dest('./src'));
});

gulp.task('build:n.ts', () => {
	const through = require('through2');
	const fs = require('fs');
	const path = require('path');
	const time = new Date().getTime();
	return gulp.src('src/*/n.ts')
		.pipe(through.obj(function (file, encode, callback) {
			let str = file.contents.toString();
			const name = path.dirname(file.path).replace(/.*src\//, '');
			const newExp = new RegExp(`t.src\.*${name}\.js.*`);
			str = str.replace(/<script.*feidao.js.*<\/script>/, `<script src="./dist/js/feidao.js?v=${time}"></script>`);
			str = str.replace(newExp, `t.src = './dist/js/${name}.js?v=${time}';`)
			fs.writeFileSync(file.path, str);
			callback();
		}))
});

gulp.task('test', gulp.series('build:tpl', 'test:build-html', gulp.parallel('test:ts', 'test:server', 'test:watch-tpl', 'test:browser-sync')));

gulp.task('build:webpack', (cb) => {
	const dest = './dist/js/';
	const pack_file_path = `./dist/*/b.js`;
	const path = require('path');
	const webpack = require('webpack');
	const through = require('through');
	const ws = require('webpack-stream');
	return gulp.src([pack_file_path])
		.pipe(((opts) => {
			return through(function (file) {
				file.named = path.basename(path.dirname(file.path));
				this.queue(file);
			});
		})())
		.pipe(ws({
			mode: 'production',
			// mode: 'development',
			plugins: [new webpack.ProvidePlugin({
				$: 'jquery',
				jQuery: 'jquery'
			})],
			optimization: {
				splitChunks: {
					name: 'feidao',
					chunks: 'all'
				}
			},
			externals: [
				// "nools" // this is a bad js file
			],
			output: {
				filename: '[name].js',
				globalObject: 'window',
				publicPath: dest
			},
			module: {
				rules: [{
					test: /\.js$/,
					// exclude: /(node_modules|bower_components)/,
					exclude: /core-js|babel|tus-js-client/,
					use: {
						loader: 'babel-loader'
					}
				}]
			}
		}, webpack))
		.on('error', (e) => {
			cb(e);
		})
		.pipe(gulp.dest(dest));
});

gulp.task('build:drop-console', () => {
	const uglify = require('gulp-uglify-es').default;

	return gulp.src('dist/**/*.js')
		.pipe(uglify({
			compress: {
				drop_console: true
			},
			output: {
				comments: false
			}
		}))
		.pipe(gulp.dest(`dist/`));
});

gulp.task('default', gulp.series('build:drop-console', 'build:webpack'));

gulp.task('check:client', (cb) => {
	const through = require('through2');
	const msg = [];
	return gulp.src(['src/*/zj-**/a*.ts', 'src/*/a*.ts'])
		.pipe(through.obj(function (file, encode, callback) {
			if (/^import\s+.*\s+from\s+['|"]@dfeidao\/nodejs\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/nodejs\/.*['|"]/).toString();
				msg.push(`\n客户端不能引入服务端的原子操作 ${file.path} 中引入了服务端原子操作  ${str}\n`);
			} else if (/^import\s+.*\s+from\s+['|"]@dfeidao\/atom-nodejs\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/atom-nodejs\/.*['|"]/).toString();
				msg.push(`\n客户端不能引入服务端的原子操作 ${file.path} 中引入了服务端原子操作  ${str}\n`);
			} else if (/^import\s+.*\s+from\s+['|"]@dfeidao\/service\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/service\/.*['|"]/).toString();
				msg.push(`\n客户端不能引入服务端的原子操作 ${file.path} 中引入了服务端原子操作  ${str}\n`);
			} else if (/^import\s+.*\s+from\s+['|"]@dfeidao\/core\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/core\/.*['|"]/).toString();
				msg.push(`\n不能引入core原子操作 ${file.path} 中引入了core原子操作  ${str}\n`);
			}
			callback();
		}, function () {
			if (msg.length > 0) {
				console.error(colors.yellow(`客户端原子操作引用错误:`));
				console.error(colors.red(msg.toString()));
				throw new Error('客户端原子操作引用错误');
			} else {
				cb();
			}
		}))
});

gulp.task('check:service', (cb) => {
	const through = require('through2');
	const msg = [];
	return gulp.src(['src/*/n.ts', 'src/*/zj-**/n.ts', 'src/*/zj-**/n*.ts', 'src/*/zj-**/s*.ts'])
		.pipe(through.obj(function (file, encode, callback) {
			if (/^import\s+.*\s+from\s+['|"]@dfeidao\/atom-web\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/atom-web\/.*['|"]/).toString();
				msg.push(`\n服务不能引入客户端的原子操作${file.path} 中引入了客户端原子操作  ${str}\n`);
			} else if (/^import\s+.*\s+from\s+['|"]@dfeidao\/web\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/web\/.*['|"]/).toString();
				msg.push(`\n服务不能引入客户端的原子操作${file.path} 中引入了客户端原子操作  ${str}\n`);
			} else if (/^import \s +.*\s + from\s + ['|"]@dfeidao\/widgets\/.*[' | "]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/widgets\/.*['|"]/).toString();
				msg.push(`\n服务不能引入控件${file.path} 中引入了控件  ${str}\n`);
			} else if (/^import\s+.*\s+from\s+['|"]@dfeidao\/core\/.*['|"]/m.test(file.contents.toString())) {
				const str = file.contents.toString().match(/import\s+.*\s+from\s+['|"]@dfeidao\/core\/.*['|"]/).toString();
				msg.push(`\n不能引入core原子操作 ${file.path} 中引入了core原子操作  ${str}\n`);
			}
			callback();
		}, function () {
			if (msg.length > 0) {
				console.error(colors.yellow(`服务端原子操作引用错误:`));
				console.error(colors.red(msg.toString()));
				throw new Error('服务端原子操作引用错误');
			} else {
				cb();
			}
		}))
});

gulp.task('check:widgets', (cb) => {
	const through = require('through2');
	const path = require('path');
	const errs = [];
	return gulp.src('src/**/b.ts')
		.pipe(through.obj(function (file, encode, callback) {
			const widgets = [];
			const dir = path.dirname(file.path);
			const centents = file.contents.toString().split('\n');
			centents.forEach((centent, i) => {
				if (/^import\s+['|"]@dfeidao\/widgets\/fd-[wh]{1,2}[0-9]+['|"]/.test(centent)) {
					const no = centent.match(/fd-[a-z]{1,2}[0-9]+/m).toString();
					widgets.push({
						no,
						path: `${file.path}:${++i}`
					});
				}
			});
			gulp.src([`${dir}/html.ts`, `${dir}/tpl.ts`, `${dir}/P*.ts`, `${dir}/p*.ts`, `${dir}/zj-**/tpl.ts`, `${dir}/zj-**/P*.ts`, `${dir}/zj-**/p*.ts`, `${dir}/zj-**/*.tpl`], {
				allowEmpty: true
			})
				.pipe(through.obj(function (file, encode, cb) {
					const centents = file.contents.toString();
					widgets.forEach((widget) => {
						if (!widget.used && centents.match(widget.no, 'mg')) {
							widget.used = true;
						}
					});
					cb();
				}, function () {
					errs.push(...widgets.filter((widget) => {
						return !widget.used;
					}).map((widget) => {
						return `${widget.path} 定义的控件未被使用`;
					}));
					callback();
				}));
		}, function () {
			if (errs.length > 0) {
				console.error(colors.yellow(`控件引用错误:`));
				console.error(colors.red(errs.join('\n')));
				throw new Error('控件引用错误');
			} else {
				cb();
			}
		}));
});

gulp.task('check:send-msg', (cb) => {
	const through = require('through2');
	const arr = [];
	return gulp.src('src/*/zj-**/s*.ts')
		.pipe(through.obj(function (file, encode, callback) {
			if (/send_msg/m.test(file.contents.toString())) {
				const strs = file.contents.toString().split('\n');
				strs.forEach((item, i) => {
					if (item.match(/send_msg/m)) {
						arr.push(file.path + ':' + (i + 1) + '\t 服务中不能引用send_msg 请手动删除');
					}
				});
			}
			callback();
		}, function () {
			if (arr.length > 0) {
				console.error(colors.yellow(`错误的方法调用:`));
				console.error(colors.red(arr.join('\n')));
				throw new Error('错误的方法调用');
			} else {
				cb();
			}
		}))
});

gulp.task('check', gulp.parallel('check:client', 'check:service', 'check:widgets', 'check:send-msg'));
